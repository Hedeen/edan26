#[macro_use] extern crate text_io;

use std::sync::{Mutex,Arc};
use std::collections::LinkedList;
use std::cmp;
//use std::thread;
use std::collections::VecDeque;

struct Node {
	i:	usize,			/* index of itself for debugging.	*/
	e:	i32,			/* excess preflow.			*/
	h:	i32,			/* height.				*/
}

struct Edge {
        u:      usize,
        v:      usize,
        f:      i32,
        c:      i32,
}

impl Node {
	fn new(ii:usize) -> Node {
		Node { i: ii, e: 0, h: 0 }
	}

}

impl Edge {
        fn new(uu:usize, vv:usize,cc:i32) -> Edge {
                Edge { u: uu, v: vv, f: 0, c: cc }
        }
}


fn main() {
	let n: usize = read!();		/* n nodes.						*/
	let m: usize = read!();		/* m edges.						*/
	let _c: usize = read!();	/* underscore avoids warning about an unused variable.	*/
	let _p: usize = read!();	/* c and p are in the input from 6railwayplanning.	*/
	let mut node = vec![];
	let mut edge = vec![];
	let mut adj: Vec<LinkedList<usize>> =Vec::with_capacity(n);
	let mut excess: VecDeque<usize> = VecDeque::new();
	let debug = false;

	let s = 0;
	let t = n-1;

	println!("n = {}", n);
	println!("m = {}", m);

	for i in 0..n {
		let u:Node = Node::new(i);
		node.push(Arc::new(Mutex::new(u)));
		adj.push(LinkedList::new());
	}

	for i in 0..m {
		let u: usize = read!();
		let v: usize = read!();
		let c: i32 = read!();
		let e:Edge = Edge::new(u,v,c);
		adj[u].push_back(i);
		adj[v].push_back(i);
		edge.push(Arc::new(Mutex::new(e)));
	}

	{
		let mut source = node[s].lock().unwrap();
		source.h += n as i32;
	}

	if debug {
		for i in 0..n {
			print!("adj[{}] = ", i);
			let iter = adj[i].iter();

			for e in iter {
				print!("e = {}, ", e);
			}
			println!("");
		}
	}

	println!("initial pushes");
	let iter = adj[s].iter();

	for &e in iter {
		let mut unlocked_edge = edge[e].lock().unwrap();
		unlocked_edge.f += unlocked_edge.c; // add flow to edge
		let v: usize;
		if s == unlocked_edge.u {
			v = unlocked_edge.v;
		} else {
			v = unlocked_edge.u;
		}
		let mut unlocked_node = node[v].lock().unwrap();
		let mut source = node[s].lock().unwrap();
		source.e -= unlocked_edge.c;
		unlocked_node.e += unlocked_edge.c; // add excess to node v
		if v != t {
			excess.push_back(v);
		}
	}

	while !excess.is_empty() {
		let u = excess.pop_front().unwrap();
		let iter = adj[u].iter();
		let mut from = node[u].lock().unwrap();
		println!("selected {}, e={}, h={}", u, from.e, from.h);
		let mut pushes:i32 = 0;
		for &edge_id in iter {
			if from.e == 0 {
				break;
			}
			let mut e = edge[edge_id].lock().unwrap();
			let v:usize;
			let b:i32;
			if u == e.u {
				v = e.v;
				b = 1;
			} else {
				v = e.u;
				b = -1;
			}
			let mut to = node[v].lock().unwrap();
			if from.h > to.h && b * e.f < e.c {
				pushes += 1;
				let d:i32;
				if b == 1 {
					d = cmp::min(from.e, e.c - e.f);
					e.f += d;
				} else {
					d = cmp::min(from.e, e.c + e.f);
					e.f -= d;
				}
				println!("pushin {}, form {} to {}",
						d, from.i, to.i);
				from.e -= d;
				to.e += d;
				if to.e == d {
					if v != s && v != t {
						excess.push_back(v);
					}
				}
			}
		}
		if pushes == 0 {
			from.h += 1; // relabel
		}
		if from.e > 0 {
			excess.push_back(u);
		}
	}
	let sink = node[t].lock().unwrap();
	println!("f = {}", sink.e);

}
