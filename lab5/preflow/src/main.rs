#[macro_use] extern crate text_io;

use std::sync::{Mutex,Arc};
use std::collections::LinkedList;
use std::cmp;
use std::thread;
use std::collections::VecDeque;

struct Node {
	_i:	usize,			/* index of itself for debugging.	*/
	e:	i32,			/* excess preflow.			*/
	h:	i32,			/* height.				*/
}

struct Edge {
        u:      usize,
        v:      usize,
        f:      i32,
        c:      i32,
}

impl Node {
	fn new(ii:usize) -> Node {
		Node { _i: ii, e: 0, h: 0 }
	}

}

impl Edge {
        fn new(uu:usize, vv:usize,cc:i32) -> Edge {
                Edge { u: uu, v: vv, f: 0, c: cc }
        }
}

fn work(excess:Arc<Mutex<VecDeque<usize>>>,
		adj:Vec<LinkedList<usize>>, node:Vec<Arc<Mutex<Node>>>,
		edge:Vec<Arc<Mutex<Edge>>>,
		s:usize,
		t:usize)
	{
	loop {
		let u:usize;
        {
            let mut unlocked_excess = excess.lock().unwrap();
			if unlocked_excess.is_empty() {
				break;
			}
            u = unlocked_excess.pop_front().unwrap();
        }
		let iter = adj[u].iter();
        let mut pushes:i32 = 0;
		for &edge_id in iter {
            {
                let from = node[u].lock().unwrap();
                if from.e == 0 {
				    break;
                }
			}
			let mut e = edge[edge_id].lock().unwrap();
			let v:usize;
			let b:i32;
			if u == e.u {
				v = e.v;
				b = 1;
			} else {
				v = e.u;
				b = -1;
			}
            let mut from;
            let mut to;
            if v < u {
                to = node[v].lock().unwrap();
                from = node[u].lock().unwrap();
            } else {
                from = node[u].lock().unwrap();
                to = node[v].lock().unwrap();
            }
			if from.h > to.h && b * e.f < e.c {
				pushes += 1;
				let d:i32;
				if b == 1 {
					d = cmp::min(from.e, e.c - e.f);
					e.f += d;
				} else {
					d = cmp::min(from.e, e.c + e.f);
					e.f -= d;
				}
				from.e -= d;
				to.e += d;
				if to.e == d {
					if v != s && v != t {
						excess.lock().unwrap().push_back(v);
					}
				}
			}
		}
        {
            let mut from = node[u].lock().unwrap();
		    if pushes == 0 {
			    from.h += 1; // relabel
		    }
		    if from.e > 0 {
			    excess.lock().unwrap().push_back(u);
		    }
        }
    }
}


fn main() {
	let n: usize = read!();		/* n nodes.						*/
	let m: usize = read!();		/* m edges.						*/
	let _c: usize = read!();	/* underscore avoids warning about an unused variable.	*/
	let _p: usize = read!();	/* c and p are in the input from 6railwayplanning.	*/
	let mut node = vec![];
	let mut edge = vec![];
	let mut adj: Vec<LinkedList<usize>> =Vec::with_capacity(n);
	let excess: Arc<Mutex<VecDeque<usize>>> = Arc::new(Mutex::new(VecDeque::new()));
	let debug = false;

	let s = 0;
	let t = n-1;

	println!("n = {}", n);
	println!("m = {}", m);

	for i in 0..n {
		let u:Node = Node::new(i);
		node.push(Arc::new(Mutex::new(u)));
		adj.push(LinkedList::new());
	}

	for i in 0..m {
		let u: usize = read!();
		let v: usize = read!();
		let c: i32 = read!();
		let e:Edge = Edge::new(u,v,c);
		adj[u].push_back(i);
		adj[v].push_back(i);
		edge.push(Arc::new(Mutex::new(e)));
	}

	{
		let mut source = node[s].lock().unwrap();
		source.h += n as i32;
	}

	if debug {
		for i in 0..n {
			print!("adj[{}] = ", i);
			let iter = adj[i].iter();

			for e in iter {
				print!("e = {}, ", e);
			}
			println!("");
		}
	}

	println!("initial pushes");
	let iter = adj[s].iter();

	for &e in iter {
		let mut unlocked_edge = edge[e].lock().unwrap();
		unlocked_edge.f += unlocked_edge.c; // add flow to edge
		let v: usize;
		if s == unlocked_edge.u {
			v = unlocked_edge.v;
		} else {
			v = unlocked_edge.u;
		}
		let mut unlocked_node = node[v].lock().unwrap();
		let mut source = node[s].lock().unwrap();
		source.e -= unlocked_edge.c;
		unlocked_node.e += unlocked_edge.c; // add excess to node v
		if v != t {
		    excess.lock().unwrap().push_back(v);
		}
	}
	let mut threads = vec![];
	for _ in 0..3 { // TODO: ask about too many threads
		let excess_clone = excess.clone();
		let node_clone = node.clone();
		let edge_clone = edge.clone();
		let adj_clone = adj.clone();
		let thread = thread::spawn(move||{
			work(excess_clone, adj_clone, node_clone, edge_clone, s, t);
		});
		threads.push(thread);
	}
	for thread in threads {
		thread.join().unwrap();
	}
	let sink = node[t].lock().unwrap();
	println!("f = {}", sink.e);

}
