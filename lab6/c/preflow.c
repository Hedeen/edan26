#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <stdatomic.h>

#define PRINT		0	/* enable/disable prints. */
#define MAIN        1   /* disable forsete */

#if PRINT
#define pr(...)		do { fprintf(stderr, __VA_ARGS__); } while (0)
#else
#define pr(...)		/* no effect at all */
#endif

#define MIN(a,b)	(((a)<=(b))?(a):(b))


pthread_barrier_t barrier;

#if MAIN
#define NBRTHREADS()  (14)
#else 
#define NBRTHREADS()  (10)
#endif

int nbr_threads = NBRTHREADS();

typedef struct graph_t	    	graph_t;
typedef struct node_t	    	node_t;
typedef struct edge_t	    	edge_t;
typedef struct list_t	    	list_t;
typedef struct thread_data_t 	thread_data_t;
typedef struct work_arg_t 	work_arg_t;
typedef struct xedge_t 			xedge_t;

struct xedge_t {
	int32_t		u;	/* one of the two nodes.	*/
	int32_t		v;	/* the other. 			*/
	int32_t		c;	/* capacity.			*/
};

struct list_t {
	edge_t*		edge;
	list_t*		next;
};

struct node_t {
	int		h;	    /* height.			*/
	int		e;	    /* excess flow.			*/
	list_t*		edge;	    /* adjacency list.		*/
	node_t*		next;	    /* with excess preflow.		*/
	int             relabel;    /* 1 if we should relabel */
	int     updated;    /* to keep track of updated this iteration */
	int	e_queue;    /* atomic int for pending excess changes */
	node_t*         next_update;/* like next, but for updates */
};

struct edge_t {
	node_t*		u;	/* one of the two nodes.	*/
	node_t*		v;	/* the other. 			*/
	int		f;	/* flow > 0 if from u to v.	*/
	int		c;	/* capacity.			*/
};

struct thread_data_t {
	node_t* 		excess;  /* personal list of nodes with excess */
	node_t*         	updates; /* personla list of updated nodes */
	int 			thread_id;
	int 			pushes;
	int 			relabels;
};

struct graph_t {
	int		n;	/* nodes.			*/
	int		m;	/* edges.			*/
	int     	done;   /* 1 if algorithm done */
	node_t*		v;	/* array of n nodes.		*/
	edge_t*		e;	/* array of m edges.		*/
	node_t*		s;	/* source.			*/
	node_t*		t;	/* sink.			*/
};

struct work_arg_t {
	graph_t* g;
	thread_data_t* stats;
};

static char* progname;


static int id(graph_t* g, node_t* v)
{
	return v - g->v;
}

void error(const char* fmt, ...)
{
	va_list		ap;
	char		buf[BUFSIZ];

	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);

	if (progname != NULL)
		fprintf(stderr, "%s: ", progname);

	fprintf(stderr, "error: %s\n", buf);
	exit(1);
}

static int next_int()
{
        int     x;
        int     c;

	x = 0;
        while (isdigit(c = getchar()))
                x = 10 * x + c - '0';

        return x;
}

static void* xmalloc(size_t s)
{
	void*		p;

	p = malloc(s);

	if (p == NULL)
		error("out of memory: malloc(%zu) failed", s);

	return p;
}

static void* xcalloc(size_t n, size_t s)
{
	void*		p;

	p = xmalloc(n * s);

	/* memset sets everything (in this case) to 0. */
	memset(p, 0, n * s);

	return p;
}

static void add_edge(node_t* u, edge_t* e)
{
	list_t*		p;

	p = xmalloc(sizeof(list_t));
	p->edge = e;
	p->next = u->edge;
	u->edge = p;
}

static void connect(node_t* u, node_t* v, int c, edge_t* e)
{

	e->u = u;
	e->v = v;
	e->c = c;

	add_edge(u, e);
	add_edge(v, e);
}

#if MAIN
static graph_t* new_graph(FILE* in, int n, int m)
{
	graph_t*	g;
	node_t*		u;
	node_t*		v;
	int		i;
	int		a;
	int		b;
	int		c;

	g = xmalloc(sizeof(graph_t));

	g->n = n;
	g->m = m;

	g->v = xcalloc(n, sizeof(node_t));
	g->e = xcalloc(m, sizeof(edge_t));

	g->s = &g->v[0];
	g->t = &g->v[n-1];


	g->done = 0;
	
	for (i = 0; i < m; i += 1) {
		a = next_int();
		b = next_int();
		c = next_int();
		u = &g->v[a];
		v = &g->v[b];
		connect(u, v, c, g->e+i);
	}

	for (i = 0; i < n; i+=1) {
		/* set all queues to 0 */
		g->v[i].e_queue = 0;
		g->v[i].updated = 0;
	}

	return g;
}
#else
static graph_t* new_graph(int n, int m, int s, int t, xedge_t* e)
{
	graph_t*	g;
	int		i;
	node_t*		u;
	node_t*		v;
	int		a;
	int		b;
	int		c;

	g = xcalloc(1, sizeof(graph_t));
	g->n = n;
	g->m = m;

	g->v = xcalloc(n, sizeof(node_t));
	g->e = xcalloc(m, sizeof(edge_t));

	g->s = &g->v[s];
	g->t = &g->v[t];
	
	g->done = 0;
	
	for (i = 0; i < m; i += 1) {
		a = e[i].u;
		b = e[i].v;
		c = e[i].c;
		u = &g->v[a];
		v = &g->v[b];
		connect(u, v, c, g->e+i);
	}

	for (i = 0; i<n; i +=1) {
		g->v[i].e_queue = 0;
		g->v[i].updated = 0;
	}

	return g;
}
#endif

static void enter_updates(thread_data_t* data, node_t* v)
{
	v->next_update = data->updates;
	data->updates = v; // update pointer
}

static node_t* leave_updates(thread_data_t* data)
{
	node_t* v = data->updates;
	if (v != NULL) {
		data->updates = v->next_update;
	}
	return v;
}

static void enter_excess(thread_data_t* data, node_t* v)
{
	/* adds node to threads excess list */
	v->next = data->excess;
	data->excess = v;
}

static node_t* leave_excess(thread_data_t* data)
{
	node_t*	v = data->excess;
	if (v != NULL) {
		data->excess = v->next;
	}
	return v;
}

static node_t* other(node_t* u, edge_t* e)
{
	if (u == e->u)
		return e->v;
	else
		return e->u;
}

static void relabel(graph_t* g, node_t* u)
{
	u->h += 1;
	u->relabel = 0; // If we relabel, we shouldn't do it for another iteration
}

void *ppreflow(void *arg) {
	int thread_id;
	work_arg_t* param = arg;
	graph_t*    g = param->g;
	thread_data_t* thread_data = param->stats;
	node_t*		u;
	thread_id = thread_data->thread_id;
	node_t* v;
	list_t* p;
	edge_t* e;
	int b;
	int new_pushes;
	while(g->done == 0) {
		u = leave_excess(thread_data);
		while (u != NULL) {
			int old;
			__transaction_atomic {
				old = u->updated;
				u->updated = 1;
			}
			pr("thread%d: selected node %d\n", thread_id, id(g, u));
			if (old == 0) {
				pr("thread%d: adding node %d to updates\n", thread_id, id(g, u));
				enter_updates(thread_data, u);
			}
			p = u->edge;
			new_pushes = 0;
			while (p != NULL && u->e > 0) {
				e = p->edge;
				p = p->next;

				if (u == e->u) {
					v = e->v;
					b = 1;
				} else {
					v = e->u;
					b = -1;
				}

				if (u->h > v->h && b * e->f < e->c) {
					int		d;	/* remaining capacity of the edge. */

					if (b==1) {
						d = MIN(u->e, e->c - e->f);
					} else {
						d = -MIN(u->e, e->c + e->f);
					}

					/* update values that no other thread can */
					u->e -= abs(d);
					e->f += d;
					int old;
					__transaction_atomic {
						v->e_queue += abs(d);
						old = v->updated;
						v->updated = 1;
					}
					if (old == 0) {
						pr("thread%d: adding node %d to updates\n", thread_id, id(g, v));
						enter_updates(thread_data, v);
					}
					thread_data->pushes += 1;
					new_pushes += 1;
				}
			}
			if (new_pushes == 0 && u->e > 0) {
				thread_data->relabels += 1;
				u->relabel = 1;
			}
			u = leave_excess(thread_data);
		}
		pthread_barrier_wait(&barrier);
		pthread_barrier_wait(&barrier);
	}
	return 0;
}

static int xpreflow(graph_t* g)
{
	node_t*		s;
	node_t*		u;
	node_t*		v;
	edge_t*		e;
	list_t*		p;
	int		b;
	pthread_t threads[nbr_threads];
	thread_data_t thread_data[nbr_threads];
	work_arg_t args[nbr_threads];

	int f;
	int t = 0;

	s = g->s;
	s->h = g->n;

	p = s->edge;

	/* start by pushing as much as possible (limited by
	 * the edge capacity) from the source to its neighbors.
	 *
	 */
	while (p != NULL) {
		e = p->edge;
		p = p->next;
		u = other(s, e);
		f = e->c;
		s->e -= f; // Source will have negative excess
		u->e += f; // Add excess to other
	    	e->f += f;
	}

	// Initialize thread data
	for (int i = 0; i < nbr_threads; i += 1) {
		thread_data[i].thread_id = i;
		thread_data[i].excess = NULL;
		thread_data[i].updates = NULL;
		thread_data[i].pushes = 0;
		thread_data[i].relabels = 0;
		args[i].g = g;
		args[i].stats = &thread_data[i];
	}
	// Add node to excess lists of threads
	for (int i = 0; i < g->n; i += 1) {
		node_t* v = &g->v[i];
		if (v->e > 0 && v != g->s && v != g->t) {
			enter_excess(&thread_data[t], v);
			t = (t+1) % nbr_threads;
		}
	}

	/* create and start threads */
	for (int i = 0; i < nbr_threads; i += 1) {
		pthread_create(&threads[i], NULL, *ppreflow, &args[i]);
	}

	/* then loop until only s and/or t have excess preflow. */
	while (g->done == 0) {

		/* wait until threads are done calculating */
		pthread_barrier_wait(&barrier);

		int          f;
		int new_excess = 0;
		int t = 0; // Thread index for load balance
		for (int i = 0; i < nbr_threads; i+=1) {
			node_t* node = leave_updates(&thread_data[i]);
			int ind = -1;
			while (node != NULL) {
				pr("updating node %d\n", id(g, node));
				if (id(g, node)==ind) {
					break;
				}
				node->e += node->e_queue;
				ind = id(g, node);
				node->e_queue = 0;
				node->updated = 0;
				if (node->relabel == 1) {
					relabel(g, node);
				}
				if (node->e > 0 && node != g->s && node != g->t) {
					enter_excess(&thread_data[t], node);
					t = (t + 1) % nbr_threads;
					new_excess += 1;
				}
				node = leave_updates(&thread_data[i]);
			}
			pr("no more nodes\n");
		}

		if (g->t->e == -(g->s->e))  {
			g->done = 1;
		}
		pthread_barrier_wait(&barrier);
	}
	for (int i = 0; i < nbr_threads; i += 1) {
		pr("thread %d: relabels=%d, pushes=%d\n",
			i,
			thread_data[i].relabels,
			thread_data[i].pushes);
	}
	for (int i = 0; i < nbr_threads; i++) {
		pthread_join(threads[i], NULL);
	}

	return g->t->e;
}

static void free_graph(graph_t* g)
{
	int		i;
	list_t*		p;
	list_t*		q;

	for (i = 0; i < g->n; i += 1) {
		p = g->v[i].edge;
		while (p != NULL) {
			q = p->next;
			free(p);
			p = q;
		}
	}
	free(g->v);
	free(g->e);
	free(g);
}

# if MAIN
int main(int argc, char* argv[])
{
	FILE*		in;	/* input file set to stdin	*/
	graph_t*	g;	/* undirected graph. 		*/
	int		f;	/* output from preflow.		*/
	int		n;	/* number of nodes.		*/
	int		m;	/* number of edges.		*/

	progname = argv[0];	/* name is a string in argv[0]. */

	in = stdin;		/* same as System.in in Java.	*/

	n = next_int();
	m = next_int();

	/* skip C and P from the 6railwayplanning lab in EDAF05 */
	next_int();
	next_int();

	g = new_graph(in, n, m);

	fclose(in);

	/* init barrier */
	pthread_barrier_init(&barrier, NULL, nbr_threads+1);

	/* create threads to run */
	f = xpreflow(g);

	/* join threads */
	printf("f = %d\n", f);

	free_graph(g);

	return 0;
}
#else
int preflow(int n, int m, int s, int t, xedge_t* e)
{
	graph_t*	g;	/* undirected graph. 		*/
	int		f;	/* output from preflow.		*/

	g = new_graph(n, m, s, t, e);

	/* init barrier */
	pthread_barrier_init(&barrier, NULL, nbr_threads+1);

	/* create threads to run */
	f = xpreflow(g);

	free_graph(g);

	return f;
}
#endif
