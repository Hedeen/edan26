	.file	"sequential.c"
	.abiversion 2
	.section	".text"
.Ltext0:
	.cfi_sections	.debug_frame
	.section	".toc","aw"
	.align 3
.LC0:
	.quad	stdin
	.section	".text"
	.align 2
	.p2align 4,,15
	.type	next_int, @function
next_int:
.LFB55:
	.file 1 "sequential.c"
	.loc 1 195 0
	.cfi_startproc
.LCF0:
0:	addis 2,12,.TOC.-.LCF0@ha
	addi 2,2,.TOC.-.LCF0@l
	.localentry	next_int,.-next_int
.LVL0:
	mflr 0
	std 28,-32(1)
	std 29,-24(1)
	std 31,-8(1)
	std 30,-16(1)
	.cfi_register 65, 0
	.cfi_offset 28, -32
	.cfi_offset 29, -24
	.cfi_offset 31, -8
	.cfi_offset 30, -16
	.loc 1 213 0
	li 31,0
	.loc 1 195 0
	std 0,16(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 65, 16
	.loc 1 214 0
	bl __ctype_b_loc
	nop
.LVL1:
	addis 29,2,.LC0@toc@ha		# gpr load fusion, type long
	ld 29,.LC0@toc@l(29)
	mr 28,3
	b .L2
.LVL2:
	.p2align 4,,15
.L3:
	.loc 1 215 0
	extsw 31,3
.LVL3:
.L2:
.LBB36:
.LBB37:
	.file 2 "/usr/include/powerpc64le-linux-gnu/bits/stdio.h"
	.loc 2 46 0
	ld 3,0(29)
.LBE37:
.LBE36:
	.loc 1 214 0
	ld 30,0(28)
.LBB39:
.LBB38:
	.loc 2 46 0
	bl _IO_getc
	nop
.LVL4:
.LBE38:
.LBE39:
	.loc 1 215 0
	mulli 9,31,10
	.loc 1 214 0
	sldi 10,3,1
	lhzx 10,30,10
	.loc 1 215 0
	add 3,9,3
	addi 3,3,-48
	.loc 1 214 0
	andi. 9,10,0x800
	bne 0,.L3
	.loc 1 218 0
	addi 1,1,64
	.cfi_def_cfa_offset 0
	mr 3,31
	ld 0,16(1)
	ld 28,-32(1)
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
.LVL5:
	mtlr 0
	.cfi_restore 65
	.cfi_restore 31
	.cfi_restore 30
	.cfi_restore 29
	.cfi_restore 28
	blr
	.long 0
	.byte 0,0,0,1,128,4,0,0
	.cfi_endproc
.LFE55:
	.size	next_int,.-next_int
	.align 2
	.p2align 4,,15
	.type	push, @function
push:
.LFB63:
	.loc 1 368 0
	.cfi_startproc
.LCF1:
0:	addis 2,12,.TOC.-.LCF1@ha
	addi 2,2,.TOC.-.LCF1@l
	.localentry	push,.-push
.LVL6:
	lwa 8,4(4)
	.loc 1 374 0
	ld 10,0(6)
	.loc 1 368 0
	stdu 1,-32(1)
	.cfi_def_cfa_offset 32
	lwa 11,20(6)
	lwa 9,16(6)
	.loc 1 374 0
	cmpd 7,10,4
	beq 7,.L20
	.loc 1 378 0
	add 10,11,9
	cmpw 7,10,8
	bgt 7,.L21
.L10:
	.loc 1 379 0
	subf 9,10,9
	.loc 1 378 0
	extsw 10,10
.LVL7:
	.loc 1 379 0
	extsw 9,9
.LVL8:
.L9:
	.loc 1 384 0
	subf 8,10,8
	stw 9,16(6)
.LVL9:
	.loc 1 389 0
	cmpwi 7,10,0
	.loc 1 384 0
	stw 8,4(4)
	.loc 1 385 0
	lwz 8,4(5)
	add 8,8,10
	stw 8,4(5)
	.loc 1 389 0
	blt 7,.L22
	.loc 1 390 0
	lwa 6,4(4)
.LVL10:
	cmpwi 7,6,0
	blt 7,.L23
	.loc 1 391 0
	srawi 7,9,31
	xor 9,7,9
	subf 9,7,9
	cmpw 7,9,11
	bgt 7,.L24
	.loc 1 393 0
	cmpdi 7,6,0
	beq 7,.L14
.LVL11:
.LBB40:
.LBB41:
	.loc 1 344 0
	ld 9,32(3)
	cmpd 7,4,9
	beq 7,.L14
	ld 9,24(3)
	cmpd 7,4,9
	beq 7,.L14
	.loc 1 345 0
	ld 9,40(3)
	std 9,16(4)
	.loc 1 346 0
	std 4,40(3)
.LVL12:
.L14:
.LBE41:
.LBE40:
	.loc 1 400 0
	cmpw 7,8,10
	beq 7,.L25
.L6:
	.loc 1 409 0
	addi 1,1,32
	.cfi_remember_state
	.cfi_def_cfa_offset 0
	blr
.LVL13:
	.p2align 4,,15
.L21:
	.cfi_restore_state
	.loc 1 378 0
	mr 10,8
	b .L10
.LVL14:
	.p2align 4,,15
.L25:
.LBB42:
.LBB43:
	.loc 1 344 0
	ld 9,32(3)
	cmpd 7,5,9
	beq 7,.L6
	ld 9,24(3)
	cmpd 7,5,9
	beq 7,.L6
	.loc 1 345 0
	ld 9,40(3)
.LBE43:
.LBE42:
	.loc 1 409 0
	addi 1,1,32
	.cfi_remember_state
	.cfi_def_cfa_offset 0
.LBB45:
.LBB44:
	.loc 1 345 0
	std 9,16(5)
	.loc 1 346 0
	std 5,40(3)
.LBE44:
.LBE45:
	.loc 1 409 0
	blr
.LVL15:
	.p2align 4,,15
.L20:
	.cfi_restore_state
	.loc 1 375 0
	subf 10,9,11
	cmpw 7,10,8
	bgt 7,.L26
	.loc 1 376 0
	add 9,9,10
	.loc 1 375 0
	extsw 10,10
.LVL16:
	.loc 1 376 0
	extsw 9,9
	b .L9
.LVL17:
	.p2align 4,,15
.L26:
	.loc 1 375 0
	mr 10,8
	.loc 1 376 0
	add 9,9,10
	.loc 1 375 0
	extsw 10,10
.LVL18:
	.loc 1 376 0
	extsw 9,9
	b .L9
.LVL19:
.L23:
	mflr 0
	.cfi_remember_state
	.cfi_register 65, 0
	.loc 1 390 0 discriminator 1
	addis 6,2,.LANCHOR0@toc@ha
	addis 4,2,.LC1@toc@ha
.LVL20:
	addis 3,2,.LC3@toc@ha
.LVL21:
	addi 6,6,.LANCHOR0@toc@l
	li 5,390
.LVL22:
	addi 4,4,.LC1@toc@l
	addi 3,3,.LC3@toc@l
	std 0,48(1)
	.cfi_offset 65, 16
	bl __assert_fail
	nop
.LVL23:
.L22:
	.cfi_restore_state
	mflr 0
	.cfi_remember_state
	.cfi_register 65, 0
	.loc 1 389 0 discriminator 1
	addis 6,2,.LANCHOR0@toc@ha
.LVL24:
	addis 4,2,.LC1@toc@ha
.LVL25:
	addis 3,2,.LC2@toc@ha
.LVL26:
	addi 6,6,.LANCHOR0@toc@l
	li 5,389
.LVL27:
	addi 4,4,.LC1@toc@l
	addi 3,3,.LC2@toc@l
	std 0,48(1)
	.cfi_offset 65, 16
	bl __assert_fail
	nop
.LVL28:
.L24:
	.cfi_restore_state
	mflr 0
	.cfi_register 65, 0
	.loc 1 391 0 discriminator 1
	addis 6,2,.LANCHOR0@toc@ha
	addis 4,2,.LC1@toc@ha
.LVL29:
	addis 3,2,.LC4@toc@ha
.LVL30:
	addi 6,6,.LANCHOR0@toc@l
	li 5,391
.LVL31:
	addi 4,4,.LC1@toc@l
	addi 3,3,.LC4@toc@l
	std 0,48(1)
	.cfi_offset 65, 16
	.loc 1 391 0 discriminator 1
	bl __assert_fail
	nop
.LVL32:
	.long 0
	.byte 0,0,0,1,128,0,0,0
	.cfi_endproc
.LFE63:
	.size	push,.-push
	.section	".toc","aw"
.LC6:
	.quad	stderr
	.section	".text"
	.align 2
	.p2align 4,,15
	.globl error
	.type	error, @function
error:
.LFB54:
	.loc 1 153 0
	.cfi_startproc
.LCF2:
0:	addis 2,12,.TOC.-.LCF2@ha
	addi 2,2,.TOC.-.LCF2@l
	.localentry	error,.-error
.LVL33:
	mflr 0
	std 30,-16(1)
	std 31,-8(1)
	std 0,16(1)
	stdu 1,-8320(1)
	.cfi_def_cfa_offset 8320
	.cfi_offset 65, 16
	.cfi_offset 30, -16
	.cfi_offset 31, -8
.LVL34:
	.loc 1 153 0
	std 6,8376(1)
	std 4,8360(1)
.LBB46:
.LBB47:
	.file 3 "/usr/include/powerpc64le-linux-gnu/bits/stdio2.h"
	.loc 3 46 0
	addi 31,1,104
.LVL35:
	mr 6,3
.LBE47:
.LBE46:
	.loc 1 153 0
	std 5,8368(1)
	std 7,8384(1)
.LBB51:
.LBB48:
	.loc 3 46 0
	mr 3,31
.LVL36:
	addi 7,1,8360
.LVL37:
.LBE48:
.LBE51:
	.loc 1 153 0
	std 8,8392(1)
	std 9,8400(1)
.LBB52:
.LBB49:
	.loc 3 46 0
	li 5,8192
	li 4,1
.LBE49:
.LBE52:
	.loc 1 153 0
	std 10,8408(1)
	ld 9,-28688(13)
	std 9,8296(1)
	li 9,0
.LBB53:
.LBB50:
	.loc 3 46 0
	bl __vsprintf_chk
	nop
.LVL38:
.LBE50:
.LBE53:
	.loc 1 187 0
	addis 6,2,.LANCHOR1@toc@ha		# gpr load fusion, type long
	ld 6,.LANCHOR1@toc@l(6)
	.loc 1 188 0
	addis 30,2,.LC6@toc@ha		# gpr load fusion, type long
	ld 30,.LC6@toc@l(30)
	.loc 1 187 0
	cmpdi 7,6,0
	beq 7,.L28
.LVL39:
.LBB54:
.LBB55:
	.loc 3 97 0
	ld 3,0(30)
	addis 5,2,.LC5@toc@ha
	li 4,1
	addi 5,5,.LC5@toc@l
	bl __fprintf_chk
	nop
.LVL40:
.L28:
.LBE55:
.LBE54:
.LBB56:
.LBB57:
	ld 3,0(30)
	addis 5,2,.LC7@toc@ha
	mr 6,31
	addi 5,5,.LC7@toc@l
	li 4,1
	bl __fprintf_chk
	nop
.LVL41:
.LBE57:
.LBE56:
	.loc 1 191 0
	li 3,1
	bl exit
	nop
.LVL42:
	.long 0
	.byte 0,0,0,1,128,2,0,0
	.cfi_endproc
.LFE54:
	.size	error,.-error
	.align 2
	.p2align 4,,15
	.type	xmalloc, @function
xmalloc:
.LFB56:
	.loc 1 221 0
	.cfi_startproc
.LCF3:
0:	addis 2,12,.TOC.-.LCF3@ha
	addi 2,2,.TOC.-.LCF3@l
	.localentry	xmalloc,.-xmalloc
.LVL43:
	mflr 0
	std 31,-8(1)
	.cfi_register 65, 0
	.cfi_offset 31, -8
	mr 31,3
	std 0,16(1)
	stdu 1,-112(1)
	.cfi_def_cfa_offset 112
	.cfi_offset 65, 16
	.loc 1 237 0
	bl malloc
	nop
.LVL44:
	.loc 1 239 0
	cmpdi 7,3,0
	beq 7,.L37
	.loc 1 243 0
	addi 1,1,112
	.cfi_remember_state
	.cfi_def_cfa_offset 0
	ld 0,16(1)
	ld 31,-8(1)
.LVL45:
	mtlr 0
	.cfi_restore 65
	.cfi_restore 31
	blr
.LVL46:
.L37:
	.cfi_restore_state
	.loc 1 240 0
	addis 3,2,.LC8@toc@ha
.LVL47:
	mr 4,31
	addi 3,3,.LC8@toc@l
	bl error
.LVL48:
	.long 0
	.byte 0,0,0,1,128,1,0,0
	.cfi_endproc
.LFE56:
	.size	xmalloc,.-xmalloc
	.section	".toc","aw"
	.set .LC9,.LC0
.LC10:
	.quad	hello
	.section	.text.startup,"ax",@progbits
	.align 2
	.p2align 4,,15
	.globl main
	.type	main, @function
main:
.LFB68:
	.loc 1 525 0
	.cfi_startproc
.LCF4:
0:	addis 2,12,.TOC.-.LCF4@ha
	addi 2,2,.TOC.-.LCF4@l
	.localentry	main,.-main
.LVL49:
	mflr 0
	std 23,-72(1)
	std 24,-64(1)
	.loc 1 532 0
	addis 10,2,.LANCHOR1@toc@ha
	.loc 1 525 0
	std 26,-48(1)
	std 27,-40(1)
	std 28,-32(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	.cfi_register 65, 0
	.cfi_offset 23, -72
	.cfi_offset 24, -64
	.cfi_offset 26, -48
	.cfi_offset 27, -40
	.cfi_offset 28, -32
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	.loc 1 534 0
	addis 27,2,.LC9@toc@ha		# gpr load fusion, type long
	ld 27,.LC9@toc@l(27)
	.loc 1 525 0
	std 0,16(1)
	stdu 1,-176(1)
	.cfi_def_cfa_offset 176
	.cfi_offset 65, 16
	.loc 1 532 0
	ld 9,0(4)
	.loc 1 534 0
	ld 23,0(27)
.LVL50:
	.loc 1 532 0
	std 9,.LANCHOR1@toc@l(10)
	.loc 1 536 0
	bl next_int
.LVL51:
.LBB104:
.LBB105:
.LBB106:
.LBB107:
	.loc 1 249 0
	mulli 30,3,24
.LBE107:
.LBE106:
.LBE105:
.LBE104:
	.loc 1 536 0
	mr 26,3
.LVL52:
	.loc 1 537 0
	bl next_int
.LVL53:
	mr 24,3
.LVL54:
.LBB177:
.LBB174:
.LBB115:
.LBB116:
	.loc 1 249 0
	mulli 29,3,24
.LBE116:
.LBE115:
.LBE174:
.LBE177:
	.loc 1 540 0
	bl next_int
.LVL55:
	.loc 1 541 0
	bl next_int
.LVL56:
.LBB178:
.LBB175:
	.loc 1 309 0
	li 3,48
	bl xmalloc
.LVL57:
	mr 28,3
.LVL58:
.LBB120:
.LBB112:
	.loc 1 249 0
	mr 3,30
.LVL59:
.LBE112:
.LBE120:
	.loc 1 311 0
	stw 26,0(28)
	.loc 1 312 0
	stw 24,4(28)
.LVL60:
.LBB121:
.LBB113:
	.loc 1 249 0
	bl xmalloc
.LVL61:
.LBB108:
.LBB109:
	.file 4 "/usr/include/powerpc64le-linux-gnu/bits/string_fortified.h"
	.loc 4 71 0
	mr 5,30
	li 4,0
.LBE109:
.LBE108:
.LBE113:
.LBE121:
	.loc 1 318 0
	addi 30,30,-24
.LVL62:
.LBB122:
.LBB114:
.LBB111:
.LBB110:
	.loc 4 71 0
	bl memset
	nop
.LVL63:
.LBE110:
.LBE111:
.LBE114:
.LBE122:
	.loc 1 314 0
	std 3,8(28)
.LVL64:
.LBB123:
.LBB119:
	.loc 1 249 0
	mr 3,29
	bl xmalloc
.LVL65:
.LBB117:
.LBB118:
	.loc 4 71 0
	mr 5,29
	li 4,0
	bl memset
	nop
.LVL66:
.LBE118:
.LBE117:
.LBE119:
.LBE123:
	.loc 1 317 0
	ld 9,8(28)
	.loc 1 321 0
	cmpwi 7,24,0
	.loc 1 319 0
	li 10,0
	.loc 1 315 0
	std 3,16(28)
	.loc 1 319 0
	std 10,40(28)
.LVL67:
	.loc 1 318 0
	add 30,9,30
	.loc 1 317 0
	std 9,24(28)
	.loc 1 318 0
	std 30,32(28)
	.loc 1 321 0
	ble 7,.L39
	addi 24,24,-1
.LVL68:
	std 25,120(1)
	.cfi_offset 25, -56
	std 22,96(1)
	.cfi_offset 22, -80
	bl __ctype_b_loc
	nop
.LVL69:
	rldicl 24,24,0,32
	.loc 1 321 0
	li 25,0
	addi 24,24,1
.LVL70:
	mulli 24,24,24
.LVL71:
	mr 26,3
.LVL72:
	.p2align 4,,15
.L40:
.LBB124:
.LBB125:
	.loc 1 213 0
	li 31,0
	b .L48
.LVL73:
	.p2align 4,,15
.L41:
	.loc 1 215 0
	mulli 31,31,10
.LVL74:
	add 31,31,3
	addi 31,31,-48
	extsw 31,31
.LVL75:
.L48:
.LBB126:
.LBB127:
	.loc 2 46 0
	ld 3,0(27)
.LBE127:
.LBE126:
	.loc 1 214 0
	ld 30,0(26)
.LBB129:
.LBB128:
	.loc 2 46 0
	bl _IO_getc
	nop
.LVL76:
.LBE128:
.LBE129:
	.loc 1 214 0
	sldi 9,3,1
	lhzx 9,30,9
	andi. 9,9,0x800
	bne 0,.L41
.LBE125:
.LBE124:
.LBB130:
.LBB131:
	.loc 1 213 0
	li 30,0
	b .L42
.LVL77:
	.p2align 4,,15
.L43:
	.loc 1 215 0
	mulli 30,30,10
.LVL78:
	add 30,30,3
	addi 30,30,-48
	extsw 30,30
.LVL79:
.L42:
.LBB132:
.LBB133:
	.loc 2 46 0
	ld 3,0(27)
.LBE133:
.LBE132:
	.loc 1 214 0
	ld 29,0(26)
.LBB135:
.LBB134:
	.loc 2 46 0
	bl _IO_getc
	nop
.LVL80:
.LBE134:
.LBE135:
	.loc 1 214 0
	sldi 9,3,1
	lhzx 9,29,9
	andi. 9,9,0x800
	bne 0,.L43
.LBE131:
.LBE130:
.LBB136:
.LBB137:
	.loc 1 213 0
	li 29,0
	b .L44
.LVL81:
	.p2align 4,,15
.L45:
	.loc 1 215 0
	mulli 29,29,10
.LVL82:
	add 29,29,3
	addi 29,29,-48
	extsw 29,29
.LVL83:
.L44:
.LBB138:
.LBB139:
	.loc 2 46 0
	ld 3,0(27)
.LBE139:
.LBE138:
	.loc 1 214 0
	ld 22,0(26)
.LBB141:
.LBB140:
	.loc 2 46 0
	bl _IO_getc
	nop
.LVL84:
.LBE140:
.LBE141:
	.loc 1 214 0
	sldi 9,3,1
	lhzx 9,22,9
	andi. 9,9,0x800
	bne 0,.L45
.LVL85:
.LBE137:
.LBE136:
	.loc 1 325 0
	mulli 31,31,24
.LVL86:
	.loc 1 326 0
	mulli 30,30,24
.LVL87:
	.loc 1 325 0
	ld 9,8(28)
	.loc 1 327 0
	ld 10,16(28)
.LBB142:
.LBB143:
.LBB144:
.LBB145:
.LBB146:
.LBB147:
	.loc 1 237 0
	li 3,16
.LBE147:
.LBE146:
.LBE145:
.LBE144:
.LBE143:
.LBE142:
	.loc 1 327 0
	add 22,10,25
	.loc 1 325 0
	add 31,9,31
.LVL88:
	.loc 1 326 0
	add 30,9,30
.LVL89:
.LBB171:
.LBB168:
	.loc 1 291 0
	stdx 31,10,25
	.loc 1 292 0
	std 30,8(22)
	.loc 1 293 0
	stw 29,20(22)
.LVL90:
.LBB155:
.LBB152:
.LBB150:
.LBB148:
	.loc 1 237 0
	bl malloc
	nop
.LVL91:
	.loc 1 239 0
	mr. 9,3
	beq 0,.L47
.LVL92:
.LBE148:
.LBE150:
	.loc 1 280 0
	ld 10,8(31)
	.loc 1 279 0
	std 22,0(9)
.LBE152:
.LBE155:
.LBB156:
.LBB157:
.LBB158:
.LBB159:
	.loc 1 237 0
	li 3,16
.LBE159:
.LBE158:
.LBE157:
.LBE156:
.LBB164:
.LBB153:
	.loc 1 280 0
	std 10,8(9)
	.loc 1 281 0
	std 9,8(31)
.LVL93:
.LBE153:
.LBE164:
.LBB165:
.LBB162:
.LBB161:
.LBB160:
	.loc 1 237 0
	bl malloc
	nop
.LVL94:
	.loc 1 239 0
	cmpdi 0,3,0
	beq 0,.L47
.LVL95:
	addi 25,25,24
.LBE160:
.LBE161:
	.loc 1 280 0
	ld 10,8(30)
	.loc 1 279 0
	std 22,0(3)
.LBE162:
.LBE165:
.LBE168:
.LBE171:
	.loc 1 321 0
	cmpld 7,25,24
.LBB172:
.LBB169:
.LBB166:
.LBB163:
	.loc 1 280 0
	std 10,8(3)
	.loc 1 281 0
	std 3,8(30)
.LVL96:
.LBE163:
.LBE166:
.LBE169:
.LBE172:
	.loc 1 321 0
	bne 7,.L40
	ld 22,96(1)
	.cfi_restore 22
	ld 25,120(1)
	.cfi_restore 25
.LVL97:
.L39:
.LBE175:
.LBE178:
	.loc 1 545 0
	mr 3,23
	bl fclose
	nop
.LVL98:
.LBB179:
.LBB180:
	.loc 1 440 0
	ld 31,24(28)
.LVL99:
	.loc 1 441 0
	lwz 9,0(28)
	.loc 1 443 0
	ld 30,8(31)
.LVL100:
	.loc 1 441 0
	stw 9,0(31)
	.loc 1 450 0
	cmpdi 7,30,0
	bne 7,.L49
	b .L83
.LVL101:
	.p2align 4,,15
.L51:
	.loc 1 455 0
	mr 4,31
	mr 3,28
	bl push
.LVL102:
	.loc 1 450 0
	cmpdi 7,30,0
	beq 7,.L83
.L49:
	.loc 1 451 0
	ld 6,0(30)
.LVL103:
	.loc 1 454 0
	lwz 9,4(31)
	.loc 1 452 0
	ld 30,8(30)
.LVL104:
	.loc 1 455 0
	ld 5,0(6)
.LVL105:
	.loc 1 454 0
	lwz 10,20(6)
.LBB181:
.LBB182:
	.loc 1 425 0
	cmpld 7,31,5
.LBE182:
.LBE181:
	.loc 1 454 0
	add 9,9,10
	stw 9,4(31)
.LBB184:
.LBB183:
	.loc 1 425 0
	bne 7,.L51
.LVL106:
.LBE183:
.LBE184:
	.loc 1 455 0
	ld 5,8(6)
	mr 4,31
	mr 3,28
	bl push
.LVL107:
	.loc 1 450 0
	cmpdi 7,30,0
	bne 7,.L49
.L83:
	ld 4,40(28)
	cmpdi 5,4,0
.LVL108:
	.p2align 4,,15
.L53:
.LBB185:
.LBB186:
	.loc 1 361 0
	beq 5,.L60
.L84:
	lwa 7,0(4)
.LBE186:
.LBE185:
	.loc 1 476 0
	ld 9,8(4)
.LBB190:
.LBB187:
	.loc 1 362 0
	ld 11,16(4)
.LBE187:
.LBE190:
	.loc 1 478 0
	cmpdi 7,9,0
.LBB191:
.LBB188:
	.loc 1 362 0
	std 11,40(28)
.LVL109:
.LBE188:
.LBE191:
	.loc 1 478 0
	bne 7,.L57
	b .L58
.LVL110:
	.p2align 4,,15
.L54:
	.loc 1 490 0
	lwz 10,0(5)
	cmpw 7,10,7
	bge 7,.L55
	lwz 10,16(6)
	lwz 3,20(6)
	mullw 10,10,8
	cmpw 7,10,3
	blt 7,.L56
.L55:
.LVL111:
	.loc 1 478 0
	cmpdi 7,9,0
	beq 7,.L58
.LVL112:
.L57:
	.loc 1 479 0
	ld 6,0(9)
.LVL113:
	.loc 1 487 0
	li 8,-1
	.loc 1 480 0
	ld 9,8(9)
.LVL114:
	.loc 1 482 0
	ld 5,0(6)
	cmpld 7,5,4
	bne 7,.L54
	.loc 1 483 0
	ld 5,8(6)
.LVL115:
	.loc 1 484 0
	li 8,1
	b .L54
.LVL116:
	.p2align 4,,15
.L58:
.LBB192:
.LBB193:
	.loc 1 414 0
	addis 9,2,.LC10@toc@ha		# gpr load fusion, type long
	ld 9,.LC10@toc@l(9)
.LVL117:
.LBB194:
.LBB195:
	.loc 1 344 0
	ld 10,32(28)
.LBE195:
.LBE194:
	.loc 1 415 0
	addi 7,7,1
	.loc 1 414 0
	lwz 8,0(9)
.LBB198:
.LBB196:
	.loc 1 344 0
	cmpd 7,10,4
.LBE196:
.LBE198:
	.loc 1 414 0
	andi. 10,8,0x1234
	stw 10,0(9)
	.loc 1 415 0
	stw 7,0(4)
	.loc 1 416 0
	ori 2,2,0
	lwz 10,0(9)
	andi. 10,10,0x5678
	stw 10,0(9)
.LBB199:
.LBB197:
	.loc 1 344 0
	beq 7,.L68
	ld 9,24(28)
	cmpd 7,9,4
	beq 7,.L68
	.loc 1 345 0
	std 11,16(4)
	.loc 1 346 0
	std 4,40(28)
.LBE197:
.LBE199:
.LBE193:
.LBE192:
.LBB200:
.LBB189:
	.loc 1 361 0
	bne 5,.L84
.LVL118:
	.p2align 4,,15
.L60:
.LBE189:
.LBE200:
	.loc 1 502 0
	ld 9,32(28)
.LBE180:
.LBE179:
.LBB202:
.LBB203:
	.loc 3 104 0
	addis 4,2,.LC11@toc@ha
	li 3,1
	addi 4,4,.LC11@toc@l
.LBE203:
.LBE202:
.LBB205:
.LBB206:
	.loc 1 511 0
	li 29,0
	li 30,0
.LBE206:
.LBE205:
.LBB208:
.LBB204:
	.loc 3 104 0
	lwa 5,4(9)
	bl __printf_chk
	nop
.LVL119:
.LBE204:
.LBE208:
.LBB209:
.LBB207:
	.loc 1 511 0
	lwa 10,0(28)
	ld 8,8(28)
	cmpwi 7,10,0
	ble 7,.L66
.LVL120:
.L63:
	.loc 1 512 0
	add 9,8,29
	ld 3,8(9)
.LVL121:
	.loc 1 513 0
	cmpdi 7,3,0
	beq 7,.L61
	.p2align 5,,31
.L62:
	.loc 1 514 0
	ld 31,8(3)
.LVL122:
	.loc 1 515 0
	bl free
	nop
.LVL123:
	.loc 1 513 0
	cmpdi 7,31,0
	mr 3,31
	bne 7,.L62
	lwa 10,0(28)
	ld 8,8(28)
.LVL124:
.L61:
	.loc 1 511 0
	addi 9,30,1
	cmpw 7,9,10
	addi 29,29,24
	extsw 30,9
.LVL125:
	blt 7,.L63
.LVL126:
.L66:
	.loc 1 519 0
	mr 3,8
	bl free
	nop
.LVL127:
	.loc 1 520 0
	ld 3,16(28)
	bl free
	nop
.LVL128:
	.loc 1 521 0
	mr 3,28
	bl free
	nop
.LVL129:
.LBE207:
.LBE209:
	.loc 1 554 0
	addi 1,1,176
	.cfi_remember_state
	.cfi_def_cfa_offset 0
	li 3,0
	ld 0,16(1)
	ld 23,-72(1)
.LVL130:
	ld 24,-64(1)
	ld 26,-48(1)
	ld 27,-40(1)
	ld 28,-32(1)
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	mtlr 0
	.cfi_restore 65
	.cfi_restore 31
	.cfi_restore 30
	.cfi_restore 29
	.cfi_restore 28
	.cfi_restore 27
	.cfi_restore 26
	.cfi_restore 24
	.cfi_restore 23
	blr
.LVL131:
	.p2align 4,,15
.L56:
	.cfi_restore_state
.LBB210:
.LBB201:
	.loc 1 497 0
	mr 3,28
	bl push
.LVL132:
	ld 4,40(28)
	cmpdi 5,4,0
	b .L53
.LVL133:
	.p2align 4,,15
.L68:
	mr 4,11
	cmpdi 5,11,0
	b .L53
.LVL134:
.L47:
	.cfi_offset 22, -80
	.cfi_offset 25, -56
.LBE201:
.LBE210:
.LBB211:
.LBB176:
.LBB173:
.LBB170:
.LBB167:
.LBB154:
.LBB151:
.LBB149:
	.loc 1 240 0
	addis 3,2,.LC8@toc@ha
	li 4,16
	addi 3,3,.LC8@toc@l
	bl error
.LVL135:
.LBE149:
.LBE151:
.LBE154:
.LBE167:
.LBE170:
.LBE173:
.LBE176:
.LBE211:
	.long 0
	.byte 0,0,0,1,128,10,0,0
	.cfi_endproc
.LFE68:
	.size	main,.-main
	.comm	hello,4,4
	.section	.rodata
	.align 3
	.set	.LANCHOR0,. + 0
	.type	__PRETTY_FUNCTION__.4363, @object
	.size	__PRETTY_FUNCTION__.4363, 5
__PRETTY_FUNCTION__.4363:
	.string	"push"
	.section	".bss"
	.align 3
	.set	.LANCHOR1,. + 0
	.type	progname, @object
	.size	progname, 8
progname:
	.zero	8
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 3
.LC1:
	.string	"sequential.c"
	.zero	3
.LC2:
	.string	"d >= 0"
	.zero	1
.LC3:
	.string	"u->e >= 0"
	.zero	6
.LC4:
	.string	"abs(e->f) <= e->c"
	.zero	6
.LC5:
	.string	"%s: "
	.zero	3
.LC7:
	.string	"error: %s\n"
	.zero	5
.LC8:
	.string	"out of memory: malloc(%zu) failed"
	.zero	6
.LC11:
	.string	"f = %d\n"
	.section	".text"
.Letext0:
	.file 5 "/usr/include/powerpc64le-linux-gnu/bits/types.h"
	.file 6 "/usr/lib/gcc/powerpc64le-linux-gnu/7/include/stdarg.h"
	.file 7 "/usr/lib/gcc/powerpc64le-linux-gnu/7/include/stddef.h"
	.file 8 "/usr/include/powerpc64le-linux-gnu/bits/libio.h"
	.file 9 "/usr/include/powerpc64le-linux-gnu/bits/types/FILE.h"
	.file 10 "/usr/include/stdio.h"
	.file 11 "/usr/include/powerpc64le-linux-gnu/bits/sys_errlist.h"
	.file 12 "/usr/include/stdlib.h"
	.file 13 "/usr/include/ctype.h"
	.file 14 "/usr/include/assert.h"
	.file 15 "<built-in>"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.4byte	0x1360
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.4byte	.LASF117
	.byte	0xc
	.4byte	.LASF118
	.4byte	.LASF119
	.4byte	.Ldebug_ranges0+0x4e0
	.8byte	0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x2
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x5
	.4byte	0x5f
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x6
	.4byte	.LASF11
	.byte	0x5
	.byte	0x8c
	.4byte	0x6b
	.uleb128 0x6
	.4byte	.LASF12
	.byte	0x5
	.byte	0x8d
	.4byte	0x6b
	.uleb128 0x7
	.byte	0x8
	.uleb128 0x8
	.byte	0x8
	.4byte	0x37
	.uleb128 0x9
	.4byte	0x9f
	.uleb128 0xa
	.byte	0x7
	.byte	0x4
	.4byte	0x29
	.byte	0xd
	.byte	0x2f
	.4byte	0x108
	.uleb128 0xb
	.4byte	.LASF13
	.2byte	0x100
	.uleb128 0xb
	.4byte	.LASF14
	.2byte	0x200
	.uleb128 0xb
	.4byte	.LASF15
	.2byte	0x400
	.uleb128 0xb
	.4byte	.LASF16
	.2byte	0x800
	.uleb128 0xb
	.4byte	.LASF17
	.2byte	0x1000
	.uleb128 0xb
	.4byte	.LASF18
	.2byte	0x2000
	.uleb128 0xb
	.4byte	.LASF19
	.2byte	0x4000
	.uleb128 0xb
	.4byte	.LASF20
	.2byte	0x8000
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x2
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x4
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x8
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.4byte	0x3e
	.uleb128 0x3
	.4byte	0x108
	.uleb128 0x9
	.4byte	0x108
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x6
	.byte	0x28
	.4byte	0x123
	.uleb128 0xd
	.byte	0x8
	.4byte	.LASF120
	.4byte	0x37
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x6
	.byte	0x63
	.4byte	0x118
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x7
	.byte	0xd8
	.4byte	0x72
	.uleb128 0xe
	.4byte	.LASF58
	.byte	0xd8
	.byte	0x8
	.byte	0xf5
	.4byte	0x2c3
	.uleb128 0xf
	.4byte	.LASF28
	.byte	0x8
	.byte	0xf6
	.4byte	0x5f
	.byte	0
	.uleb128 0xf
	.4byte	.LASF29
	.byte	0x8
	.byte	0xfb
	.4byte	0x9f
	.byte	0x8
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x8
	.byte	0xfc
	.4byte	0x9f
	.byte	0x10
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x8
	.byte	0xfd
	.4byte	0x9f
	.byte	0x18
	.uleb128 0xf
	.4byte	.LASF32
	.byte	0x8
	.byte	0xfe
	.4byte	0x9f
	.byte	0x20
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x8
	.byte	0xff
	.4byte	0x9f
	.byte	0x28
	.uleb128 0x10
	.4byte	.LASF34
	.byte	0x8
	.2byte	0x100
	.4byte	0x9f
	.byte	0x30
	.uleb128 0x10
	.4byte	.LASF35
	.byte	0x8
	.2byte	0x101
	.4byte	0x9f
	.byte	0x38
	.uleb128 0x10
	.4byte	.LASF36
	.byte	0x8
	.2byte	0x102
	.4byte	0x9f
	.byte	0x40
	.uleb128 0x10
	.4byte	.LASF37
	.byte	0x8
	.2byte	0x104
	.4byte	0x9f
	.byte	0x48
	.uleb128 0x10
	.4byte	.LASF38
	.byte	0x8
	.2byte	0x105
	.4byte	0x9f
	.byte	0x50
	.uleb128 0x10
	.4byte	.LASF39
	.byte	0x8
	.2byte	0x106
	.4byte	0x9f
	.byte	0x58
	.uleb128 0x10
	.4byte	.LASF40
	.byte	0x8
	.2byte	0x108
	.4byte	0x306
	.byte	0x60
	.uleb128 0x10
	.4byte	.LASF41
	.byte	0x8
	.2byte	0x10a
	.4byte	0x30c
	.byte	0x68
	.uleb128 0x10
	.4byte	.LASF42
	.byte	0x8
	.2byte	0x10c
	.4byte	0x5f
	.byte	0x70
	.uleb128 0x10
	.4byte	.LASF43
	.byte	0x8
	.2byte	0x110
	.4byte	0x5f
	.byte	0x74
	.uleb128 0x10
	.4byte	.LASF44
	.byte	0x8
	.2byte	0x112
	.4byte	0x87
	.byte	0x78
	.uleb128 0x10
	.4byte	.LASF45
	.byte	0x8
	.2byte	0x116
	.4byte	0x58
	.byte	0x80
	.uleb128 0x10
	.4byte	.LASF46
	.byte	0x8
	.2byte	0x117
	.4byte	0x43
	.byte	0x82
	.uleb128 0x10
	.4byte	.LASF47
	.byte	0x8
	.2byte	0x118
	.4byte	0x312
	.byte	0x83
	.uleb128 0x10
	.4byte	.LASF48
	.byte	0x8
	.2byte	0x11c
	.4byte	0x322
	.byte	0x88
	.uleb128 0x10
	.4byte	.LASF49
	.byte	0x8
	.2byte	0x125
	.4byte	0x92
	.byte	0x90
	.uleb128 0x10
	.4byte	.LASF50
	.byte	0x8
	.2byte	0x12d
	.4byte	0x9d
	.byte	0x98
	.uleb128 0x10
	.4byte	.LASF51
	.byte	0x8
	.2byte	0x12e
	.4byte	0x9d
	.byte	0xa0
	.uleb128 0x10
	.4byte	.LASF52
	.byte	0x8
	.2byte	0x12f
	.4byte	0x9d
	.byte	0xa8
	.uleb128 0x10
	.4byte	.LASF53
	.byte	0x8
	.2byte	0x130
	.4byte	0x9d
	.byte	0xb0
	.uleb128 0x10
	.4byte	.LASF54
	.byte	0x8
	.2byte	0x132
	.4byte	0x138
	.byte	0xb8
	.uleb128 0x10
	.4byte	.LASF55
	.byte	0x8
	.2byte	0x133
	.4byte	0x5f
	.byte	0xc0
	.uleb128 0x10
	.4byte	.LASF56
	.byte	0x8
	.2byte	0x135
	.4byte	0x328
	.byte	0xc4
	.byte	0
	.uleb128 0x6
	.4byte	.LASF57
	.byte	0x9
	.byte	0x7
	.4byte	0x143
	.uleb128 0x11
	.4byte	.LASF121
	.byte	0x8
	.byte	0x9a
	.uleb128 0xe
	.4byte	.LASF59
	.byte	0x18
	.byte	0x8
	.byte	0xa0
	.4byte	0x306
	.uleb128 0xf
	.4byte	.LASF60
	.byte	0x8
	.byte	0xa1
	.4byte	0x306
	.byte	0
	.uleb128 0xf
	.4byte	.LASF61
	.byte	0x8
	.byte	0xa2
	.4byte	0x30c
	.byte	0x8
	.uleb128 0xf
	.4byte	.LASF62
	.byte	0x8
	.byte	0xa6
	.4byte	0x5f
	.byte	0x10
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.4byte	0x2d5
	.uleb128 0x8
	.byte	0x8
	.4byte	0x143
	.uleb128 0x12
	.4byte	0x37
	.4byte	0x322
	.uleb128 0x13
	.4byte	0x72
	.byte	0
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.4byte	0x2ce
	.uleb128 0x12
	.4byte	0x37
	.4byte	0x338
	.uleb128 0x13
	.4byte	0x72
	.byte	0x13
	.byte	0
	.uleb128 0x14
	.4byte	.LASF122
	.uleb128 0x15
	.4byte	.LASF63
	.byte	0x8
	.2byte	0x13f
	.4byte	0x338
	.uleb128 0x15
	.4byte	.LASF64
	.byte	0x8
	.2byte	0x140
	.4byte	0x338
	.uleb128 0x15
	.4byte	.LASF65
	.byte	0x8
	.2byte	0x141
	.4byte	0x338
	.uleb128 0x16
	.4byte	.LASF66
	.byte	0xa
	.byte	0x87
	.4byte	0x30c
	.uleb128 0x16
	.4byte	.LASF67
	.byte	0xa
	.byte	0x88
	.4byte	0x30c
	.uleb128 0x16
	.4byte	.LASF68
	.byte	0xa
	.byte	0x89
	.4byte	0x30c
	.uleb128 0x16
	.4byte	.LASF69
	.byte	0xb
	.byte	0x1a
	.4byte	0x5f
	.uleb128 0x12
	.4byte	0x10e
	.4byte	0x398
	.uleb128 0x17
	.byte	0
	.uleb128 0x3
	.4byte	0x38d
	.uleb128 0x16
	.4byte	.LASF70
	.byte	0xb
	.byte	0x1b
	.4byte	0x398
	.uleb128 0x6
	.4byte	.LASF71
	.byte	0x1
	.byte	0x41
	.4byte	0x3b3
	.uleb128 0xe
	.4byte	.LASF71
	.byte	0x30
	.byte	0x1
	.byte	0x59
	.4byte	0x408
	.uleb128 0x18
	.string	"n"
	.byte	0x1
	.byte	0x5a
	.4byte	0x5f
	.byte	0
	.uleb128 0x18
	.string	"m"
	.byte	0x1
	.byte	0x5b
	.4byte	0x5f
	.byte	0x4
	.uleb128 0x18
	.string	"v"
	.byte	0x1
	.byte	0x5c
	.4byte	0x4c8
	.byte	0x8
	.uleb128 0x18
	.string	"e"
	.byte	0x1
	.byte	0x5d
	.4byte	0x4bc
	.byte	0x10
	.uleb128 0x18
	.string	"s"
	.byte	0x1
	.byte	0x5e
	.4byte	0x4c8
	.byte	0x18
	.uleb128 0x18
	.string	"t"
	.byte	0x1
	.byte	0x5f
	.4byte	0x4c8
	.byte	0x20
	.uleb128 0xf
	.4byte	.LASF72
	.byte	0x1
	.byte	0x60
	.4byte	0x4c8
	.byte	0x28
	.byte	0
	.uleb128 0x6
	.4byte	.LASF73
	.byte	0x1
	.byte	0x42
	.4byte	0x413
	.uleb128 0xe
	.4byte	.LASF73
	.byte	0x18
	.byte	0x1
	.byte	0x4b
	.4byte	0x44c
	.uleb128 0x18
	.string	"h"
	.byte	0x1
	.byte	0x4c
	.4byte	0x5f
	.byte	0
	.uleb128 0x18
	.string	"e"
	.byte	0x1
	.byte	0x4d
	.4byte	0x5f
	.byte	0x4
	.uleb128 0xf
	.4byte	.LASF74
	.byte	0x1
	.byte	0x4e
	.4byte	0x4c2
	.byte	0x8
	.uleb128 0xf
	.4byte	.LASF75
	.byte	0x1
	.byte	0x4f
	.4byte	0x4c8
	.byte	0x10
	.byte	0
	.uleb128 0x6
	.4byte	.LASF76
	.byte	0x1
	.byte	0x43
	.4byte	0x457
	.uleb128 0xe
	.4byte	.LASF76
	.byte	0x18
	.byte	0x1
	.byte	0x52
	.4byte	0x48c
	.uleb128 0x18
	.string	"u"
	.byte	0x1
	.byte	0x53
	.4byte	0x4c8
	.byte	0
	.uleb128 0x18
	.string	"v"
	.byte	0x1
	.byte	0x54
	.4byte	0x4c8
	.byte	0x8
	.uleb128 0x18
	.string	"f"
	.byte	0x1
	.byte	0x55
	.4byte	0x5f
	.byte	0x10
	.uleb128 0x18
	.string	"c"
	.byte	0x1
	.byte	0x56
	.4byte	0x5f
	.byte	0x14
	.byte	0
	.uleb128 0x6
	.4byte	.LASF77
	.byte	0x1
	.byte	0x44
	.4byte	0x497
	.uleb128 0xe
	.4byte	.LASF77
	.byte	0x10
	.byte	0x1
	.byte	0x46
	.4byte	0x4bc
	.uleb128 0xf
	.4byte	.LASF74
	.byte	0x1
	.byte	0x47
	.4byte	0x4bc
	.byte	0
	.uleb128 0xf
	.4byte	.LASF75
	.byte	0x1
	.byte	0x48
	.4byte	0x4c2
	.byte	0x8
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.4byte	0x44c
	.uleb128 0x8
	.byte	0x8
	.4byte	0x48c
	.uleb128 0x8
	.byte	0x8
	.4byte	0x408
	.uleb128 0x19
	.4byte	.LASF81
	.byte	0x1
	.byte	0x7a
	.4byte	0x9f
	.uleb128 0x9
	.byte	0x3
	.8byte	progname
	.uleb128 0x1a
	.4byte	.LASF78
	.byte	0x1
	.2byte	0x19b
	.4byte	0x66
	.uleb128 0x9
	.byte	0x3
	.8byte	hello
	.uleb128 0x1b
	.4byte	.LASF123
	.byte	0x1
	.2byte	0x20c
	.4byte	0x5f
	.8byte	.LFB68
	.8byte	.LFE68-.LFB68
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xbd9
	.uleb128 0x1c
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x20c
	.4byte	0x5f
	.4byte	.LLST19
	.uleb128 0x1c
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x20c
	.4byte	0xbd9
	.4byte	.LLST20
	.uleb128 0x1d
	.string	"in"
	.byte	0x1
	.2byte	0x20e
	.4byte	0xbdf
	.4byte	.LLST21
	.uleb128 0x1e
	.string	"g"
	.byte	0x1
	.2byte	0x20f
	.4byte	0xbea
	.uleb128 0x1e
	.string	"f"
	.byte	0x1
	.2byte	0x210
	.4byte	0x5f
	.uleb128 0x1d
	.string	"n"
	.byte	0x1
	.2byte	0x211
	.4byte	0x5f
	.4byte	.LLST22
	.uleb128 0x1d
	.string	"m"
	.byte	0x1
	.2byte	0x212
	.4byte	0x5f
	.4byte	.LLST23
	.uleb128 0x1f
	.4byte	0xeb1
	.8byte	.LBB104
	.4byte	.Ldebug_ranges0+0xb0
	.byte	0x1
	.2byte	0x21f
	.4byte	0x97e
	.uleb128 0x20
	.4byte	0xec2
	.4byte	.LLST24
	.uleb128 0x20
	.4byte	0xed7
	.4byte	.LLST25
	.uleb128 0x20
	.4byte	0xecd
	.4byte	.LLST26
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0xb0
	.uleb128 0x22
	.4byte	0xee1
	.4byte	.LLST27
	.uleb128 0x22
	.4byte	0xeeb
	.4byte	.LLST28
	.uleb128 0x22
	.4byte	0xef5
	.4byte	.LLST29
	.uleb128 0x22
	.4byte	0xeff
	.4byte	.LLST30
	.uleb128 0x23
	.4byte	0xf09
	.uleb128 0x23
	.4byte	0xf13
	.uleb128 0x23
	.4byte	0xf1d
	.uleb128 0x1f
	.4byte	0xf8a
	.8byte	.LBB106
	.4byte	.Ldebug_ranges0+0x100
	.byte	0x1
	.2byte	0x13a
	.4byte	0x67f
	.uleb128 0x20
	.4byte	0xfa3
	.4byte	.LLST31
	.uleb128 0x20
	.4byte	0xf9a
	.4byte	.LLST32
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x100
	.uleb128 0x22
	.4byte	0xfac
	.4byte	.LLST33
	.uleb128 0x24
	.4byte	0x1166
	.8byte	.LBB108
	.4byte	.Ldebug_ranges0+0x150
	.byte	0x1
	.byte	0xfc
	.4byte	0x669
	.uleb128 0x20
	.4byte	0x118c
	.4byte	.LLST34
	.uleb128 0x20
	.4byte	0x1181
	.4byte	.LLST35
	.uleb128 0x20
	.4byte	0x1176
	.4byte	.LLST33
	.uleb128 0x25
	.8byte	.LVL63
	.4byte	0x12de
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x30
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x8e
	.sleb128 24
	.byte	0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL61
	.4byte	0xfb6
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8e
	.sleb128 0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	0xf8a
	.8byte	.LBB115
	.4byte	.Ldebug_ranges0+0x180
	.byte	0x1
	.2byte	0x13b
	.4byte	0x71d
	.uleb128 0x20
	.4byte	0xfa3
	.4byte	.LLST37
	.uleb128 0x20
	.4byte	0xf9a
	.4byte	.LLST38
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x180
	.uleb128 0x22
	.4byte	0xfac
	.4byte	.LLST39
	.uleb128 0x27
	.4byte	0x1166
	.8byte	.LBB117
	.8byte	.LBE117-.LBB117
	.byte	0x1
	.byte	0xfc
	.4byte	0x707
	.uleb128 0x20
	.4byte	0x118c
	.4byte	.LLST40
	.uleb128 0x20
	.4byte	0x1181
	.4byte	.LLST41
	.uleb128 0x20
	.4byte	0x1176
	.4byte	.LLST39
	.uleb128 0x25
	.8byte	.LVL66
	.4byte	0x12de
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x30
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x8d
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL65
	.4byte	0xfb6
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8d
	.sleb128 0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x28
	.4byte	0xfd9
	.8byte	.LBB124
	.8byte	.LBE124-.LBB124
	.byte	0x1
	.2byte	0x142
	.4byte	0x777
	.uleb128 0x29
	.8byte	.LBB125
	.8byte	.LBE125-.LBB125
	.uleb128 0x23
	.4byte	0x1236
	.uleb128 0x23
	.4byte	0x123f
	.uleb128 0x2a
	.4byte	0x120f
	.8byte	.LBB126
	.4byte	.Ldebug_ranges0+0x1b0
	.byte	0x1
	.byte	0xd6
	.uleb128 0x2b
	.8byte	.LVL76
	.4byte	0x12ed
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x28
	.4byte	0xfd9
	.8byte	.LBB130
	.8byte	.LBE130-.LBB130
	.byte	0x1
	.2byte	0x143
	.4byte	0x7d1
	.uleb128 0x29
	.8byte	.LBB131
	.8byte	.LBE131-.LBB131
	.uleb128 0x23
	.4byte	0x1236
	.uleb128 0x23
	.4byte	0x123f
	.uleb128 0x2a
	.4byte	0x120f
	.8byte	.LBB132
	.4byte	.Ldebug_ranges0+0x1e0
	.byte	0x1
	.byte	0xd6
	.uleb128 0x2b
	.8byte	.LVL80
	.4byte	0x12ed
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x28
	.4byte	0xfd9
	.8byte	.LBB136
	.8byte	.LBE136-.LBB136
	.byte	0x1
	.2byte	0x144
	.4byte	0x82b
	.uleb128 0x29
	.8byte	.LBB137
	.8byte	.LBE137-.LBB137
	.uleb128 0x23
	.4byte	0x1236
	.uleb128 0x23
	.4byte	0x123f
	.uleb128 0x2a
	.4byte	0x120f
	.8byte	.LBB138
	.4byte	.Ldebug_ranges0+0x210
	.byte	0x1
	.byte	0xd6
	.uleb128 0x2b
	.8byte	.LVL84
	.4byte	0x12ed
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	0xf28
	.8byte	.LBB142
	.4byte	.Ldebug_ranges0+0x240
	.byte	0x1
	.2byte	0x147
	.4byte	0x968
	.uleb128 0x20
	.4byte	0xf53
	.4byte	.LLST43
	.uleb128 0x2c
	.4byte	0xf49
	.uleb128 0x20
	.4byte	0xf3f
	.4byte	.LLST44
	.uleb128 0x20
	.4byte	0xf35
	.4byte	.LLST45
	.uleb128 0x1f
	.4byte	0xf5e
	.8byte	.LBB144
	.4byte	.Ldebug_ranges0+0x290
	.byte	0x1
	.2byte	0x127
	.4byte	0x8f9
	.uleb128 0x20
	.4byte	0xf6b
	.4byte	.LLST46
	.uleb128 0x20
	.4byte	0xf75
	.4byte	.LLST47
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x290
	.uleb128 0x23
	.4byte	0xf7f
	.uleb128 0x2d
	.4byte	0xfb6
	.8byte	.LBB146
	.4byte	.Ldebug_ranges0+0x2e0
	.byte	0x1
	.2byte	0x116
	.uleb128 0x20
	.4byte	0xfc6
	.4byte	.LLST48
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x2e0
	.uleb128 0x23
	.4byte	0x129b
	.uleb128 0x2e
	.8byte	.LVL91
	.4byte	0x12f9
	.4byte	0x8d5
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x1
	.byte	0x40
	.byte	0
	.uleb128 0x25
	.8byte	.LVL135
	.4byte	0xffc
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC8
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x40
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2d
	.4byte	0xf5e
	.8byte	.LBB156
	.4byte	.Ldebug_ranges0+0x320
	.byte	0x1
	.2byte	0x128
	.uleb128 0x20
	.4byte	0xf6b
	.4byte	.LLST49
	.uleb128 0x20
	.4byte	0xf75
	.4byte	.LLST50
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x320
	.uleb128 0x23
	.4byte	0xf7f
	.uleb128 0x2d
	.4byte	0xfb6
	.8byte	.LBB158
	.4byte	.Ldebug_ranges0+0x360
	.byte	0x1
	.2byte	0x116
	.uleb128 0x20
	.4byte	0xfc6
	.4byte	.LLST51
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x360
	.uleb128 0x23
	.4byte	0x129b
	.uleb128 0x25
	.8byte	.LVL94
	.4byte	0x12f9
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x1
	.byte	0x40
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL57
	.4byte	0xfb6
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8
	.byte	0x30
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	0xc26
	.8byte	.LBB179
	.4byte	.Ldebug_ranges0+0x390
	.byte	0x1
	.2byte	0x223
	.4byte	0xac7
	.uleb128 0x2c
	.4byte	0xc37
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x390
	.uleb128 0x22
	.4byte	0xc41
	.4byte	.LLST52
	.uleb128 0x23
	.4byte	0xc4b
	.uleb128 0x22
	.4byte	0xc55
	.4byte	.LLST53
	.uleb128 0x22
	.4byte	0xc5f
	.4byte	.LLST54
	.uleb128 0x22
	.4byte	0xc69
	.4byte	.LLST55
	.uleb128 0x22
	.4byte	0xc73
	.4byte	.LLST56
	.uleb128 0x1f
	.4byte	0xc7e
	.8byte	.LBB181
	.4byte	.Ldebug_ranges0+0x3c0
	.byte	0x1
	.2byte	0x1c7
	.4byte	0xa06
	.uleb128 0x20
	.4byte	0xc99
	.4byte	.LLST57
	.uleb128 0x20
	.4byte	0xc99
	.4byte	.LLST57
	.uleb128 0x20
	.4byte	0xc8f
	.4byte	.LLST59
	.byte	0
	.uleb128 0x1f
	.4byte	0xe69
	.8byte	.LBB185
	.4byte	.Ldebug_ranges0+0x3f0
	.byte	0x1
	.2byte	0x1cc
	.4byte	0xa2f
	.uleb128 0x2c
	.4byte	0xe7a
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x3f0
	.uleb128 0x23
	.4byte	0xe84
	.byte	0
	.byte	0
	.uleb128 0x28
	.4byte	0xca4
	.8byte	.LBB192
	.8byte	.LBE192-.LBB192
	.byte	0x1
	.2byte	0x1f3
	.4byte	0xa75
	.uleb128 0x2c
	.4byte	0xcbb
	.uleb128 0x2c
	.4byte	0xcb1
	.uleb128 0x2d
	.4byte	0xe8f
	.8byte	.LBB194
	.4byte	.Ldebug_ranges0+0x440
	.byte	0x1
	.2byte	0x1a4
	.uleb128 0x2c
	.4byte	0xea6
	.uleb128 0x2c
	.4byte	0xe9c
	.byte	0
	.byte	0
	.uleb128 0x2e
	.8byte	.LVL102
	.4byte	0xcc6
	.4byte	0xa93
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8c
	.sleb128 0
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.byte	0
	.uleb128 0x2e
	.8byte	.LVL107
	.4byte	0xcc6
	.4byte	0xab1
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8c
	.sleb128 0
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL132
	.4byte	0xcc6
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8c
	.sleb128 0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	0x1198
	.8byte	.LBB202
	.4byte	.Ldebug_ranges0+0x480
	.byte	0x1
	.2byte	0x225
	.4byte	0xb09
	.uleb128 0x20
	.4byte	0x11a8
	.4byte	.LLST60
	.uleb128 0x25
	.8byte	.LVL119
	.4byte	0x1305
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC11
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	0xbf0
	.8byte	.LBB205
	.4byte	.Ldebug_ranges0+0x4b0
	.byte	0x1
	.2byte	0x227
	.4byte	0xb83
	.uleb128 0x2c
	.4byte	0xbfd
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x4b0
	.uleb128 0x22
	.4byte	0xc07
	.4byte	.LLST61
	.uleb128 0x22
	.4byte	0xc11
	.4byte	.LLST62
	.uleb128 0x22
	.4byte	0xc1b
	.4byte	.LLST63
	.uleb128 0x2b
	.8byte	.LVL123
	.4byte	0x1310
	.uleb128 0x2b
	.8byte	.LVL127
	.4byte	0x1310
	.uleb128 0x2b
	.8byte	.LVL128
	.4byte	0x1310
	.uleb128 0x25
	.8byte	.LVL129
	.4byte	0x1310
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8c
	.sleb128 0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2b
	.8byte	.LVL51
	.4byte	0xfd9
	.uleb128 0x2b
	.8byte	.LVL53
	.4byte	0xfd9
	.uleb128 0x2b
	.8byte	.LVL55
	.4byte	0xfd9
	.uleb128 0x2b
	.8byte	.LVL56
	.4byte	0xfd9
	.uleb128 0x2b
	.8byte	.LVL69
	.4byte	0x131c
	.uleb128 0x25
	.8byte	.LVL98
	.4byte	0x1327
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x87
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.4byte	0x9f
	.uleb128 0x8
	.byte	0x8
	.4byte	0x2c3
	.uleb128 0x9
	.4byte	0xbdf
	.uleb128 0x8
	.byte	0x8
	.4byte	0x3a8
	.uleb128 0x2f
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x1f9
	.byte	0x1
	.4byte	0xc26
	.uleb128 0x30
	.string	"g"
	.byte	0x1
	.2byte	0x1f9
	.4byte	0xbea
	.uleb128 0x1e
	.string	"i"
	.byte	0x1
	.2byte	0x1fb
	.4byte	0x5f
	.uleb128 0x1e
	.string	"p"
	.byte	0x1
	.2byte	0x1fc
	.4byte	0x4c2
	.uleb128 0x1e
	.string	"q"
	.byte	0x1
	.2byte	0x1fd
	.4byte	0x4c2
	.byte	0
	.uleb128 0x31
	.4byte	.LASF82
	.byte	0x1
	.2byte	0x1af
	.4byte	0x5f
	.byte	0x1
	.4byte	0xc7e
	.uleb128 0x30
	.string	"g"
	.byte	0x1
	.2byte	0x1af
	.4byte	0xbea
	.uleb128 0x1e
	.string	"s"
	.byte	0x1
	.2byte	0x1b1
	.4byte	0x4c8
	.uleb128 0x1e
	.string	"u"
	.byte	0x1
	.2byte	0x1b2
	.4byte	0x4c8
	.uleb128 0x1e
	.string	"v"
	.byte	0x1
	.2byte	0x1b3
	.4byte	0x4c8
	.uleb128 0x1e
	.string	"e"
	.byte	0x1
	.2byte	0x1b4
	.4byte	0x4bc
	.uleb128 0x1e
	.string	"p"
	.byte	0x1
	.2byte	0x1b5
	.4byte	0x4c2
	.uleb128 0x1e
	.string	"b"
	.byte	0x1
	.2byte	0x1b6
	.4byte	0x5f
	.byte	0
	.uleb128 0x31
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x1a7
	.4byte	0x4c8
	.byte	0x1
	.4byte	0xca4
	.uleb128 0x30
	.string	"u"
	.byte	0x1
	.2byte	0x1a7
	.4byte	0x4c8
	.uleb128 0x30
	.string	"e"
	.byte	0x1
	.2byte	0x1a7
	.4byte	0x4bc
	.byte	0
	.uleb128 0x2f
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x19c
	.byte	0x1
	.4byte	0xcc6
	.uleb128 0x30
	.string	"g"
	.byte	0x1
	.2byte	0x19c
	.4byte	0xbea
	.uleb128 0x30
	.string	"u"
	.byte	0x1
	.2byte	0x19c
	.4byte	0x4c8
	.byte	0
	.uleb128 0x32
	.4byte	.LASF124
	.byte	0x1
	.2byte	0x16f
	.8byte	.LFB63
	.8byte	.LFE63-.LFB63
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xe54
	.uleb128 0x33
	.string	"g"
	.byte	0x1
	.2byte	0x16f
	.4byte	0xbea
	.4byte	.LLST1
	.uleb128 0x33
	.string	"u"
	.byte	0x1
	.2byte	0x16f
	.4byte	0x4c8
	.4byte	.LLST2
	.uleb128 0x33
	.string	"v"
	.byte	0x1
	.2byte	0x16f
	.4byte	0x4c8
	.4byte	.LLST3
	.uleb128 0x33
	.string	"e"
	.byte	0x1
	.2byte	0x16f
	.4byte	0x4bc
	.4byte	.LLST4
	.uleb128 0x1d
	.string	"d"
	.byte	0x1
	.2byte	0x171
	.4byte	0x5f
	.4byte	.LLST5
	.uleb128 0x34
	.4byte	.LASF86
	.4byte	0xe64
	.uleb128 0x9
	.byte	0x3
	.8byte	__PRETTY_FUNCTION__.4363
	.uleb128 0x28
	.4byte	0xe8f
	.8byte	.LBB40
	.8byte	.LBE40-.LBB40
	.byte	0x1
	.2byte	0x18d
	.4byte	0xd6c
	.uleb128 0x20
	.4byte	0xea6
	.4byte	.LLST6
	.uleb128 0x20
	.4byte	0xe9c
	.4byte	.LLST7
	.byte	0
	.uleb128 0x1f
	.4byte	0xe8f
	.8byte	.LBB42
	.4byte	.Ldebug_ranges0+0x30
	.byte	0x1
	.2byte	0x197
	.4byte	0xd97
	.uleb128 0x20
	.4byte	0xea6
	.4byte	.LLST8
	.uleb128 0x20
	.4byte	0xe9c
	.4byte	.LLST9
	.byte	0
	.uleb128 0x2e
	.8byte	.LVL23
	.4byte	0x1332
	.4byte	0xdd7
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC3
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC1
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.2byte	0x186
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x56
	.uleb128 0x9
	.byte	0x3
	.8byte	.LANCHOR0
	.byte	0
	.uleb128 0x2e
	.8byte	.LVL28
	.4byte	0x1332
	.4byte	0xe17
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC2
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC1
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.2byte	0x185
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x56
	.uleb128 0x9
	.byte	0x3
	.8byte	.LANCHOR0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL32
	.4byte	0x1332
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC4
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC1
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.2byte	0x187
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x56
	.uleb128 0x9
	.byte	0x3
	.8byte	.LANCHOR0
	.byte	0
	.byte	0
	.uleb128 0x12
	.4byte	0x3e
	.4byte	0xe64
	.uleb128 0x13
	.4byte	0x72
	.byte	0x4
	.byte	0
	.uleb128 0x3
	.4byte	0xe54
	.uleb128 0x31
	.4byte	.LASF87
	.byte	0x1
	.2byte	0x15e
	.4byte	0x4c8
	.byte	0x1
	.4byte	0xe8f
	.uleb128 0x30
	.string	"g"
	.byte	0x1
	.2byte	0x15e
	.4byte	0xbea
	.uleb128 0x1e
	.string	"v"
	.byte	0x1
	.2byte	0x160
	.4byte	0x4c8
	.byte	0
	.uleb128 0x2f
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x14d
	.byte	0x1
	.4byte	0xeb1
	.uleb128 0x30
	.string	"g"
	.byte	0x1
	.2byte	0x14d
	.4byte	0xbea
	.uleb128 0x30
	.string	"v"
	.byte	0x1
	.2byte	0x14d
	.4byte	0x4c8
	.byte	0
	.uleb128 0x31
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x12b
	.4byte	0xbea
	.byte	0x1
	.4byte	0xf28
	.uleb128 0x30
	.string	"in"
	.byte	0x1
	.2byte	0x12b
	.4byte	0xbdf
	.uleb128 0x30
	.string	"n"
	.byte	0x1
	.2byte	0x12b
	.4byte	0x5f
	.uleb128 0x30
	.string	"m"
	.byte	0x1
	.2byte	0x12b
	.4byte	0x5f
	.uleb128 0x1e
	.string	"g"
	.byte	0x1
	.2byte	0x12d
	.4byte	0xbea
	.uleb128 0x1e
	.string	"u"
	.byte	0x1
	.2byte	0x12e
	.4byte	0x4c8
	.uleb128 0x1e
	.string	"v"
	.byte	0x1
	.2byte	0x12f
	.4byte	0x4c8
	.uleb128 0x1e
	.string	"i"
	.byte	0x1
	.2byte	0x130
	.4byte	0x5f
	.uleb128 0x1e
	.string	"a"
	.byte	0x1
	.2byte	0x131
	.4byte	0x5f
	.uleb128 0x1e
	.string	"b"
	.byte	0x1
	.2byte	0x132
	.4byte	0x5f
	.uleb128 0x1e
	.string	"c"
	.byte	0x1
	.2byte	0x133
	.4byte	0x5f
	.byte	0
	.uleb128 0x2f
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x11c
	.byte	0x1
	.4byte	0xf5e
	.uleb128 0x30
	.string	"u"
	.byte	0x1
	.2byte	0x11c
	.4byte	0x4c8
	.uleb128 0x30
	.string	"v"
	.byte	0x1
	.2byte	0x11c
	.4byte	0x4c8
	.uleb128 0x30
	.string	"c"
	.byte	0x1
	.2byte	0x11c
	.4byte	0x5f
	.uleb128 0x30
	.string	"e"
	.byte	0x1
	.2byte	0x11c
	.4byte	0x4bc
	.byte	0
	.uleb128 0x2f
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x10d
	.byte	0x1
	.4byte	0xf8a
	.uleb128 0x30
	.string	"u"
	.byte	0x1
	.2byte	0x10d
	.4byte	0x4c8
	.uleb128 0x30
	.string	"e"
	.byte	0x1
	.2byte	0x10d
	.4byte	0x4bc
	.uleb128 0x1e
	.string	"p"
	.byte	0x1
	.2byte	0x10f
	.4byte	0x4c2
	.byte	0
	.uleb128 0x35
	.4byte	.LASF92
	.byte	0x1
	.byte	0xf5
	.4byte	0x9d
	.byte	0x1
	.4byte	0xfb6
	.uleb128 0x36
	.string	"n"
	.byte	0x1
	.byte	0xf5
	.4byte	0x138
	.uleb128 0x36
	.string	"s"
	.byte	0x1
	.byte	0xf5
	.4byte	0x138
	.uleb128 0x37
	.string	"p"
	.byte	0x1
	.byte	0xf7
	.4byte	0x9d
	.byte	0
	.uleb128 0x35
	.4byte	.LASF93
	.byte	0x1
	.byte	0xdc
	.4byte	0x9d
	.byte	0x1
	.4byte	0xfd9
	.uleb128 0x36
	.string	"s"
	.byte	0x1
	.byte	0xdc
	.4byte	0x138
	.uleb128 0x37
	.string	"p"
	.byte	0x1
	.byte	0xde
	.4byte	0x9d
	.byte	0
	.uleb128 0x38
	.4byte	.LASF94
	.byte	0x1
	.byte	0xc2
	.4byte	0x5f
	.byte	0x1
	.4byte	0xffc
	.uleb128 0x37
	.string	"x"
	.byte	0x1
	.byte	0xc4
	.4byte	0x5f
	.uleb128 0x37
	.string	"c"
	.byte	0x1
	.byte	0xc5
	.4byte	0x5f
	.byte	0
	.uleb128 0x39
	.4byte	.LASF125
	.byte	0x1
	.byte	0x98
	.8byte	.LFB54
	.8byte	.LFE54-.LFB54
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x1155
	.uleb128 0x3a
	.string	"fmt"
	.byte	0x1
	.byte	0x98
	.4byte	0x108
	.4byte	.LLST10
	.uleb128 0x3b
	.uleb128 0x3c
	.string	"ap"
	.byte	0x1
	.byte	0xb5
	.4byte	0x12d
	.4byte	.LLST11
	.uleb128 0x3d
	.string	"buf"
	.byte	0x1
	.byte	0xb6
	.4byte	0x1155
	.uleb128 0x4
	.byte	0x91
	.sleb128 -8216
	.uleb128 0x24
	.4byte	0x11dd
	.8byte	.LBB46
	.4byte	.Ldebug_ranges0+0x60
	.byte	0x1
	.byte	0xb9
	.4byte	0x10a7
	.uleb128 0x20
	.4byte	0x1203
	.4byte	.LLST12
	.uleb128 0x20
	.4byte	0x11f8
	.4byte	.LLST13
	.uleb128 0x20
	.4byte	0x11ed
	.4byte	.LLST14
	.uleb128 0x25
	.8byte	.LVL38
	.4byte	0x133d
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.2byte	0x2000
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x56
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x57
	.uleb128 0x2
	.byte	0x91
	.sleb128 40
	.byte	0
	.byte	0
	.uleb128 0x27
	.4byte	0x11b5
	.8byte	.LBB54
	.8byte	.LBE54-.LBB54
	.byte	0x1
	.byte	0xbc
	.4byte	0x10f1
	.uleb128 0x20
	.4byte	0x11d0
	.4byte	.LLST15
	.uleb128 0x2c
	.4byte	0x11c5
	.uleb128 0x25
	.8byte	.LVL40
	.4byte	0x134c
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC5
	.byte	0
	.byte	0
	.uleb128 0x27
	.4byte	0x11b5
	.8byte	.LBB56
	.8byte	.LBE56-.LBB56
	.byte	0x1
	.byte	0xbe
	.4byte	0x1141
	.uleb128 0x20
	.4byte	0x11d0
	.4byte	.LLST16
	.uleb128 0x2c
	.4byte	0x11c5
	.uleb128 0x25
	.8byte	.LVL41
	.4byte	0x134c
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC7
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x56
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL42
	.4byte	0x1357
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x12
	.4byte	0x37
	.4byte	0x1166
	.uleb128 0x3e
	.4byte	0x72
	.2byte	0x1fff
	.byte	0
	.uleb128 0x3f
	.4byte	.LASF98
	.byte	0x4
	.byte	0x3b
	.4byte	0x9d
	.byte	0x3
	.4byte	0x1198
	.uleb128 0x40
	.4byte	.LASF95
	.byte	0x4
	.byte	0x3b
	.4byte	0x9d
	.uleb128 0x40
	.4byte	.LASF96
	.byte	0x4
	.byte	0x3b
	.4byte	0x5f
	.uleb128 0x40
	.4byte	.LASF97
	.byte	0x4
	.byte	0x3b
	.4byte	0x138
	.byte	0
	.uleb128 0x3f
	.4byte	.LASF99
	.byte	0x3
	.byte	0x66
	.4byte	0x5f
	.byte	0x3
	.4byte	0x11b5
	.uleb128 0x40
	.4byte	.LASF100
	.byte	0x3
	.byte	0x66
	.4byte	0x113
	.uleb128 0x3b
	.byte	0
	.uleb128 0x3f
	.4byte	.LASF101
	.byte	0x3
	.byte	0x5f
	.4byte	0x5f
	.byte	0x3
	.4byte	0x11dd
	.uleb128 0x40
	.4byte	.LASF102
	.byte	0x3
	.byte	0x5f
	.4byte	0xbe5
	.uleb128 0x40
	.4byte	.LASF100
	.byte	0x3
	.byte	0x5f
	.4byte	0x113
	.uleb128 0x3b
	.byte	0
	.uleb128 0x3f
	.4byte	.LASF103
	.byte	0x3
	.byte	0x2b
	.4byte	0x5f
	.byte	0x3
	.4byte	0x120f
	.uleb128 0x36
	.string	"__s"
	.byte	0x3
	.byte	0x2b
	.4byte	0xa5
	.uleb128 0x40
	.4byte	.LASF100
	.byte	0x3
	.byte	0x2b
	.4byte	0x113
	.uleb128 0x40
	.4byte	.LASF104
	.byte	0x3
	.byte	0x2b
	.4byte	0x118
	.byte	0
	.uleb128 0x41
	.4byte	.LASF126
	.byte	0x2
	.byte	0x2c
	.4byte	0x5f
	.byte	0x3
	.uleb128 0x42
	.4byte	0xfd9
	.8byte	.LFB55
	.8byte	.LFE55-.LFB55
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x1277
	.uleb128 0x22
	.4byte	0xfe9
	.4byte	.LLST0
	.uleb128 0x23
	.4byte	0xff2
	.uleb128 0x24
	.4byte	0x120f
	.8byte	.LBB36
	.4byte	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0xd6
	.4byte	0x1269
	.uleb128 0x2b
	.8byte	.LVL4
	.4byte	0x12ed
	.byte	0
	.uleb128 0x2b
	.8byte	.LVL1
	.4byte	0x131c
	.byte	0
	.uleb128 0x42
	.4byte	0xfb6
	.8byte	.LFB56
	.8byte	.LFE56-.LFB56
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x12de
	.uleb128 0x20
	.4byte	0xfc6
	.4byte	.LLST17
	.uleb128 0x22
	.4byte	0xfcf
	.4byte	.LLST18
	.uleb128 0x2e
	.8byte	.LVL44
	.4byte	0x12f9
	.4byte	0x12bc
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL48
	.4byte	0xffc
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC8
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x43
	.4byte	.LASF98
	.4byte	.LASF113
	.byte	0xf
	.byte	0
	.4byte	.LASF98
	.uleb128 0x44
	.4byte	.LASF105
	.4byte	.LASF105
	.byte	0x8
	.2byte	0x1b1
	.uleb128 0x44
	.4byte	.LASF106
	.4byte	.LASF106
	.byte	0xc
	.2byte	0x21b
	.uleb128 0x45
	.4byte	.LASF107
	.4byte	.LASF107
	.byte	0x3
	.byte	0x57
	.uleb128 0x44
	.4byte	.LASF108
	.4byte	.LASF108
	.byte	0xc
	.2byte	0x233
	.uleb128 0x45
	.4byte	.LASF109
	.4byte	.LASF109
	.byte	0xd
	.byte	0x4f
	.uleb128 0x45
	.4byte	.LASF110
	.4byte	.LASF110
	.byte	0xa
	.byte	0xc7
	.uleb128 0x45
	.4byte	.LASF111
	.4byte	.LASF111
	.byte	0xe
	.byte	0x45
	.uleb128 0x43
	.4byte	.LASF112
	.4byte	.LASF114
	.byte	0xf
	.byte	0
	.4byte	.LASF112
	.uleb128 0x45
	.4byte	.LASF115
	.4byte	.LASF115
	.byte	0x3
	.byte	0x55
	.uleb128 0x44
	.4byte	.LASF116
	.4byte	.LASF116
	.byte	0xc
	.2byte	0x266
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x32
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x33
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x34
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x35
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x36
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x37
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x38
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x39
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3a
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x3b
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x3c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x3d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x3e
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x3f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x40
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x41
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x42
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x43
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x6e
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x44
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x45
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST19:
	.8byte	.LVL49
	.8byte	.LVL51-1
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL51-1
	.8byte	.LFE68
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST20:
	.8byte	.LVL49
	.8byte	.LVL51-1
	.2byte	0x1
	.byte	0x54
	.8byte	.LVL51-1
	.8byte	.LFE68
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST21:
	.8byte	.LVL50
	.8byte	.LVL130
	.2byte	0x1
	.byte	0x67
	.8byte	.LVL131
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x67
	.8byte	0
	.8byte	0
.LLST22:
	.8byte	.LVL52
	.8byte	.LVL53-1
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST23:
	.8byte	.LVL54
	.8byte	.LVL55-1
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST24:
	.8byte	.LVL56
	.8byte	.LVL130
	.2byte	0x1
	.byte	0x67
	.8byte	.LVL131
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x67
	.8byte	0
	.8byte	0
.LLST25:
	.8byte	.LVL56
	.8byte	.LVL68
	.2byte	0x1
	.byte	0x68
	.8byte	.LVL68
	.8byte	.LVL70
	.2byte	0x3
	.byte	0x88
	.sleb128 1
	.byte	0x9f
	.8byte	.LVL70
	.8byte	.LVL71
	.2byte	0x1
	.byte	0x68
	.8byte	0
	.8byte	0
.LLST26:
	.8byte	.LVL56
	.8byte	.LVL72
	.2byte	0x1
	.byte	0x6a
	.8byte	0
	.8byte	0
.LLST27:
	.8byte	.LVL58
	.8byte	.LVL59
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL59
	.8byte	.LVL97
	.2byte	0x1
	.byte	0x6c
	.8byte	.LVL134
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x6c
	.8byte	0
	.8byte	0
.LLST28:
	.8byte	.LVL88
	.8byte	.LVL97
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL134
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST29:
	.8byte	.LVL89
	.8byte	.LVL97
	.2byte	0x1
	.byte	0x6e
	.8byte	.LVL134
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST30:
	.8byte	.LVL67
	.8byte	.LVL72
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST31:
	.8byte	.LVL60
	.8byte	.LVL63
	.2byte	0x2
	.byte	0x48
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST32:
	.8byte	.LVL60
	.8byte	.LVL63
	.2byte	0x9
	.byte	0x8a
	.sleb128 0
	.byte	0x8
	.byte	0x20
	.byte	0x24
	.byte	0x8
	.byte	0x20
	.byte	0x26
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST33:
	.8byte	.LVL61
	.8byte	.LVL63-1
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST34:
	.8byte	.LVL61
	.8byte	.LVL62
	.2byte	0x1
	.byte	0x6e
	.8byte	.LVL62
	.8byte	.LVL63-1
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL63-1
	.8byte	.LVL63
	.2byte	0x3
	.byte	0x8e
	.sleb128 24
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST35:
	.8byte	.LVL61
	.8byte	.LVL63
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST37:
	.8byte	.LVL64
	.8byte	.LVL66
	.2byte	0x2
	.byte	0x48
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST38:
	.8byte	.LVL64
	.8byte	.LVL66
	.2byte	0x9
	.byte	0x88
	.sleb128 0
	.byte	0x8
	.byte	0x20
	.byte	0x24
	.byte	0x8
	.byte	0x20
	.byte	0x26
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST39:
	.8byte	.LVL65
	.8byte	.LVL66-1
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST40:
	.8byte	.LVL65
	.8byte	.LVL66
	.2byte	0x1
	.byte	0x6d
	.8byte	0
	.8byte	0
.LLST41:
	.8byte	.LVL65
	.8byte	.LVL66
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST43:
	.8byte	.LVL89
	.8byte	.LVL96
	.2byte	0x1
	.byte	0x66
	.8byte	.LVL134
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x66
	.8byte	0
	.8byte	0
.LLST44:
	.8byte	.LVL89
	.8byte	.LVL96
	.2byte	0x1
	.byte	0x6e
	.8byte	.LVL134
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST45:
	.8byte	.LVL89
	.8byte	.LVL96
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL134
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST46:
	.8byte	.LVL90
	.8byte	.LVL93
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL93
	.8byte	.LVL97
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST47:
	.8byte	.LVL90
	.8byte	.LVL93
	.2byte	0x1
	.byte	0x66
	.8byte	0
	.8byte	0
.LLST48:
	.8byte	.LVL90
	.8byte	.LVL92
	.2byte	0x2
	.byte	0x40
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST49:
	.8byte	.LVL93
	.8byte	.LVL97
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST50:
	.8byte	.LVL93
	.8byte	.LVL96
	.2byte	0x1
	.byte	0x66
	.8byte	0
	.8byte	0
.LLST51:
	.8byte	.LVL93
	.8byte	.LVL95
	.2byte	0x2
	.byte	0x40
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST52:
	.8byte	.LVL99
	.8byte	.LVL118
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL131
	.8byte	.LVL134
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST53:
	.8byte	.LVL109
	.8byte	.LVL110
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	.LVL110
	.8byte	.LVL111
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL111
	.8byte	.LVL115
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	.LVL115
	.8byte	.LVL116
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL116
	.8byte	.LVL118
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	.LVL131
	.8byte	.LVL132-1
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL133
	.8byte	.LVL134
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST54:
	.8byte	.LVL101
	.8byte	.LVL102-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL103
	.8byte	.LVL107-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL110
	.8byte	.LVL111
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL113
	.8byte	.LVL116
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL131
	.8byte	.LVL132-1
	.2byte	0x1
	.byte	0x56
	.8byte	0
	.8byte	0
.LLST55:
	.8byte	.LVL100
	.8byte	.LVL108
	.2byte	0x1
	.byte	0x6e
	.8byte	.LVL109
	.8byte	.LVL117
	.2byte	0x1
	.byte	0x59
	.8byte	.LVL131
	.8byte	.LVL132-1
	.2byte	0x1
	.byte	0x59
	.8byte	0
	.8byte	0
.LLST56:
	.8byte	.LVL110
	.8byte	.LVL112
	.2byte	0x1
	.byte	0x58
	.8byte	.LVL115
	.8byte	.LVL116
	.2byte	0x2
	.byte	0x31
	.byte	0x9f
	.8byte	.LVL131
	.8byte	.LVL132-1
	.2byte	0x1
	.byte	0x58
	.8byte	0
	.8byte	0
.LLST57:
	.8byte	.LVL101
	.8byte	.LVL102-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL105
	.8byte	.LVL107-1
	.2byte	0x1
	.byte	0x56
	.8byte	0
	.8byte	0
.LLST59:
	.8byte	.LVL105
	.8byte	.LVL106
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST60:
	.8byte	.LVL118
	.8byte	.LVL119
	.2byte	0xa
	.byte	0x3
	.8byte	.LC11
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST61:
	.8byte	.LVL119
	.8byte	.LVL120
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	.LVL120
	.8byte	.LVL125
	.2byte	0x1
	.byte	0x6e
	.8byte	.LVL125
	.8byte	.LVL126
	.2byte	0x1
	.byte	0x59
	.8byte	0
	.8byte	0
.LLST62:
	.8byte	.LVL121
	.8byte	.LVL123-1
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL123
	.8byte	.LVL124
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL124
	.8byte	.LVL126
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST63:
	.8byte	.LVL122
	.8byte	.LVL124
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST1:
	.8byte	.LVL6
	.8byte	.LVL21
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL21
	.8byte	.LVL23
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	.LVL23
	.8byte	.LVL26
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL26
	.8byte	.LVL28
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	.LVL28
	.8byte	.LVL30
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL30
	.8byte	.LFE63
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST2:
	.8byte	.LVL6
	.8byte	.LVL20
	.2byte	0x1
	.byte	0x54
	.8byte	.LVL20
	.8byte	.LVL23
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.8byte	.LVL23
	.8byte	.LVL25
	.2byte	0x1
	.byte	0x54
	.8byte	.LVL25
	.8byte	.LVL28
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.8byte	.LVL28
	.8byte	.LVL29
	.2byte	0x1
	.byte	0x54
	.8byte	.LVL29
	.8byte	.LFE63
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST3:
	.8byte	.LVL6
	.8byte	.LVL22
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL22
	.8byte	.LVL23
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.8byte	.LVL23
	.8byte	.LVL27
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL27
	.8byte	.LVL28
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.8byte	.LVL28
	.8byte	.LVL31
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL31
	.8byte	.LFE63
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST4:
	.8byte	.LVL6
	.8byte	.LVL10
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL10
	.8byte	.LVL13
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x56
	.byte	0x9f
	.8byte	.LVL13
	.8byte	.LVL14
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL14
	.8byte	.LVL15
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x56
	.byte	0x9f
	.8byte	.LVL15
	.8byte	.LVL19
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL19
	.8byte	.LVL23
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x56
	.byte	0x9f
	.8byte	.LVL23
	.8byte	.LVL24
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL24
	.8byte	.LFE63
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x56
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST5:
	.8byte	.LVL7
	.8byte	.LVL8
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL9
	.8byte	.LVL13
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL14
	.8byte	.LVL15
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL16
	.8byte	.LVL17
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL18
	.8byte	.LVL19
	.2byte	0x1
	.byte	0x58
	.8byte	.LVL19
	.8byte	.LVL23-1
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL23
	.8byte	.LVL28-1
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL28
	.8byte	.LVL32-1
	.2byte	0x1
	.byte	0x5a
	.8byte	0
	.8byte	0
.LLST6:
	.8byte	.LVL11
	.8byte	.LVL12
	.2byte	0x1
	.byte	0x54
	.8byte	0
	.8byte	0
.LLST7:
	.8byte	.LVL11
	.8byte	.LVL12
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST8:
	.8byte	.LVL14
	.8byte	.LVL15
	.2byte	0x1
	.byte	0x55
	.8byte	0
	.8byte	0
.LLST9:
	.8byte	.LVL14
	.8byte	.LVL15
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST10:
	.8byte	.LVL33
	.8byte	.LVL36
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL36
	.8byte	.LVL38-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL38-1
	.8byte	.LFE54
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST11:
	.8byte	.LVL34
	.8byte	.LVL37
	.2byte	0x6
	.byte	0x71
	.sleb128 0
	.byte	0x6
	.byte	0x23
	.uleb128 0x28
	.byte	0x9f
	.8byte	.LVL37
	.8byte	.LVL38-1
	.2byte	0x1
	.byte	0x57
	.8byte	.LVL38-1
	.8byte	.LFE54
	.2byte	0x3
	.byte	0x91
	.sleb128 40
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST12:
	.8byte	.LVL34
	.8byte	.LVL37
	.2byte	0x6
	.byte	0x71
	.sleb128 0
	.byte	0x6
	.byte	0x23
	.uleb128 0x28
	.byte	0x9f
	.8byte	.LVL37
	.8byte	.LVL38-1
	.2byte	0x1
	.byte	0x57
	.8byte	.LVL38-1
	.8byte	.LVL38
	.2byte	0x3
	.byte	0x91
	.sleb128 40
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST13:
	.8byte	.LVL34
	.8byte	.LVL36
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL36
	.8byte	.LVL38-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL38-1
	.8byte	.LVL38
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST14:
	.8byte	.LVL34
	.8byte	.LVL35
	.2byte	0x8
	.byte	0x71
	.sleb128 0
	.byte	0x6
	.byte	0xa
	.2byte	0x2018
	.byte	0x1c
	.byte	0x9f
	.8byte	.LVL35
	.8byte	.LVL38
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST15:
	.8byte	.LVL39
	.8byte	.LVL40
	.2byte	0xa
	.byte	0x3
	.8byte	.LC5
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST16:
	.8byte	.LVL40
	.8byte	.LVL41
	.2byte	0xa
	.byte	0x3
	.8byte	.LC7
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST0:
	.8byte	.LVL0
	.8byte	.LVL2
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	.LVL2
	.8byte	.LVL5
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST17:
	.8byte	.LVL43
	.8byte	.LVL44-1
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL44-1
	.8byte	.LVL45
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL45
	.8byte	.LVL46
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	.LVL46
	.8byte	.LFE56
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST18:
	.8byte	.LVL44
	.8byte	.LVL47
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
	.section	.debug_aranges,"",@progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x8
	.byte	0
	.2byte	0
	.2byte	0
	.8byte	.Ltext0
	.8byte	.Letext0-.Ltext0
	.8byte	.LFB68
	.8byte	.LFE68-.LFB68
	.8byte	0
	.8byte	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.8byte	.LBB36
	.8byte	.LBE36
	.8byte	.LBB39
	.8byte	.LBE39
	.8byte	0
	.8byte	0
	.8byte	.LBB42
	.8byte	.LBE42
	.8byte	.LBB45
	.8byte	.LBE45
	.8byte	0
	.8byte	0
	.8byte	.LBB46
	.8byte	.LBE46
	.8byte	.LBB51
	.8byte	.LBE51
	.8byte	.LBB52
	.8byte	.LBE52
	.8byte	.LBB53
	.8byte	.LBE53
	.8byte	0
	.8byte	0
	.8byte	.LBB104
	.8byte	.LBE104
	.8byte	.LBB177
	.8byte	.LBE177
	.8byte	.LBB178
	.8byte	.LBE178
	.8byte	.LBB211
	.8byte	.LBE211
	.8byte	0
	.8byte	0
	.8byte	.LBB106
	.8byte	.LBE106
	.8byte	.LBB120
	.8byte	.LBE120
	.8byte	.LBB121
	.8byte	.LBE121
	.8byte	.LBB122
	.8byte	.LBE122
	.8byte	0
	.8byte	0
	.8byte	.LBB108
	.8byte	.LBE108
	.8byte	.LBB111
	.8byte	.LBE111
	.8byte	0
	.8byte	0
	.8byte	.LBB115
	.8byte	.LBE115
	.8byte	.LBB123
	.8byte	.LBE123
	.8byte	0
	.8byte	0
	.8byte	.LBB126
	.8byte	.LBE126
	.8byte	.LBB129
	.8byte	.LBE129
	.8byte	0
	.8byte	0
	.8byte	.LBB132
	.8byte	.LBE132
	.8byte	.LBB135
	.8byte	.LBE135
	.8byte	0
	.8byte	0
	.8byte	.LBB138
	.8byte	.LBE138
	.8byte	.LBB141
	.8byte	.LBE141
	.8byte	0
	.8byte	0
	.8byte	.LBB142
	.8byte	.LBE142
	.8byte	.LBB171
	.8byte	.LBE171
	.8byte	.LBB172
	.8byte	.LBE172
	.8byte	.LBB173
	.8byte	.LBE173
	.8byte	0
	.8byte	0
	.8byte	.LBB144
	.8byte	.LBE144
	.8byte	.LBB155
	.8byte	.LBE155
	.8byte	.LBB164
	.8byte	.LBE164
	.8byte	.LBB167
	.8byte	.LBE167
	.8byte	0
	.8byte	0
	.8byte	.LBB146
	.8byte	.LBE146
	.8byte	.LBB150
	.8byte	.LBE150
	.8byte	.LBB151
	.8byte	.LBE151
	.8byte	0
	.8byte	0
	.8byte	.LBB156
	.8byte	.LBE156
	.8byte	.LBB165
	.8byte	.LBE165
	.8byte	.LBB166
	.8byte	.LBE166
	.8byte	0
	.8byte	0
	.8byte	.LBB158
	.8byte	.LBE158
	.8byte	.LBB161
	.8byte	.LBE161
	.8byte	0
	.8byte	0
	.8byte	.LBB179
	.8byte	.LBE179
	.8byte	.LBB210
	.8byte	.LBE210
	.8byte	0
	.8byte	0
	.8byte	.LBB181
	.8byte	.LBE181
	.8byte	.LBB184
	.8byte	.LBE184
	.8byte	0
	.8byte	0
	.8byte	.LBB185
	.8byte	.LBE185
	.8byte	.LBB190
	.8byte	.LBE190
	.8byte	.LBB191
	.8byte	.LBE191
	.8byte	.LBB200
	.8byte	.LBE200
	.8byte	0
	.8byte	0
	.8byte	.LBB194
	.8byte	.LBE194
	.8byte	.LBB198
	.8byte	.LBE198
	.8byte	.LBB199
	.8byte	.LBE199
	.8byte	0
	.8byte	0
	.8byte	.LBB202
	.8byte	.LBE202
	.8byte	.LBB208
	.8byte	.LBE208
	.8byte	0
	.8byte	0
	.8byte	.LBB205
	.8byte	.LBE205
	.8byte	.LBB209
	.8byte	.LBE209
	.8byte	0
	.8byte	0
	.8byte	.Ltext0
	.8byte	.Letext0
	.8byte	.LFB68
	.8byte	.LFE68
	.8byte	0
	.8byte	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF78:
	.string	"hello"
.LASF47:
	.string	"_shortbuf"
.LASF121:
	.string	"_IO_lock_t"
.LASF117:
	.string	"GNU C11 7.5.0 -Asystem=linux -Asystem=unix -Asystem=posix -msecure-plt -mcpu=power8 -g -O3 -fstack-protector-strong"
.LASF68:
	.string	"stderr"
.LASF36:
	.string	"_IO_buf_end"
.LASF109:
	.string	"__ctype_b_loc"
.LASF93:
	.string	"xmalloc"
.LASF34:
	.string	"_IO_write_end"
.LASF0:
	.string	"unsigned int"
.LASF75:
	.string	"next"
.LASF28:
	.string	"_flags"
.LASF119:
	.string	"/home/fi6468he-s/labs/lab4"
.LASF92:
	.string	"xcalloc"
.LASF40:
	.string	"_markers"
.LASF125:
	.string	"error"
.LASF114:
	.string	"__builtin___vsprintf_chk"
.LASF81:
	.string	"progname"
.LASF83:
	.string	"other"
.LASF62:
	.string	"_pos"
.LASF67:
	.string	"stdout"
.LASF39:
	.string	"_IO_save_end"
.LASF106:
	.string	"malloc"
.LASF97:
	.string	"__len"
.LASF87:
	.string	"leave_excess"
.LASF124:
	.string	"push"
.LASF10:
	.string	"long long unsigned int"
.LASF70:
	.string	"sys_errlist"
.LASF38:
	.string	"_IO_backup_base"
.LASF49:
	.string	"_offset"
.LASF69:
	.string	"sys_nerr"
.LASF95:
	.string	"__dest"
.LASF14:
	.string	"_ISlower"
.LASF42:
	.string	"_fileno"
.LASF25:
	.string	"__gnuc_va_list"
.LASF27:
	.string	"size_t"
.LASF17:
	.string	"_ISxdigit"
.LASF31:
	.string	"_IO_read_base"
.LASF1:
	.string	"_Bool"
.LASF79:
	.string	"argc"
.LASF66:
	.string	"stdin"
.LASF60:
	.string	"_next"
.LASF71:
	.string	"graph_t"
.LASF108:
	.string	"free"
.LASF74:
	.string	"edge"
.LASF100:
	.string	"__fmt"
.LASF103:
	.string	"vsprintf"
.LASF84:
	.string	"free_graph"
.LASF2:
	.string	"char"
.LASF115:
	.string	"__fprintf_chk"
.LASF55:
	.string	"_mode"
.LASF59:
	.string	"_IO_marker"
.LASF29:
	.string	"_IO_read_ptr"
.LASF116:
	.string	"exit"
.LASF73:
	.string	"node_t"
.LASF26:
	.string	"va_list"
.LASF32:
	.string	"_IO_write_base"
.LASF63:
	.string	"_IO_2_1_stdin_"
.LASF9:
	.string	"long long int"
.LASF99:
	.string	"printf"
.LASF64:
	.string	"_IO_2_1_stdout_"
.LASF112:
	.string	"__vsprintf_chk"
.LASF37:
	.string	"_IO_save_base"
.LASF85:
	.string	"relabel"
.LASF22:
	.string	"_IScntrl"
.LASF107:
	.string	"__printf_chk"
.LASF104:
	.string	"__ap"
.LASF16:
	.string	"_ISdigit"
.LASF98:
	.string	"memset"
.LASF88:
	.string	"enter_excess"
.LASF18:
	.string	"_ISspace"
.LASF50:
	.string	"__pad1"
.LASF51:
	.string	"__pad2"
.LASF52:
	.string	"__pad3"
.LASF53:
	.string	"__pad4"
.LASF54:
	.string	"__pad5"
.LASF76:
	.string	"edge_t"
.LASF105:
	.string	"_IO_getc"
.LASF46:
	.string	"_vtable_offset"
.LASF72:
	.string	"excess"
.LASF80:
	.string	"argv"
.LASF91:
	.string	"add_edge"
.LASF96:
	.string	"__ch"
.LASF23:
	.string	"_ISpunct"
.LASF30:
	.string	"_IO_read_end"
.LASF19:
	.string	"_ISprint"
.LASF94:
	.string	"next_int"
.LASF5:
	.string	"short int"
.LASF90:
	.string	"connect"
.LASF7:
	.string	"long int"
.LASF110:
	.string	"fclose"
.LASF101:
	.string	"fprintf"
.LASF122:
	.string	"_IO_FILE_plus"
.LASF20:
	.string	"_ISgraph"
.LASF113:
	.string	"__builtin_memset"
.LASF48:
	.string	"_lock"
.LASF8:
	.string	"long unsigned int"
.LASF44:
	.string	"_old_offset"
.LASF58:
	.string	"_IO_FILE"
.LASF15:
	.string	"_ISalpha"
.LASF82:
	.string	"preflow"
.LASF111:
	.string	"__assert_fail"
.LASF4:
	.string	"unsigned char"
.LASF61:
	.string	"_sbuf"
.LASF126:
	.string	"getchar"
.LASF33:
	.string	"_IO_write_ptr"
.LASF102:
	.string	"__stream"
.LASF24:
	.string	"_ISalnum"
.LASF11:
	.string	"__off_t"
.LASF21:
	.string	"_ISblank"
.LASF3:
	.string	"signed char"
.LASF89:
	.string	"new_graph"
.LASF118:
	.string	"sequential.c"
.LASF6:
	.string	"short unsigned int"
.LASF123:
	.string	"main"
.LASF120:
	.string	"__builtin_va_list"
.LASF86:
	.string	"__PRETTY_FUNCTION__"
.LASF41:
	.string	"_chain"
.LASF13:
	.string	"_ISupper"
.LASF57:
	.string	"FILE"
.LASF43:
	.string	"_flags2"
.LASF77:
	.string	"list_t"
.LASF45:
	.string	"_cur_column"
.LASF65:
	.string	"_IO_2_1_stderr_"
.LASF12:
	.string	"__off64_t"
.LASF56:
	.string	"_unused2"
.LASF35:
	.string	"_IO_buf_base"
	.ident	"GCC: (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0"
	.section	.note.GNU-stack,"",@progbits
