	.file	"sequential.c"
	.abiversion 2
	.section	".text"
.Ltext0:
	.cfi_sections	.debug_frame
	.section	".toc","aw"
	.align 3
.LC0:
	.quad	stdin
	.section	".text"
	.align 2
	.p2align 4,,15
	.type	next_int, @function
next_int:
.LFB55:
	.file 1 "sequential.c"
	.loc 1 195 0
	.cfi_startproc
.LCF0:
0:	addis 2,12,.TOC.-.LCF0@ha
	addi 2,2,.TOC.-.LCF0@l
	.localentry	next_int,.-next_int
.LVL0:
	mflr 0
	std 28,-32(1)
	std 29,-24(1)
	std 31,-8(1)
	std 30,-16(1)
	.cfi_register 65, 0
	.cfi_offset 28, -32
	.cfi_offset 29, -24
	.cfi_offset 31, -8
	.cfi_offset 30, -16
	.loc 1 213 0
	li 31,0
	.loc 1 195 0
	std 0,16(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 65, 16
	.loc 1 214 0
	bl __ctype_b_loc
	nop
.LVL1:
	addis 29,2,.LC0@toc@ha		# gpr load fusion, type long
	ld 29,.LC0@toc@l(29)
	mr 28,3
	b .L2
.LVL2:
	.p2align 4,,15
.L3:
	.loc 1 215 0
	extsw 31,3
.LVL3:
.L2:
.LBB32:
.LBB33:
	.file 2 "/usr/include/powerpc64le-linux-gnu/bits/stdio.h"
	.loc 2 46 0
	ld 3,0(29)
.LBE33:
.LBE32:
	.loc 1 214 0
	ld 30,0(28)
.LBB35:
.LBB34:
	.loc 2 46 0
	bl _IO_getc
	nop
.LVL4:
.LBE34:
.LBE35:
	.loc 1 215 0
	mulli 9,31,10
	.loc 1 214 0
	sldi 10,3,1
	lhzx 10,30,10
	.loc 1 215 0
	add 3,9,3
	addi 3,3,-48
	.loc 1 214 0
	andi. 9,10,0x800
	bne 0,.L3
	.loc 1 218 0
	addi 1,1,64
	.cfi_def_cfa_offset 0
	mr 3,31
	ld 0,16(1)
	ld 28,-32(1)
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
.LVL5:
	mtlr 0
	.cfi_restore 65
	.cfi_restore 31
	.cfi_restore 30
	.cfi_restore 29
	.cfi_restore 28
	blr
	.long 0
	.byte 0,0,0,1,128,4,0,0
	.cfi_endproc
.LFE55:
	.size	next_int,.-next_int
	.align 2
	.p2align 4,,15
	.type	push, @function
push:
.LFB63:
	.loc 1 368 0
	.cfi_startproc
.LCF1:
0:	addis 2,12,.TOC.-.LCF1@ha
	addi 2,2,.TOC.-.LCF1@l
	.localentry	push,.-push
.LVL6:
	.loc 1 374 0
	ld 9,0(6)
	.loc 1 368 0
	stdu 1,-32(1)
	.cfi_def_cfa_offset 32
	addi 7,4,4
	addi 11,6,16
	.loc 1 374 0
	cmpd 7,9,4
	.loc 1 375 0
	sync
	lwz 9,4(4)
	.loc 1 374 0
	beq 7,.L28
	.loc 1 378 0
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	rldicl 10,9,0,32
	lwz 9,20(6)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	rldicl 8,9,0,32
	lwz 9,16(6)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	add 9,9,8
	cmpw 7,9,10
	bge 7,.L29
	.loc 1 378 0 is_stmt 0 discriminator 2
	lwz 10,20(6)
	cmpw 7,10,10
	bne- 7,$+4
	isync
	sync
	rldicl 9,10,0,32
	lwz 10,16(6)
	cmpw 7,10,10
	bne- 7,$+4
	isync
	add 10,10,9
	extsw 10,10
.L13:
.LVL7:
	.loc 1 379 0 is_stmt 1 discriminator 4
	sync
	rldicl 8,10,0,32
.L14:
	lwarx 9,0,11
	subf 9,8,9
	stwcx. 9,0,11
	bne 0,.L14
.L27:
	isync
	.loc 1 384 0 discriminator 4
	sync
.L15:
	.loc 1 384 0 is_stmt 0
	lwarx 9,0,7
	subf 9,8,9
	stwcx. 9,0,7
	bne 0,.L15
	isync
	.loc 1 385 0 is_stmt 1
	sync
	addi 7,5,4
.L16:
	lwarx 9,0,7
	add 9,9,8
	stwcx. 9,0,7
	bne 0,.L16
	isync
	.loc 1 389 0
	cmpwi 7,10,0
	blt 7,.L30
	.loc 1 390 0
	sync
	lwz 9,4(4)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	cmpwi 7,9,0
	blt 7,.L31
	.loc 1 391 0
	sync
	lwz 9,16(6)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	lwz 8,20(6)
	rldicl 9,9,0,32
	cmpw 7,8,8
	bne- 7,$+4
	isync
	srawi 7,9,31
	xor 9,7,9
	subf 9,7,9
	cmpw 7,8,9
	blt 7,.L32
	.loc 1 393 0
	sync
	lwz 9,4(4)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	cmpwi 7,9,0
	ble 7,.L20
.LVL8:
.LBB36:
.LBB37:
	.loc 1 344 0
	ld 9,32(3)
	cmpd 7,4,9
	beq 7,.L20
	ld 9,24(3)
	cmpd 7,4,9
	beq 7,.L20
	.loc 1 345 0
	ld 9,40(3)
	std 9,16(4)
	.loc 1 346 0
	std 4,40(3)
.LVL9:
.L20:
.LBE37:
.LBE36:
	.loc 1 400 0
	sync
	lwz 9,4(5)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	cmpw 7,9,10
	beq 7,.L33
.L6:
	.loc 1 409 0
	addi 1,1,32
	.cfi_remember_state
	.cfi_def_cfa_offset 0
	blr
.LVL10:
	.p2align 4,,15
.L29:
	.cfi_restore_state
	.loc 1 378 0 discriminator 1
	lwz 10,4(4)
	cmpw 7,10,10
	bne- 7,$+4
	isync
	extsw 10,10
	b .L13
.LVL11:
	.p2align 4,,15
.L33:
.LBB38:
.LBB39:
	.loc 1 344 0
	ld 9,32(3)
	cmpd 7,5,9
	beq 7,.L6
	ld 9,24(3)
	cmpd 7,5,9
	beq 7,.L6
	.loc 1 345 0
	ld 9,40(3)
.LBE39:
.LBE38:
	.loc 1 409 0
	addi 1,1,32
	.cfi_remember_state
	.cfi_def_cfa_offset 0
.LBB41:
.LBB40:
	.loc 1 345 0
	std 9,16(5)
	.loc 1 346 0
	std 5,40(3)
.LBE40:
.LBE41:
	.loc 1 409 0
	blr
.LVL12:
	.p2align 4,,15
.L28:
	.cfi_restore_state
	.loc 1 375 0
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	rldicl 10,9,0,32
	lwz 9,20(6)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	rldicl 8,9,0,32
	lwz 9,16(6)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	subf 9,9,8
	cmpw 7,9,10
	bge 7,.L34
	.loc 1 375 0 is_stmt 0 discriminator 2
	lwz 10,20(6)
	cmpw 7,10,10
	bne- 7,$+4
	isync
	sync
	rldicl 9,10,0,32
	lwz 10,16(6)
	cmpw 7,10,10
	bne- 7,$+4
	isync
	subf 10,10,9
	extsw 10,10
.L9:
.LVL13:
	.loc 1 376 0 is_stmt 1 discriminator 4
	sync
	rldicl 8,10,0,32
.L10:
	lwarx 9,0,11
	add 9,9,8
	stwcx. 9,0,11
	beq 0,.L27
	b .L10
.LVL14:
	.p2align 4,,15
.L34:
	.loc 1 375 0 discriminator 1
	lwz 10,4(4)
	cmpw 7,10,10
	bne- 7,$+4
	isync
	extsw 10,10
	b .L9
.LVL15:
.L32:
	mflr 0
	.cfi_remember_state
	.cfi_register 65, 0
	.loc 1 391 0 discriminator 1
	addis 6,2,.LANCHOR0@toc@ha
.LVL16:
	addis 4,2,.LC1@toc@ha
.LVL17:
	addis 3,2,.LC4@toc@ha
.LVL18:
	addi 6,6,.LANCHOR0@toc@l
	li 5,391
.LVL19:
	addi 4,4,.LC1@toc@l
	addi 3,3,.LC4@toc@l
	std 0,48(1)
	.cfi_offset 65, 16
	bl __assert_fail
	nop
.LVL20:
.L30:
	.cfi_restore_state
	mflr 0
	.cfi_remember_state
	.cfi_register 65, 0
	.loc 1 389 0 discriminator 1
	addis 6,2,.LANCHOR0@toc@ha
.LVL21:
	addis 4,2,.LC1@toc@ha
.LVL22:
	addis 3,2,.LC2@toc@ha
.LVL23:
	addi 6,6,.LANCHOR0@toc@l
	li 5,389
.LVL24:
	addi 4,4,.LC1@toc@l
	addi 3,3,.LC2@toc@l
	std 0,48(1)
	.cfi_offset 65, 16
	bl __assert_fail
	nop
.LVL25:
.L31:
	.cfi_restore_state
	mflr 0
	.cfi_register 65, 0
	.loc 1 390 0 discriminator 1
	addis 6,2,.LANCHOR0@toc@ha
.LVL26:
	addis 4,2,.LC1@toc@ha
.LVL27:
	addis 3,2,.LC3@toc@ha
.LVL28:
	addi 6,6,.LANCHOR0@toc@l
	li 5,390
.LVL29:
	addi 4,4,.LC1@toc@l
	addi 3,3,.LC3@toc@l
	std 0,48(1)
	.cfi_offset 65, 16
	.loc 1 390 0 discriminator 1
	bl __assert_fail
	nop
.LVL30:
	.long 0
	.byte 0,0,0,1,128,0,0,0
	.cfi_endproc
.LFE63:
	.size	push,.-push
	.section	".toc","aw"
.LC6:
	.quad	stderr
	.section	".text"
	.align 2
	.p2align 4,,15
	.globl error
	.type	error, @function
error:
.LFB54:
	.loc 1 153 0
	.cfi_startproc
.LCF2:
0:	addis 2,12,.TOC.-.LCF2@ha
	addi 2,2,.TOC.-.LCF2@l
	.localentry	error,.-error
.LVL31:
	mflr 0
	std 30,-16(1)
	std 31,-8(1)
	std 0,16(1)
	stdu 1,-8320(1)
	.cfi_def_cfa_offset 8320
	.cfi_offset 65, 16
	.cfi_offset 30, -16
	.cfi_offset 31, -8
.LVL32:
	.loc 1 153 0
	std 6,8376(1)
	std 4,8360(1)
.LBB42:
.LBB43:
	.file 3 "/usr/include/powerpc64le-linux-gnu/bits/stdio2.h"
	.loc 3 46 0
	addi 31,1,104
.LVL33:
	mr 6,3
.LBE43:
.LBE42:
	.loc 1 153 0
	std 5,8368(1)
	std 7,8384(1)
.LBB47:
.LBB44:
	.loc 3 46 0
	mr 3,31
.LVL34:
	addi 7,1,8360
.LVL35:
.LBE44:
.LBE47:
	.loc 1 153 0
	std 8,8392(1)
	std 9,8400(1)
.LBB48:
.LBB45:
	.loc 3 46 0
	li 5,8192
	li 4,1
.LBE45:
.LBE48:
	.loc 1 153 0
	std 10,8408(1)
	ld 9,-28688(13)
	std 9,8296(1)
	li 9,0
.LBB49:
.LBB46:
	.loc 3 46 0
	bl __vsprintf_chk
	nop
.LVL36:
.LBE46:
.LBE49:
	.loc 1 187 0
	addis 6,2,.LANCHOR1@toc@ha		# gpr load fusion, type long
	ld 6,.LANCHOR1@toc@l(6)
	.loc 1 188 0
	addis 30,2,.LC6@toc@ha		# gpr load fusion, type long
	ld 30,.LC6@toc@l(30)
	.loc 1 187 0
	cmpdi 7,6,0
	beq 7,.L36
.LVL37:
.LBB50:
.LBB51:
	.loc 3 97 0
	ld 3,0(30)
	addis 5,2,.LC5@toc@ha
	li 4,1
	addi 5,5,.LC5@toc@l
	bl __fprintf_chk
	nop
.LVL38:
.L36:
.LBE51:
.LBE50:
.LBB52:
.LBB53:
	ld 3,0(30)
	addis 5,2,.LC7@toc@ha
	mr 6,31
	addi 5,5,.LC7@toc@l
	li 4,1
	bl __fprintf_chk
	nop
.LVL39:
.LBE53:
.LBE52:
	.loc 1 191 0
	li 3,1
	bl exit
	nop
.LVL40:
	.long 0
	.byte 0,0,0,1,128,2,0,0
	.cfi_endproc
.LFE54:
	.size	error,.-error
	.align 2
	.p2align 4,,15
	.type	xmalloc, @function
xmalloc:
.LFB56:
	.loc 1 221 0
	.cfi_startproc
.LCF3:
0:	addis 2,12,.TOC.-.LCF3@ha
	addi 2,2,.TOC.-.LCF3@l
	.localentry	xmalloc,.-xmalloc
.LVL41:
	mflr 0
	std 31,-8(1)
	.cfi_register 65, 0
	.cfi_offset 31, -8
	mr 31,3
	std 0,16(1)
	stdu 1,-112(1)
	.cfi_def_cfa_offset 112
	.cfi_offset 65, 16
	.loc 1 237 0
	bl malloc
	nop
.LVL42:
	.loc 1 239 0
	cmpdi 7,3,0
	beq 7,.L45
	.loc 1 243 0
	addi 1,1,112
	.cfi_remember_state
	.cfi_def_cfa_offset 0
	ld 0,16(1)
	ld 31,-8(1)
.LVL43:
	mtlr 0
	.cfi_restore 65
	.cfi_restore 31
	blr
.LVL44:
.L45:
	.cfi_restore_state
	.loc 1 240 0
	addis 3,2,.LC8@toc@ha
.LVL45:
	mr 4,31
	addi 3,3,.LC8@toc@l
	bl error
.LVL46:
	.long 0
	.byte 0,0,0,1,128,1,0,0
	.cfi_endproc
.LFE56:
	.size	xmalloc,.-xmalloc
	.section	".toc","aw"
	.set .LC9,.LC0
.LC10:
	.quad	hello
	.section	.text.startup,"ax",@progbits
	.align 2
	.p2align 4,,15
	.globl main
	.type	main, @function
main:
.LFB68:
	.loc 1 525 0
	.cfi_startproc
.LCF4:
0:	addis 2,12,.TOC.-.LCF4@ha
	addi 2,2,.TOC.-.LCF4@l
	.localentry	main,.-main
.LVL47:
	mflr 0
	std 23,-72(1)
	std 24,-64(1)
	.loc 1 532 0
	addis 10,2,.LANCHOR1@toc@ha
	.loc 1 525 0
	std 25,-56(1)
	std 26,-48(1)
	std 27,-40(1)
	std 28,-32(1)
	std 29,-24(1)
	std 31,-8(1)
	std 30,-16(1)
	.cfi_register 65, 0
	.cfi_offset 23, -72
	.cfi_offset 24, -64
	.cfi_offset 25, -56
	.cfi_offset 26, -48
	.cfi_offset 27, -40
	.cfi_offset 28, -32
	.cfi_offset 29, -24
	.cfi_offset 31, -8
	.cfi_offset 30, -16
	.loc 1 534 0
	addis 27,2,.LC9@toc@ha		# gpr load fusion, type long
	ld 27,.LC9@toc@l(27)
	.loc 1 525 0
	std 0,16(1)
	stdu 1,-176(1)
	.cfi_def_cfa_offset 176
	.cfi_offset 65, 16
	.loc 1 532 0
	ld 9,0(4)
	.loc 1 534 0
	ld 23,0(27)
.LVL48:
	.loc 1 532 0
	std 9,.LANCHOR1@toc@l(10)
	.loc 1 536 0
	bl next_int
.LVL49:
.LBB100:
.LBB101:
.LBB102:
.LBB103:
	.loc 1 249 0
	mulli 31,3,24
.LBE103:
.LBE102:
.LBE101:
.LBE100:
	.loc 1 536 0
	mr 25,3
.LVL50:
	.loc 1 537 0
	bl next_int
.LVL51:
.LBB174:
.LBB170:
.LBB111:
.LBB112:
	.loc 1 249 0
	mulli 26,3,24
.LBE112:
.LBE111:
.LBE170:
.LBE174:
	.loc 1 537 0
	mr 29,3
.LVL52:
.LBB175:
.LBB171:
	.loc 1 312 0
	rldicl 24,3,0,32
.LBE171:
.LBE175:
	.loc 1 540 0
	bl next_int
.LVL53:
	.loc 1 541 0
	bl next_int
.LVL54:
.LBB176:
.LBB172:
	.loc 1 309 0
	li 3,48
	bl xmalloc
.LVL55:
	.loc 1 311 0
	sync
	.loc 1 309 0
	mr 28,3
.LVL56:
.LBB116:
.LBB108:
	.loc 1 249 0
	mr 3,31
.LVL57:
.LBE108:
.LBE116:
	.loc 1 311 0
	stw 25,0(28)
	.loc 1 312 0
	sync
	stw 24,4(28)
.LVL58:
.LBB117:
.LBB109:
	.loc 1 249 0
	bl xmalloc
.LVL59:
.LBB104:
.LBB105:
	.file 4 "/usr/include/powerpc64le-linux-gnu/bits/string_fortified.h"
	.loc 4 71 0
	mr 5,31
	li 4,0
.LBE105:
.LBE104:
.LBE109:
.LBE117:
	.loc 1 318 0
	addi 31,31,-24
.LVL60:
.LBB118:
.LBB110:
.LBB107:
.LBB106:
	.loc 4 71 0
	bl memset
	nop
.LVL61:
.LBE106:
.LBE107:
.LBE110:
.LBE118:
	.loc 1 314 0
	std 3,8(28)
.LVL62:
.LBB119:
.LBB115:
	.loc 1 249 0
	mr 3,26
	bl xmalloc
.LVL63:
.LBB113:
.LBB114:
	.loc 4 71 0
	mr 5,26
	li 4,0
	bl memset
	nop
.LVL64:
.LBE114:
.LBE113:
.LBE115:
.LBE119:
	.loc 1 317 0
	ld 9,8(28)
	.loc 1 321 0
	cmpwi 7,29,0
	.loc 1 319 0
	li 10,0
	.loc 1 315 0
	std 3,16(28)
	.loc 1 319 0
	std 10,40(28)
.LVL65:
	.loc 1 318 0
	add 31,9,31
	.loc 1 317 0
	std 9,24(28)
	.loc 1 318 0
	std 31,32(28)
	.loc 1 321 0
	ble 7,.L47
	addi 24,24,-1
.LVL66:
	std 22,96(1)
	.cfi_offset 22, -80
	bl __ctype_b_loc
	nop
.LVL67:
	rldicl 24,24,0,32
	.loc 1 321 0
	li 25,0
.LVL68:
	addi 24,24,1
	mulli 24,24,24
	mr 26,3
.LVL69:
	.p2align 4,,15
.L48:
.LBB120:
.LBB121:
	.loc 1 213 0
	li 31,0
	b .L56
.LVL70:
	.p2align 4,,15
.L49:
	.loc 1 215 0
	mulli 31,31,10
.LVL71:
	add 31,31,3
	addi 31,31,-48
	extsw 31,31
.LVL72:
.L56:
.LBB122:
.LBB123:
	.loc 2 46 0
	ld 3,0(27)
.LBE123:
.LBE122:
	.loc 1 214 0
	ld 30,0(26)
.LBB125:
.LBB124:
	.loc 2 46 0
	bl _IO_getc
	nop
.LVL73:
.LBE124:
.LBE125:
	.loc 1 214 0
	sldi 9,3,1
	lhzx 9,30,9
	andi. 9,9,0x800
	bne 0,.L49
.LBE121:
.LBE120:
.LBB126:
.LBB127:
	.loc 1 213 0
	li 30,0
	b .L50
.LVL74:
	.p2align 4,,15
.L51:
	.loc 1 215 0
	mulli 30,30,10
.LVL75:
	add 30,30,3
	addi 30,30,-48
	extsw 30,30
.LVL76:
.L50:
.LBB128:
.LBB129:
	.loc 2 46 0
	ld 3,0(27)
.LBE129:
.LBE128:
	.loc 1 214 0
	ld 29,0(26)
.LBB131:
.LBB130:
	.loc 2 46 0
	bl _IO_getc
	nop
.LVL77:
.LBE130:
.LBE131:
	.loc 1 214 0
	sldi 9,3,1
	lhzx 9,29,9
	andi. 9,9,0x800
	bne 0,.L51
.LBE127:
.LBE126:
.LBB132:
.LBB133:
	.loc 1 213 0
	li 29,0
	b .L52
.LVL78:
	.p2align 4,,15
.L53:
	.loc 1 215 0
	mulli 29,29,10
.LVL79:
	add 29,29,3
	addi 29,29,-48
	extsw 29,29
.LVL80:
.L52:
.LBB134:
.LBB135:
	.loc 2 46 0
	ld 3,0(27)
.LBE135:
.LBE134:
	.loc 1 214 0
	ld 22,0(26)
.LBB137:
.LBB136:
	.loc 2 46 0
	bl _IO_getc
	nop
.LVL81:
.LBE136:
.LBE137:
	.loc 1 214 0
	sldi 9,3,1
	lhzx 9,22,9
	andi. 9,9,0x800
	bne 0,.L53
.LVL82:
.LBE133:
.LBE132:
	.loc 1 325 0
	mulli 31,31,24
.LVL83:
	.loc 1 326 0
	mulli 30,30,24
.LVL84:
	.loc 1 325 0
	ld 9,8(28)
	.loc 1 327 0
	ld 10,16(28)
.LBB138:
.LBB139:
.LBB140:
.LBB141:
.LBB142:
.LBB143:
	.loc 1 237 0
	li 3,16
.LBE143:
.LBE142:
.LBE141:
.LBE140:
.LBE139:
.LBE138:
	.loc 1 327 0
	add 22,10,25
	.loc 1 325 0
	add 31,9,31
.LVL85:
	.loc 1 326 0
	add 30,9,30
.LVL86:
.LBB167:
.LBB164:
	.loc 1 291 0
	stdx 31,10,25
	.loc 1 292 0
	std 30,8(22)
	.loc 1 293 0
	sync
	stw 29,20(22)
.LVL87:
.LBB151:
.LBB148:
.LBB146:
.LBB144:
	.loc 1 237 0
	bl malloc
	nop
.LVL88:
	.loc 1 239 0
	mr. 9,3
	beq 0,.L55
.LVL89:
.LBE144:
.LBE146:
	.loc 1 280 0
	ld 10,8(31)
	.loc 1 279 0
	std 22,0(9)
.LBE148:
.LBE151:
.LBB152:
.LBB153:
.LBB154:
.LBB155:
	.loc 1 237 0
	li 3,16
.LBE155:
.LBE154:
.LBE153:
.LBE152:
.LBB160:
.LBB149:
	.loc 1 280 0
	std 10,8(9)
	.loc 1 281 0
	std 9,8(31)
.LVL90:
.LBE149:
.LBE160:
.LBB161:
.LBB158:
.LBB157:
.LBB156:
	.loc 1 237 0
	bl malloc
	nop
.LVL91:
	.loc 1 239 0
	cmpdi 0,3,0
	beq 0,.L55
.LVL92:
	addi 25,25,24
.LBE156:
.LBE157:
	.loc 1 280 0
	ld 10,8(30)
	.loc 1 279 0
	std 22,0(3)
.LBE158:
.LBE161:
.LBE164:
.LBE167:
	.loc 1 321 0
	cmpld 7,25,24
.LBB168:
.LBB165:
.LBB162:
.LBB159:
	.loc 1 280 0
	std 10,8(3)
	.loc 1 281 0
	std 3,8(30)
.LVL93:
.LBE159:
.LBE162:
.LBE165:
.LBE168:
	.loc 1 321 0
	bne 7,.L48
	ld 22,96(1)
	.cfi_restore 22
.LVL94:
.L47:
.LBE172:
.LBE176:
	.loc 1 545 0
	mr 3,23
	bl fclose
	nop
.LVL95:
.LBB177:
.LBB178:
	.loc 1 440 0
	ld 30,24(28)
.LVL96:
	.loc 1 441 0
	sync
	lwz 9,0(28)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	rldicl 9,9,0,32
	stw 9,0(30)
	.loc 1 443 0
	ld 31,8(30)
.LVL97:
	.loc 1 450 0
	cmpdi 7,31,0
	beq 7,.L99
	addi 29,30,4
	b .L58
.LVL98:
	.p2align 4,,15
.L60:
	.loc 1 455 0
	mr 4,30
	mr 3,28
	bl push
.LVL99:
	.loc 1 450 0
	cmpdi 7,31,0
	beq 7,.L99
.L58:
	.loc 1 451 0
	ld 6,0(31)
.LVL100:
	.loc 1 452 0
	ld 31,8(31)
.LVL101:
	.loc 1 454 0
	sync
	lwz 9,20(6)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	rldicl 10,9,0,32
.L59:
	lwarx 9,0,29
	add 9,9,10
	stwcx. 9,0,29
	bne 0,.L59
	isync
	.loc 1 455 0
	ld 5,0(6)
.LVL102:
.LBB179:
.LBB180:
	.loc 1 425 0
	cmpld 7,30,5
	bne 7,.L60
.LVL103:
.LBE180:
.LBE179:
	.loc 1 455 0
	ld 5,8(6)
	mr 4,30
	mr 3,28
	bl push
.LVL104:
	.loc 1 450 0
	cmpdi 7,31,0
	bne 7,.L58
.LVL105:
	.p2align 4,,15
.L99:
	ld 4,40(28)
	cmpdi 5,4,0
.L62:
.LVL106:
.LBB181:
.LBB182:
	.loc 1 361 0
	beq 5,.L71
.L101:
.LBE182:
.LBE181:
	.loc 1 476 0
	ld 8,8(4)
.LBB186:
.LBB183:
	.loc 1 362 0
	ld 9,16(4)
.LBE183:
.LBE186:
	.loc 1 478 0
	cmpdi 7,8,0
.LBB187:
.LBB184:
	.loc 1 362 0
	std 9,40(28)
.LVL107:
.LBE184:
.LBE187:
	.loc 1 478 0
	bne 7,.L65
	b .L66
.LVL108:
	.p2align 4,,15
.L67:
	cmpdi 7,8,0
	beq 7,.L66
.LVL109:
.L65:
	.loc 1 479 0
	ld 6,0(8)
.LVL110:
	.loc 1 487 0
	li 7,-1
	.loc 1 480 0
	ld 8,8(8)
.LVL111:
	.loc 1 482 0
	ld 5,0(6)
	cmpld 7,5,4
	beq 7,.L100
.LVL112:
.L63:
	.loc 1 490 0
	sync
	lwz 9,0(4)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	rldicl 10,9,0,32
	lwz 9,0(5)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	cmpw 7,9,10
	bge 7,.L67
	sync
	lwz 9,16(6)
	cmpw 7,9,9
	bne- 7,$+4
	isync
	sync
	lwz 10,20(6)
	rldicl 9,9,0,32
	cmpw 7,10,10
	bne- 7,$+4
	isync
	mullw 9,9,7
	cmpw 7,10,9
	ble 7,.L67
	.loc 1 496 0
	cmpdi 7,5,0
	beq 7,.L66
	.loc 1 497 0
	mr 3,28
	bl push
.LVL113:
	ld 4,40(28)
	cmpdi 5,4,0
.LBB188:
.LBB185:
	.loc 1 361 0
	bne 5,.L101
.L71:
.LBE185:
.LBE188:
	.loc 1 502 0
	ld 9,32(28)
	sync
	lwz 5,4(9)
	cmpw 7,5,5
	bne- 7,$+4
	isync
.LVL114:
.LBE178:
.LBE177:
.LBB198:
.LBB199:
	.loc 3 104 0
	addis 4,2,.LC11@toc@ha
	extsw 5,5
	addi 4,4,.LC11@toc@l
	li 3,1
	bl __printf_chk
	nop
.LVL115:
.LBE199:
.LBE198:
.LBB201:
.LBB202:
	.loc 1 511 0
	sync
	lwz 9,0(28)
.LBE202:
.LBE201:
.LBB204:
.LBB200:
	.loc 3 104 0
	li 29,0
.LBE200:
.LBE204:
.LBB205:
.LBB203:
	.loc 1 511 0
	li 30,0
.LVL116:
	cmpw 7,9,9
	bne- 7,$+4
	isync
	cmpw 7,9,30
	ble 7,.L102
.L74:
	.loc 1 512 0
	ld 9,8(28)
	add 9,9,29
	ld 3,8(9)
.LVL117:
	.loc 1 513 0
	cmpdi 7,3,0
	beq 7,.L72
	.p2align 5,,31
.L73:
	.loc 1 514 0
	ld 31,8(3)
.LVL118:
	.loc 1 515 0
	bl free
	nop
.LVL119:
	.loc 1 513 0
	cmpdi 7,31,0
	mr 3,31
	bne 7,.L73
.LVL120:
.L72:
	.loc 1 511 0
	sync
	lwz 9,0(28)
	addi 30,30,1
.LVL121:
	addi 29,29,24
	extsw 30,30
.LVL122:
	cmpw 7,9,9
	bne- 7,$+4
	isync
	cmpw 7,9,30
	bgt 7,.L74
.LVL123:
.L102:
	.loc 1 519 0
	ld 3,8(28)
	bl free
	nop
.LVL124:
	.loc 1 520 0
	ld 3,16(28)
	bl free
	nop
.LVL125:
	.loc 1 521 0
	mr 3,28
	bl free
	nop
.LVL126:
.LBE203:
.LBE205:
	.loc 1 554 0
	addi 1,1,176
	.cfi_remember_state
	.cfi_def_cfa_offset 0
	li 3,0
	ld 0,16(1)
	ld 23,-72(1)
.LVL127:
	ld 24,-64(1)
	ld 25,-56(1)
	ld 26,-48(1)
	ld 27,-40(1)
	ld 28,-32(1)
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	mtlr 0
	.cfi_restore 65
	.cfi_restore 31
	.cfi_restore 30
	.cfi_restore 29
	.cfi_restore 28
	.cfi_restore 27
	.cfi_restore 26
	.cfi_restore 25
	.cfi_restore 24
	.cfi_restore 23
	blr
.LVL128:
	.p2align 4,,15
.L100:
	.cfi_restore_state
.LBB206:
.LBB197:
	.loc 1 483 0
	ld 5,8(6)
.LVL129:
	.loc 1 484 0
	li 7,1
	b .L63
.LVL130:
	.p2align 4,,15
.L66:
.LBB189:
.LBB190:
	.loc 1 414 0
	addis 10,2,.LC10@toc@ha		# gpr load fusion, type long
	ld 10,.LC10@toc@l(10)
	lwz 9,0(10)
	andi. 9,9,0x1234
	stw 9,0(10)
	.loc 1 415 0
	sync
.L69:
	lwarx 9,0,4
	addi 9,9,1
	stwcx. 9,0,4
	bne 0,.L69
	isync
.LBB191:
.LBB192:
	.loc 1 344 0
	ld 9,32(28)
.LBE192:
.LBE191:
	.loc 1 416 0
	lwz 8,0(10)
.LVL131:
.LBB195:
.LBB193:
	.loc 1 344 0
	cmpd 7,4,9
.LBE193:
.LBE195:
	.loc 1 416 0
	andi. 9,8,0x5678
	stw 9,0(10)
.LVL132:
.LBB196:
.LBB194:
	.loc 1 344 0
	beq 7,.L99
	ld 9,24(28)
	ld 10,40(28)
	cmpd 7,4,9
	beq 7,.L77
	.loc 1 345 0
	std 10,16(4)
	.loc 1 346 0
	std 4,40(28)
	b .L62
.L77:
	mr 4,10
	cmpdi 5,10,0
	b .L62
.LVL133:
.L55:
	.cfi_offset 22, -80
.LBE194:
.LBE196:
.LBE190:
.LBE189:
.LBE197:
.LBE206:
.LBB207:
.LBB173:
.LBB169:
.LBB166:
.LBB163:
.LBB150:
.LBB147:
.LBB145:
	.loc 1 240 0
	addis 3,2,.LC8@toc@ha
	li 4,16
	addi 3,3,.LC8@toc@l
	bl error
.LVL134:
.LBE145:
.LBE147:
.LBE150:
.LBE163:
.LBE166:
.LBE169:
.LBE173:
.LBE207:
	.long 0
	.byte 0,0,0,1,128,10,0,0
	.cfi_endproc
.LFE68:
	.size	main,.-main
	.comm	hello,4,4
	.section	.rodata
	.align 3
	.set	.LANCHOR0,. + 0
	.type	__PRETTY_FUNCTION__.4386, @object
	.size	__PRETTY_FUNCTION__.4386, 5
__PRETTY_FUNCTION__.4386:
	.string	"push"
	.section	".bss"
	.align 3
	.set	.LANCHOR1,. + 0
	.type	progname, @object
	.size	progname, 8
progname:
	.zero	8
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 3
.LC1:
	.string	"sequential.c"
	.zero	3
.LC2:
	.string	"d >= 0"
	.zero	1
.LC3:
	.string	"u->e >= 0"
	.zero	6
.LC4:
	.string	"abs(e->f) <= e->c"
	.zero	6
.LC5:
	.string	"%s: "
	.zero	3
.LC7:
	.string	"error: %s\n"
	.zero	5
.LC8:
	.string	"out of memory: malloc(%zu) failed"
	.zero	6
.LC11:
	.string	"f = %d\n"
	.section	".text"
.Letext0:
	.file 5 "/usr/include/powerpc64le-linux-gnu/bits/types.h"
	.file 6 "/usr/lib/gcc/powerpc64le-linux-gnu/7/include/stdarg.h"
	.file 7 "/usr/lib/gcc/powerpc64le-linux-gnu/7/include/stddef.h"
	.file 8 "/usr/include/powerpc64le-linux-gnu/bits/libio.h"
	.file 9 "/usr/include/powerpc64le-linux-gnu/bits/types/FILE.h"
	.file 10 "/usr/include/stdio.h"
	.file 11 "/usr/include/powerpc64le-linux-gnu/bits/sys_errlist.h"
	.file 12 "/usr/include/stdlib.h"
	.file 13 "/usr/include/ctype.h"
	.file 14 "/usr/include/assert.h"
	.file 15 "<built-in>"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.4byte	0x1392
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.4byte	.LASF118
	.byte	0xc
	.4byte	.LASF119
	.4byte	.LASF120
	.4byte	.Ldebug_ranges0+0x4c0
	.8byte	0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x2
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF5
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF6
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x5
	.4byte	0x5f
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x6
	.4byte	.LASF11
	.byte	0x5
	.byte	0x8c
	.4byte	0x6b
	.uleb128 0x6
	.4byte	.LASF12
	.byte	0x5
	.byte	0x8d
	.4byte	0x6b
	.uleb128 0x7
	.byte	0x8
	.uleb128 0x8
	.byte	0x8
	.4byte	0x37
	.uleb128 0x9
	.4byte	0x9f
	.uleb128 0xa
	.byte	0x7
	.byte	0x4
	.4byte	0x29
	.byte	0xd
	.byte	0x2f
	.4byte	0x108
	.uleb128 0xb
	.4byte	.LASF13
	.2byte	0x100
	.uleb128 0xb
	.4byte	.LASF14
	.2byte	0x200
	.uleb128 0xb
	.4byte	.LASF15
	.2byte	0x400
	.uleb128 0xb
	.4byte	.LASF16
	.2byte	0x800
	.uleb128 0xb
	.4byte	.LASF17
	.2byte	0x1000
	.uleb128 0xb
	.4byte	.LASF18
	.2byte	0x2000
	.uleb128 0xb
	.4byte	.LASF19
	.2byte	0x4000
	.uleb128 0xb
	.4byte	.LASF20
	.2byte	0x8000
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF22
	.byte	0x2
	.uleb128 0xc
	.4byte	.LASF23
	.byte	0x4
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x8
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.4byte	0x3e
	.uleb128 0x3
	.4byte	0x108
	.uleb128 0x9
	.4byte	0x108
	.uleb128 0x6
	.4byte	.LASF25
	.byte	0x6
	.byte	0x28
	.4byte	0x123
	.uleb128 0xd
	.byte	0x8
	.4byte	.LASF121
	.4byte	0x37
	.uleb128 0x6
	.4byte	.LASF26
	.byte	0x6
	.byte	0x63
	.4byte	0x118
	.uleb128 0x6
	.4byte	.LASF27
	.byte	0x7
	.byte	0xd8
	.4byte	0x72
	.uleb128 0xe
	.4byte	.LASF58
	.byte	0xd8
	.byte	0x8
	.byte	0xf5
	.4byte	0x2c3
	.uleb128 0xf
	.4byte	.LASF28
	.byte	0x8
	.byte	0xf6
	.4byte	0x5f
	.byte	0
	.uleb128 0xf
	.4byte	.LASF29
	.byte	0x8
	.byte	0xfb
	.4byte	0x9f
	.byte	0x8
	.uleb128 0xf
	.4byte	.LASF30
	.byte	0x8
	.byte	0xfc
	.4byte	0x9f
	.byte	0x10
	.uleb128 0xf
	.4byte	.LASF31
	.byte	0x8
	.byte	0xfd
	.4byte	0x9f
	.byte	0x18
	.uleb128 0xf
	.4byte	.LASF32
	.byte	0x8
	.byte	0xfe
	.4byte	0x9f
	.byte	0x20
	.uleb128 0xf
	.4byte	.LASF33
	.byte	0x8
	.byte	0xff
	.4byte	0x9f
	.byte	0x28
	.uleb128 0x10
	.4byte	.LASF34
	.byte	0x8
	.2byte	0x100
	.4byte	0x9f
	.byte	0x30
	.uleb128 0x10
	.4byte	.LASF35
	.byte	0x8
	.2byte	0x101
	.4byte	0x9f
	.byte	0x38
	.uleb128 0x10
	.4byte	.LASF36
	.byte	0x8
	.2byte	0x102
	.4byte	0x9f
	.byte	0x40
	.uleb128 0x10
	.4byte	.LASF37
	.byte	0x8
	.2byte	0x104
	.4byte	0x9f
	.byte	0x48
	.uleb128 0x10
	.4byte	.LASF38
	.byte	0x8
	.2byte	0x105
	.4byte	0x9f
	.byte	0x50
	.uleb128 0x10
	.4byte	.LASF39
	.byte	0x8
	.2byte	0x106
	.4byte	0x9f
	.byte	0x58
	.uleb128 0x10
	.4byte	.LASF40
	.byte	0x8
	.2byte	0x108
	.4byte	0x306
	.byte	0x60
	.uleb128 0x10
	.4byte	.LASF41
	.byte	0x8
	.2byte	0x10a
	.4byte	0x30c
	.byte	0x68
	.uleb128 0x10
	.4byte	.LASF42
	.byte	0x8
	.2byte	0x10c
	.4byte	0x5f
	.byte	0x70
	.uleb128 0x10
	.4byte	.LASF43
	.byte	0x8
	.2byte	0x110
	.4byte	0x5f
	.byte	0x74
	.uleb128 0x10
	.4byte	.LASF44
	.byte	0x8
	.2byte	0x112
	.4byte	0x87
	.byte	0x78
	.uleb128 0x10
	.4byte	.LASF45
	.byte	0x8
	.2byte	0x116
	.4byte	0x58
	.byte	0x80
	.uleb128 0x10
	.4byte	.LASF46
	.byte	0x8
	.2byte	0x117
	.4byte	0x43
	.byte	0x82
	.uleb128 0x10
	.4byte	.LASF47
	.byte	0x8
	.2byte	0x118
	.4byte	0x312
	.byte	0x83
	.uleb128 0x10
	.4byte	.LASF48
	.byte	0x8
	.2byte	0x11c
	.4byte	0x322
	.byte	0x88
	.uleb128 0x10
	.4byte	.LASF49
	.byte	0x8
	.2byte	0x125
	.4byte	0x92
	.byte	0x90
	.uleb128 0x10
	.4byte	.LASF50
	.byte	0x8
	.2byte	0x12d
	.4byte	0x9d
	.byte	0x98
	.uleb128 0x10
	.4byte	.LASF51
	.byte	0x8
	.2byte	0x12e
	.4byte	0x9d
	.byte	0xa0
	.uleb128 0x10
	.4byte	.LASF52
	.byte	0x8
	.2byte	0x12f
	.4byte	0x9d
	.byte	0xa8
	.uleb128 0x10
	.4byte	.LASF53
	.byte	0x8
	.2byte	0x130
	.4byte	0x9d
	.byte	0xb0
	.uleb128 0x10
	.4byte	.LASF54
	.byte	0x8
	.2byte	0x132
	.4byte	0x138
	.byte	0xb8
	.uleb128 0x10
	.4byte	.LASF55
	.byte	0x8
	.2byte	0x133
	.4byte	0x5f
	.byte	0xc0
	.uleb128 0x10
	.4byte	.LASF56
	.byte	0x8
	.2byte	0x135
	.4byte	0x328
	.byte	0xc4
	.byte	0
	.uleb128 0x6
	.4byte	.LASF57
	.byte	0x9
	.byte	0x7
	.4byte	0x143
	.uleb128 0x11
	.4byte	.LASF122
	.byte	0x8
	.byte	0x9a
	.uleb128 0xe
	.4byte	.LASF59
	.byte	0x18
	.byte	0x8
	.byte	0xa0
	.4byte	0x306
	.uleb128 0xf
	.4byte	.LASF60
	.byte	0x8
	.byte	0xa1
	.4byte	0x306
	.byte	0
	.uleb128 0xf
	.4byte	.LASF61
	.byte	0x8
	.byte	0xa2
	.4byte	0x30c
	.byte	0x8
	.uleb128 0xf
	.4byte	.LASF62
	.byte	0x8
	.byte	0xa6
	.4byte	0x5f
	.byte	0x10
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.4byte	0x2d5
	.uleb128 0x8
	.byte	0x8
	.4byte	0x143
	.uleb128 0x12
	.4byte	0x37
	.4byte	0x322
	.uleb128 0x13
	.4byte	0x72
	.byte	0
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.4byte	0x2ce
	.uleb128 0x12
	.4byte	0x37
	.4byte	0x338
	.uleb128 0x13
	.4byte	0x72
	.byte	0x13
	.byte	0
	.uleb128 0x14
	.4byte	.LASF123
	.uleb128 0x15
	.4byte	.LASF63
	.byte	0x8
	.2byte	0x13f
	.4byte	0x338
	.uleb128 0x15
	.4byte	.LASF64
	.byte	0x8
	.2byte	0x140
	.4byte	0x338
	.uleb128 0x15
	.4byte	.LASF65
	.byte	0x8
	.2byte	0x141
	.4byte	0x338
	.uleb128 0x16
	.4byte	.LASF66
	.byte	0xa
	.byte	0x87
	.4byte	0x30c
	.uleb128 0x16
	.4byte	.LASF67
	.byte	0xa
	.byte	0x88
	.4byte	0x30c
	.uleb128 0x16
	.4byte	.LASF68
	.byte	0xa
	.byte	0x89
	.4byte	0x30c
	.uleb128 0x16
	.4byte	.LASF69
	.byte	0xb
	.byte	0x1a
	.4byte	0x5f
	.uleb128 0x12
	.4byte	0x10e
	.4byte	0x398
	.uleb128 0x17
	.byte	0
	.uleb128 0x3
	.4byte	0x38d
	.uleb128 0x16
	.4byte	.LASF70
	.byte	0xb
	.byte	0x1b
	.4byte	0x398
	.uleb128 0x6
	.4byte	.LASF71
	.byte	0x1
	.byte	0x41
	.4byte	0x3b3
	.uleb128 0xe
	.4byte	.LASF71
	.byte	0x30
	.byte	0x1
	.byte	0x59
	.4byte	0x408
	.uleb128 0x18
	.string	"n"
	.byte	0x1
	.byte	0x5a
	.4byte	0x4ea
	.byte	0
	.uleb128 0x18
	.string	"m"
	.byte	0x1
	.byte	0x5b
	.4byte	0x4f1
	.byte	0x4
	.uleb128 0x18
	.string	"v"
	.byte	0x1
	.byte	0x5c
	.4byte	0x4d6
	.byte	0x8
	.uleb128 0x18
	.string	"e"
	.byte	0x1
	.byte	0x5d
	.4byte	0x4bc
	.byte	0x10
	.uleb128 0x18
	.string	"s"
	.byte	0x1
	.byte	0x5e
	.4byte	0x4d6
	.byte	0x18
	.uleb128 0x18
	.string	"t"
	.byte	0x1
	.byte	0x5f
	.4byte	0x4d6
	.byte	0x20
	.uleb128 0xf
	.4byte	.LASF72
	.byte	0x1
	.byte	0x60
	.4byte	0x4d6
	.byte	0x28
	.byte	0
	.uleb128 0x6
	.4byte	.LASF73
	.byte	0x1
	.byte	0x42
	.4byte	0x413
	.uleb128 0xe
	.4byte	.LASF73
	.byte	0x18
	.byte	0x1
	.byte	0x4b
	.4byte	0x44c
	.uleb128 0x18
	.string	"h"
	.byte	0x1
	.byte	0x4c
	.4byte	0x4c8
	.byte	0
	.uleb128 0x18
	.string	"e"
	.byte	0x1
	.byte	0x4d
	.4byte	0x4cf
	.byte	0x4
	.uleb128 0xf
	.4byte	.LASF74
	.byte	0x1
	.byte	0x4e
	.4byte	0x4c2
	.byte	0x8
	.uleb128 0xf
	.4byte	.LASF75
	.byte	0x1
	.byte	0x4f
	.4byte	0x4d6
	.byte	0x10
	.byte	0
	.uleb128 0x6
	.4byte	.LASF76
	.byte	0x1
	.byte	0x43
	.4byte	0x457
	.uleb128 0xe
	.4byte	.LASF76
	.byte	0x18
	.byte	0x1
	.byte	0x52
	.4byte	0x48c
	.uleb128 0x18
	.string	"u"
	.byte	0x1
	.byte	0x53
	.4byte	0x4d6
	.byte	0
	.uleb128 0x18
	.string	"v"
	.byte	0x1
	.byte	0x54
	.4byte	0x4d6
	.byte	0x8
	.uleb128 0x18
	.string	"f"
	.byte	0x1
	.byte	0x55
	.4byte	0x4dc
	.byte	0x10
	.uleb128 0x18
	.string	"c"
	.byte	0x1
	.byte	0x56
	.4byte	0x4e3
	.byte	0x14
	.byte	0
	.uleb128 0x6
	.4byte	.LASF77
	.byte	0x1
	.byte	0x44
	.4byte	0x497
	.uleb128 0xe
	.4byte	.LASF77
	.byte	0x10
	.byte	0x1
	.byte	0x46
	.4byte	0x4bc
	.uleb128 0xf
	.4byte	.LASF74
	.byte	0x1
	.byte	0x47
	.4byte	0x4bc
	.byte	0
	.uleb128 0xf
	.4byte	.LASF75
	.byte	0x1
	.byte	0x48
	.4byte	0x4c2
	.byte	0x8
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.4byte	0x44c
	.uleb128 0x8
	.byte	0x8
	.4byte	0x48c
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF78
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF78
	.uleb128 0x8
	.byte	0x8
	.4byte	0x408
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF78
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF78
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF78
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF78
	.uleb128 0x19
	.4byte	.LASF82
	.byte	0x1
	.byte	0x7a
	.4byte	0x9f
	.uleb128 0x9
	.byte	0x3
	.8byte	progname
	.uleb128 0x1a
	.4byte	.LASF79
	.byte	0x1
	.2byte	0x19b
	.4byte	0x66
	.uleb128 0x9
	.byte	0x3
	.8byte	hello
	.uleb128 0x1b
	.4byte	.LASF124
	.byte	0x1
	.2byte	0x20c
	.4byte	0x5f
	.8byte	.LFB68
	.8byte	.LFE68-.LFB68
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xc0b
	.uleb128 0x1c
	.4byte	.LASF80
	.byte	0x1
	.2byte	0x20c
	.4byte	0x5f
	.4byte	.LLST19
	.uleb128 0x1c
	.4byte	.LASF81
	.byte	0x1
	.2byte	0x20c
	.4byte	0xc0b
	.4byte	.LLST20
	.uleb128 0x1d
	.string	"in"
	.byte	0x1
	.2byte	0x20e
	.4byte	0xc11
	.4byte	.LLST21
	.uleb128 0x1e
	.string	"g"
	.byte	0x1
	.2byte	0x20f
	.4byte	0xc1c
	.uleb128 0x1e
	.string	"f"
	.byte	0x1
	.2byte	0x210
	.4byte	0x5f
	.uleb128 0x1d
	.string	"n"
	.byte	0x1
	.2byte	0x211
	.4byte	0x5f
	.4byte	.LLST22
	.uleb128 0x1d
	.string	"m"
	.byte	0x1
	.2byte	0x212
	.4byte	0x5f
	.4byte	.LLST23
	.uleb128 0x1f
	.4byte	0xee3
	.8byte	.LBB100
	.4byte	.Ldebug_ranges0+0xb0
	.byte	0x1
	.2byte	0x21f
	.4byte	0x9a8
	.uleb128 0x20
	.4byte	0xef4
	.4byte	.LLST24
	.uleb128 0x20
	.4byte	0xf09
	.4byte	.LLST25
	.uleb128 0x20
	.4byte	0xeff
	.4byte	.LLST26
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0xb0
	.uleb128 0x22
	.4byte	0xf13
	.4byte	.LLST27
	.uleb128 0x22
	.4byte	0xf1d
	.4byte	.LLST28
	.uleb128 0x22
	.4byte	0xf27
	.4byte	.LLST29
	.uleb128 0x22
	.4byte	0xf31
	.4byte	.LLST30
	.uleb128 0x23
	.4byte	0xf3b
	.uleb128 0x23
	.4byte	0xf45
	.uleb128 0x23
	.4byte	0xf4f
	.uleb128 0x1f
	.4byte	0xfbc
	.8byte	.LBB102
	.4byte	.Ldebug_ranges0+0x110
	.byte	0x1
	.2byte	0x13a
	.4byte	0x6a9
	.uleb128 0x20
	.4byte	0xfd5
	.4byte	.LLST31
	.uleb128 0x20
	.4byte	0xfcc
	.4byte	.LLST32
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x110
	.uleb128 0x22
	.4byte	0xfde
	.4byte	.LLST33
	.uleb128 0x24
	.4byte	0x1198
	.8byte	.LBB104
	.4byte	.Ldebug_ranges0+0x160
	.byte	0x1
	.byte	0xfc
	.4byte	0x693
	.uleb128 0x20
	.4byte	0x11be
	.4byte	.LLST34
	.uleb128 0x20
	.4byte	0x11b3
	.4byte	.LLST35
	.uleb128 0x20
	.4byte	0x11a8
	.4byte	.LLST33
	.uleb128 0x25
	.8byte	.LVL61
	.4byte	0x1310
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x30
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x8f
	.sleb128 24
	.byte	0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL59
	.4byte	0xfe8
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	0xfbc
	.8byte	.LBB111
	.4byte	.Ldebug_ranges0+0x190
	.byte	0x1
	.2byte	0x13b
	.4byte	0x747
	.uleb128 0x20
	.4byte	0xfd5
	.4byte	.LLST37
	.uleb128 0x20
	.4byte	0xfcc
	.4byte	.LLST38
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x190
	.uleb128 0x22
	.4byte	0xfde
	.4byte	.LLST39
	.uleb128 0x27
	.4byte	0x1198
	.8byte	.LBB113
	.8byte	.LBE113-.LBB113
	.byte	0x1
	.byte	0xfc
	.4byte	0x731
	.uleb128 0x20
	.4byte	0x11be
	.4byte	.LLST40
	.uleb128 0x20
	.4byte	0x11b3
	.4byte	.LLST41
	.uleb128 0x20
	.4byte	0x11a8
	.4byte	.LLST39
	.uleb128 0x25
	.8byte	.LVL64
	.4byte	0x1310
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x30
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x8a
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL63
	.4byte	0xfe8
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8a
	.sleb128 0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x28
	.4byte	0x100b
	.8byte	.LBB120
	.8byte	.LBE120-.LBB120
	.byte	0x1
	.2byte	0x142
	.4byte	0x7a1
	.uleb128 0x29
	.8byte	.LBB121
	.8byte	.LBE121-.LBB121
	.uleb128 0x23
	.4byte	0x1268
	.uleb128 0x23
	.4byte	0x1271
	.uleb128 0x2a
	.4byte	0x1241
	.8byte	.LBB122
	.4byte	.Ldebug_ranges0+0x1c0
	.byte	0x1
	.byte	0xd6
	.uleb128 0x2b
	.8byte	.LVL73
	.4byte	0x131f
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x28
	.4byte	0x100b
	.8byte	.LBB126
	.8byte	.LBE126-.LBB126
	.byte	0x1
	.2byte	0x143
	.4byte	0x7fb
	.uleb128 0x29
	.8byte	.LBB127
	.8byte	.LBE127-.LBB127
	.uleb128 0x23
	.4byte	0x1268
	.uleb128 0x23
	.4byte	0x1271
	.uleb128 0x2a
	.4byte	0x1241
	.8byte	.LBB128
	.4byte	.Ldebug_ranges0+0x1f0
	.byte	0x1
	.byte	0xd6
	.uleb128 0x2b
	.8byte	.LVL77
	.4byte	0x131f
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x28
	.4byte	0x100b
	.8byte	.LBB132
	.8byte	.LBE132-.LBB132
	.byte	0x1
	.2byte	0x144
	.4byte	0x855
	.uleb128 0x29
	.8byte	.LBB133
	.8byte	.LBE133-.LBB133
	.uleb128 0x23
	.4byte	0x1268
	.uleb128 0x23
	.4byte	0x1271
	.uleb128 0x2a
	.4byte	0x1241
	.8byte	.LBB134
	.4byte	.Ldebug_ranges0+0x220
	.byte	0x1
	.byte	0xd6
	.uleb128 0x2b
	.8byte	.LVL81
	.4byte	0x131f
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	0xf5a
	.8byte	.LBB138
	.4byte	.Ldebug_ranges0+0x250
	.byte	0x1
	.2byte	0x147
	.4byte	0x992
	.uleb128 0x20
	.4byte	0xf85
	.4byte	.LLST43
	.uleb128 0x2c
	.4byte	0xf7b
	.uleb128 0x20
	.4byte	0xf71
	.4byte	.LLST44
	.uleb128 0x20
	.4byte	0xf67
	.4byte	.LLST45
	.uleb128 0x1f
	.4byte	0xf90
	.8byte	.LBB140
	.4byte	.Ldebug_ranges0+0x2a0
	.byte	0x1
	.2byte	0x127
	.4byte	0x923
	.uleb128 0x20
	.4byte	0xf9d
	.4byte	.LLST46
	.uleb128 0x20
	.4byte	0xfa7
	.4byte	.LLST47
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x2a0
	.uleb128 0x23
	.4byte	0xfb1
	.uleb128 0x2d
	.4byte	0xfe8
	.8byte	.LBB142
	.4byte	.Ldebug_ranges0+0x2f0
	.byte	0x1
	.2byte	0x116
	.uleb128 0x20
	.4byte	0xff8
	.4byte	.LLST48
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x2f0
	.uleb128 0x23
	.4byte	0x12cd
	.uleb128 0x2e
	.8byte	.LVL88
	.4byte	0x132b
	.4byte	0x8ff
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x1
	.byte	0x40
	.byte	0
	.uleb128 0x25
	.8byte	.LVL134
	.4byte	0x102e
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC8
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x40
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2d
	.4byte	0xf90
	.8byte	.LBB152
	.4byte	.Ldebug_ranges0+0x330
	.byte	0x1
	.2byte	0x128
	.uleb128 0x20
	.4byte	0xf9d
	.4byte	.LLST49
	.uleb128 0x20
	.4byte	0xfa7
	.4byte	.LLST50
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x330
	.uleb128 0x23
	.4byte	0xfb1
	.uleb128 0x2d
	.4byte	0xfe8
	.8byte	.LBB154
	.4byte	.Ldebug_ranges0+0x370
	.byte	0x1
	.2byte	0x116
	.uleb128 0x20
	.4byte	0xff8
	.4byte	.LLST51
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x370
	.uleb128 0x23
	.4byte	0x12cd
	.uleb128 0x25
	.8byte	.LVL91
	.4byte	0x132b
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x1
	.byte	0x40
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL55
	.4byte	0xfe8
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8
	.byte	0x30
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	0xc58
	.8byte	.LBB177
	.4byte	.Ldebug_ranges0+0x3a0
	.byte	0x1
	.2byte	0x223
	.4byte	0xaf9
	.uleb128 0x2c
	.4byte	0xc69
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x3a0
	.uleb128 0x22
	.4byte	0xc73
	.4byte	.LLST52
	.uleb128 0x23
	.4byte	0xc7d
	.uleb128 0x22
	.4byte	0xc87
	.4byte	.LLST53
	.uleb128 0x22
	.4byte	0xc91
	.4byte	.LLST54
	.uleb128 0x22
	.4byte	0xc9b
	.4byte	.LLST55
	.uleb128 0x22
	.4byte	0xca5
	.4byte	.LLST56
	.uleb128 0x28
	.4byte	0xcb0
	.8byte	.LBB179
	.8byte	.LBE179-.LBB179
	.byte	0x1
	.2byte	0x1c7
	.4byte	0xa34
	.uleb128 0x20
	.4byte	0xccb
	.4byte	.LLST57
	.uleb128 0x20
	.4byte	0xccb
	.4byte	.LLST57
	.uleb128 0x20
	.4byte	0xcc1
	.4byte	.LLST59
	.byte	0
	.uleb128 0x1f
	.4byte	0xe9b
	.8byte	.LBB181
	.4byte	.Ldebug_ranges0+0x3d0
	.byte	0x1
	.2byte	0x1cc
	.4byte	0xa5d
	.uleb128 0x2c
	.4byte	0xeac
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x3d0
	.uleb128 0x23
	.4byte	0xeb6
	.byte	0
	.byte	0
	.uleb128 0x28
	.4byte	0xcd6
	.8byte	.LBB189
	.8byte	.LBE189-.LBB189
	.byte	0x1
	.2byte	0x1f3
	.4byte	0xaa7
	.uleb128 0x2c
	.4byte	0xced
	.uleb128 0x2c
	.4byte	0xce3
	.uleb128 0x2d
	.4byte	0xec1
	.8byte	.LBB191
	.4byte	.Ldebug_ranges0+0x420
	.byte	0x1
	.2byte	0x1a4
	.uleb128 0x2c
	.4byte	0xed8
	.uleb128 0x20
	.4byte	0xece
	.4byte	.LLST60
	.byte	0
	.byte	0
	.uleb128 0x2e
	.8byte	.LVL99
	.4byte	0xcf8
	.4byte	0xac5
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8c
	.sleb128 0
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x8e
	.sleb128 0
	.byte	0
	.uleb128 0x2e
	.8byte	.LVL104
	.4byte	0xcf8
	.4byte	0xae3
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8c
	.sleb128 0
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x8e
	.sleb128 0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL113
	.4byte	0xcf8
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8c
	.sleb128 0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	0x11ca
	.8byte	.LBB198
	.4byte	.Ldebug_ranges0+0x460
	.byte	0x1
	.2byte	0x225
	.4byte	0xb3b
	.uleb128 0x20
	.4byte	0x11da
	.4byte	.LLST61
	.uleb128 0x25
	.8byte	.LVL115
	.4byte	0x1337
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC11
	.byte	0
	.byte	0
	.uleb128 0x1f
	.4byte	0xc22
	.8byte	.LBB201
	.4byte	.Ldebug_ranges0+0x490
	.byte	0x1
	.2byte	0x227
	.4byte	0xbb5
	.uleb128 0x2c
	.4byte	0xc2f
	.uleb128 0x21
	.4byte	.Ldebug_ranges0+0x490
	.uleb128 0x22
	.4byte	0xc39
	.4byte	.LLST62
	.uleb128 0x22
	.4byte	0xc43
	.4byte	.LLST63
	.uleb128 0x22
	.4byte	0xc4d
	.4byte	.LLST64
	.uleb128 0x2b
	.8byte	.LVL119
	.4byte	0x1342
	.uleb128 0x2b
	.8byte	.LVL124
	.4byte	0x1342
	.uleb128 0x2b
	.8byte	.LVL125
	.4byte	0x1342
	.uleb128 0x25
	.8byte	.LVL126
	.4byte	0x1342
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8c
	.sleb128 0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x2b
	.8byte	.LVL49
	.4byte	0x100b
	.uleb128 0x2b
	.8byte	.LVL51
	.4byte	0x100b
	.uleb128 0x2b
	.8byte	.LVL53
	.4byte	0x100b
	.uleb128 0x2b
	.8byte	.LVL54
	.4byte	0x100b
	.uleb128 0x2b
	.8byte	.LVL67
	.4byte	0x134e
	.uleb128 0x25
	.8byte	.LVL95
	.4byte	0x1359
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x87
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.4byte	0x9f
	.uleb128 0x8
	.byte	0x8
	.4byte	0x2c3
	.uleb128 0x9
	.4byte	0xc11
	.uleb128 0x8
	.byte	0x8
	.4byte	0x3a8
	.uleb128 0x2f
	.4byte	.LASF85
	.byte	0x1
	.2byte	0x1f9
	.byte	0x1
	.4byte	0xc58
	.uleb128 0x30
	.string	"g"
	.byte	0x1
	.2byte	0x1f9
	.4byte	0xc1c
	.uleb128 0x1e
	.string	"i"
	.byte	0x1
	.2byte	0x1fb
	.4byte	0x5f
	.uleb128 0x1e
	.string	"p"
	.byte	0x1
	.2byte	0x1fc
	.4byte	0x4c2
	.uleb128 0x1e
	.string	"q"
	.byte	0x1
	.2byte	0x1fd
	.4byte	0x4c2
	.byte	0
	.uleb128 0x31
	.4byte	.LASF83
	.byte	0x1
	.2byte	0x1af
	.4byte	0x5f
	.byte	0x1
	.4byte	0xcb0
	.uleb128 0x30
	.string	"g"
	.byte	0x1
	.2byte	0x1af
	.4byte	0xc1c
	.uleb128 0x1e
	.string	"s"
	.byte	0x1
	.2byte	0x1b1
	.4byte	0x4d6
	.uleb128 0x1e
	.string	"u"
	.byte	0x1
	.2byte	0x1b2
	.4byte	0x4d6
	.uleb128 0x1e
	.string	"v"
	.byte	0x1
	.2byte	0x1b3
	.4byte	0x4d6
	.uleb128 0x1e
	.string	"e"
	.byte	0x1
	.2byte	0x1b4
	.4byte	0x4bc
	.uleb128 0x1e
	.string	"p"
	.byte	0x1
	.2byte	0x1b5
	.4byte	0x4c2
	.uleb128 0x1e
	.string	"b"
	.byte	0x1
	.2byte	0x1b6
	.4byte	0x5f
	.byte	0
	.uleb128 0x31
	.4byte	.LASF84
	.byte	0x1
	.2byte	0x1a7
	.4byte	0x4d6
	.byte	0x1
	.4byte	0xcd6
	.uleb128 0x30
	.string	"u"
	.byte	0x1
	.2byte	0x1a7
	.4byte	0x4d6
	.uleb128 0x30
	.string	"e"
	.byte	0x1
	.2byte	0x1a7
	.4byte	0x4bc
	.byte	0
	.uleb128 0x2f
	.4byte	.LASF86
	.byte	0x1
	.2byte	0x19c
	.byte	0x1
	.4byte	0xcf8
	.uleb128 0x30
	.string	"g"
	.byte	0x1
	.2byte	0x19c
	.4byte	0xc1c
	.uleb128 0x30
	.string	"u"
	.byte	0x1
	.2byte	0x19c
	.4byte	0x4d6
	.byte	0
	.uleb128 0x32
	.4byte	.LASF125
	.byte	0x1
	.2byte	0x16f
	.8byte	.LFB63
	.8byte	.LFE63-.LFB63
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xe86
	.uleb128 0x33
	.string	"g"
	.byte	0x1
	.2byte	0x16f
	.4byte	0xc1c
	.4byte	.LLST1
	.uleb128 0x33
	.string	"u"
	.byte	0x1
	.2byte	0x16f
	.4byte	0x4d6
	.4byte	.LLST2
	.uleb128 0x33
	.string	"v"
	.byte	0x1
	.2byte	0x16f
	.4byte	0x4d6
	.4byte	.LLST3
	.uleb128 0x33
	.string	"e"
	.byte	0x1
	.2byte	0x16f
	.4byte	0x4bc
	.4byte	.LLST4
	.uleb128 0x1d
	.string	"d"
	.byte	0x1
	.2byte	0x171
	.4byte	0x5f
	.4byte	.LLST5
	.uleb128 0x34
	.4byte	.LASF87
	.4byte	0xe96
	.uleb128 0x9
	.byte	0x3
	.8byte	__PRETTY_FUNCTION__.4386
	.uleb128 0x28
	.4byte	0xec1
	.8byte	.LBB36
	.8byte	.LBE36-.LBB36
	.byte	0x1
	.2byte	0x18d
	.4byte	0xd9e
	.uleb128 0x20
	.4byte	0xed8
	.4byte	.LLST6
	.uleb128 0x20
	.4byte	0xece
	.4byte	.LLST7
	.byte	0
	.uleb128 0x1f
	.4byte	0xec1
	.8byte	.LBB38
	.4byte	.Ldebug_ranges0+0x30
	.byte	0x1
	.2byte	0x197
	.4byte	0xdc9
	.uleb128 0x20
	.4byte	0xed8
	.4byte	.LLST8
	.uleb128 0x20
	.4byte	0xece
	.4byte	.LLST9
	.byte	0
	.uleb128 0x2e
	.8byte	.LVL20
	.4byte	0x1364
	.4byte	0xe09
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC4
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC1
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.2byte	0x187
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x56
	.uleb128 0x9
	.byte	0x3
	.8byte	.LANCHOR0
	.byte	0
	.uleb128 0x2e
	.8byte	.LVL25
	.4byte	0x1364
	.4byte	0xe49
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC2
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC1
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.2byte	0x185
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x56
	.uleb128 0x9
	.byte	0x3
	.8byte	.LANCHOR0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL30
	.4byte	0x1364
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC3
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC1
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.2byte	0x186
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x56
	.uleb128 0x9
	.byte	0x3
	.8byte	.LANCHOR0
	.byte	0
	.byte	0
	.uleb128 0x12
	.4byte	0x3e
	.4byte	0xe96
	.uleb128 0x13
	.4byte	0x72
	.byte	0x4
	.byte	0
	.uleb128 0x3
	.4byte	0xe86
	.uleb128 0x31
	.4byte	.LASF88
	.byte	0x1
	.2byte	0x15e
	.4byte	0x4d6
	.byte	0x1
	.4byte	0xec1
	.uleb128 0x30
	.string	"g"
	.byte	0x1
	.2byte	0x15e
	.4byte	0xc1c
	.uleb128 0x1e
	.string	"v"
	.byte	0x1
	.2byte	0x160
	.4byte	0x4d6
	.byte	0
	.uleb128 0x2f
	.4byte	.LASF89
	.byte	0x1
	.2byte	0x14d
	.byte	0x1
	.4byte	0xee3
	.uleb128 0x30
	.string	"g"
	.byte	0x1
	.2byte	0x14d
	.4byte	0xc1c
	.uleb128 0x30
	.string	"v"
	.byte	0x1
	.2byte	0x14d
	.4byte	0x4d6
	.byte	0
	.uleb128 0x31
	.4byte	.LASF90
	.byte	0x1
	.2byte	0x12b
	.4byte	0xc1c
	.byte	0x1
	.4byte	0xf5a
	.uleb128 0x30
	.string	"in"
	.byte	0x1
	.2byte	0x12b
	.4byte	0xc11
	.uleb128 0x30
	.string	"n"
	.byte	0x1
	.2byte	0x12b
	.4byte	0x5f
	.uleb128 0x30
	.string	"m"
	.byte	0x1
	.2byte	0x12b
	.4byte	0x5f
	.uleb128 0x1e
	.string	"g"
	.byte	0x1
	.2byte	0x12d
	.4byte	0xc1c
	.uleb128 0x1e
	.string	"u"
	.byte	0x1
	.2byte	0x12e
	.4byte	0x4d6
	.uleb128 0x1e
	.string	"v"
	.byte	0x1
	.2byte	0x12f
	.4byte	0x4d6
	.uleb128 0x1e
	.string	"i"
	.byte	0x1
	.2byte	0x130
	.4byte	0x5f
	.uleb128 0x1e
	.string	"a"
	.byte	0x1
	.2byte	0x131
	.4byte	0x5f
	.uleb128 0x1e
	.string	"b"
	.byte	0x1
	.2byte	0x132
	.4byte	0x5f
	.uleb128 0x1e
	.string	"c"
	.byte	0x1
	.2byte	0x133
	.4byte	0x5f
	.byte	0
	.uleb128 0x2f
	.4byte	.LASF91
	.byte	0x1
	.2byte	0x11c
	.byte	0x1
	.4byte	0xf90
	.uleb128 0x30
	.string	"u"
	.byte	0x1
	.2byte	0x11c
	.4byte	0x4d6
	.uleb128 0x30
	.string	"v"
	.byte	0x1
	.2byte	0x11c
	.4byte	0x4d6
	.uleb128 0x30
	.string	"c"
	.byte	0x1
	.2byte	0x11c
	.4byte	0x5f
	.uleb128 0x30
	.string	"e"
	.byte	0x1
	.2byte	0x11c
	.4byte	0x4bc
	.byte	0
	.uleb128 0x2f
	.4byte	.LASF92
	.byte	0x1
	.2byte	0x10d
	.byte	0x1
	.4byte	0xfbc
	.uleb128 0x30
	.string	"u"
	.byte	0x1
	.2byte	0x10d
	.4byte	0x4d6
	.uleb128 0x30
	.string	"e"
	.byte	0x1
	.2byte	0x10d
	.4byte	0x4bc
	.uleb128 0x1e
	.string	"p"
	.byte	0x1
	.2byte	0x10f
	.4byte	0x4c2
	.byte	0
	.uleb128 0x35
	.4byte	.LASF93
	.byte	0x1
	.byte	0xf5
	.4byte	0x9d
	.byte	0x1
	.4byte	0xfe8
	.uleb128 0x36
	.string	"n"
	.byte	0x1
	.byte	0xf5
	.4byte	0x138
	.uleb128 0x36
	.string	"s"
	.byte	0x1
	.byte	0xf5
	.4byte	0x138
	.uleb128 0x37
	.string	"p"
	.byte	0x1
	.byte	0xf7
	.4byte	0x9d
	.byte	0
	.uleb128 0x35
	.4byte	.LASF94
	.byte	0x1
	.byte	0xdc
	.4byte	0x9d
	.byte	0x1
	.4byte	0x100b
	.uleb128 0x36
	.string	"s"
	.byte	0x1
	.byte	0xdc
	.4byte	0x138
	.uleb128 0x37
	.string	"p"
	.byte	0x1
	.byte	0xde
	.4byte	0x9d
	.byte	0
	.uleb128 0x38
	.4byte	.LASF95
	.byte	0x1
	.byte	0xc2
	.4byte	0x5f
	.byte	0x1
	.4byte	0x102e
	.uleb128 0x37
	.string	"x"
	.byte	0x1
	.byte	0xc4
	.4byte	0x5f
	.uleb128 0x37
	.string	"c"
	.byte	0x1
	.byte	0xc5
	.4byte	0x5f
	.byte	0
	.uleb128 0x39
	.4byte	.LASF126
	.byte	0x1
	.byte	0x98
	.8byte	.LFB54
	.8byte	.LFE54-.LFB54
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x1187
	.uleb128 0x3a
	.string	"fmt"
	.byte	0x1
	.byte	0x98
	.4byte	0x108
	.4byte	.LLST10
	.uleb128 0x3b
	.uleb128 0x3c
	.string	"ap"
	.byte	0x1
	.byte	0xb5
	.4byte	0x12d
	.4byte	.LLST11
	.uleb128 0x3d
	.string	"buf"
	.byte	0x1
	.byte	0xb6
	.4byte	0x1187
	.uleb128 0x4
	.byte	0x91
	.sleb128 -8216
	.uleb128 0x24
	.4byte	0x120f
	.8byte	.LBB42
	.4byte	.Ldebug_ranges0+0x60
	.byte	0x1
	.byte	0xb9
	.4byte	0x10d9
	.uleb128 0x20
	.4byte	0x1235
	.4byte	.LLST12
	.uleb128 0x20
	.4byte	0x122a
	.4byte	.LLST13
	.uleb128 0x20
	.4byte	0x121f
	.4byte	.LLST14
	.uleb128 0x25
	.8byte	.LVL36
	.4byte	0x136f
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xa
	.2byte	0x2000
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x56
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x57
	.uleb128 0x2
	.byte	0x91
	.sleb128 40
	.byte	0
	.byte	0
	.uleb128 0x27
	.4byte	0x11e7
	.8byte	.LBB50
	.8byte	.LBE50-.LBB50
	.byte	0x1
	.byte	0xbc
	.4byte	0x1123
	.uleb128 0x20
	.4byte	0x1202
	.4byte	.LLST15
	.uleb128 0x2c
	.4byte	0x11f7
	.uleb128 0x25
	.8byte	.LVL38
	.4byte	0x137e
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC5
	.byte	0
	.byte	0
	.uleb128 0x27
	.4byte	0x11e7
	.8byte	.LBB52
	.8byte	.LBE52-.LBB52
	.byte	0x1
	.byte	0xbe
	.4byte	0x1173
	.uleb128 0x20
	.4byte	0x1202
	.4byte	.LLST16
	.uleb128 0x2c
	.4byte	0x11f7
	.uleb128 0x25
	.8byte	.LVL39
	.4byte	0x137e
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC7
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x56
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL40
	.4byte	0x1389
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x12
	.4byte	0x37
	.4byte	0x1198
	.uleb128 0x3e
	.4byte	0x72
	.2byte	0x1fff
	.byte	0
	.uleb128 0x3f
	.4byte	.LASF99
	.byte	0x4
	.byte	0x3b
	.4byte	0x9d
	.byte	0x3
	.4byte	0x11ca
	.uleb128 0x40
	.4byte	.LASF96
	.byte	0x4
	.byte	0x3b
	.4byte	0x9d
	.uleb128 0x40
	.4byte	.LASF97
	.byte	0x4
	.byte	0x3b
	.4byte	0x5f
	.uleb128 0x40
	.4byte	.LASF98
	.byte	0x4
	.byte	0x3b
	.4byte	0x138
	.byte	0
	.uleb128 0x3f
	.4byte	.LASF100
	.byte	0x3
	.byte	0x66
	.4byte	0x5f
	.byte	0x3
	.4byte	0x11e7
	.uleb128 0x40
	.4byte	.LASF101
	.byte	0x3
	.byte	0x66
	.4byte	0x113
	.uleb128 0x3b
	.byte	0
	.uleb128 0x3f
	.4byte	.LASF102
	.byte	0x3
	.byte	0x5f
	.4byte	0x5f
	.byte	0x3
	.4byte	0x120f
	.uleb128 0x40
	.4byte	.LASF103
	.byte	0x3
	.byte	0x5f
	.4byte	0xc17
	.uleb128 0x40
	.4byte	.LASF101
	.byte	0x3
	.byte	0x5f
	.4byte	0x113
	.uleb128 0x3b
	.byte	0
	.uleb128 0x3f
	.4byte	.LASF104
	.byte	0x3
	.byte	0x2b
	.4byte	0x5f
	.byte	0x3
	.4byte	0x1241
	.uleb128 0x36
	.string	"__s"
	.byte	0x3
	.byte	0x2b
	.4byte	0xa5
	.uleb128 0x40
	.4byte	.LASF101
	.byte	0x3
	.byte	0x2b
	.4byte	0x113
	.uleb128 0x40
	.4byte	.LASF105
	.byte	0x3
	.byte	0x2b
	.4byte	0x118
	.byte	0
	.uleb128 0x41
	.4byte	.LASF127
	.byte	0x2
	.byte	0x2c
	.4byte	0x5f
	.byte	0x3
	.uleb128 0x42
	.4byte	0x100b
	.8byte	.LFB55
	.8byte	.LFE55-.LFB55
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x12a9
	.uleb128 0x22
	.4byte	0x101b
	.4byte	.LLST0
	.uleb128 0x23
	.4byte	0x1024
	.uleb128 0x24
	.4byte	0x1241
	.8byte	.LBB32
	.4byte	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0xd6
	.4byte	0x129b
	.uleb128 0x2b
	.8byte	.LVL4
	.4byte	0x131f
	.byte	0
	.uleb128 0x2b
	.8byte	.LVL1
	.4byte	0x134e
	.byte	0
	.uleb128 0x42
	.4byte	0xfe8
	.8byte	.LFB56
	.8byte	.LFE56-.LFB56
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x1310
	.uleb128 0x20
	.4byte	0xff8
	.4byte	.LLST17
	.uleb128 0x22
	.4byte	0x1001
	.4byte	.LLST18
	.uleb128 0x2e
	.8byte	.LVL42
	.4byte	0x132b
	.4byte	0x12ee
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.byte	0
	.uleb128 0x25
	.8byte	.LVL46
	.4byte	0x102e
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x53
	.uleb128 0x9
	.byte	0x3
	.8byte	.LC8
	.uleb128 0x26
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x8f
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x43
	.4byte	.LASF99
	.4byte	.LASF114
	.byte	0xf
	.byte	0
	.4byte	.LASF99
	.uleb128 0x44
	.4byte	.LASF106
	.4byte	.LASF106
	.byte	0x8
	.2byte	0x1b1
	.uleb128 0x44
	.4byte	.LASF107
	.4byte	.LASF107
	.byte	0xc
	.2byte	0x21b
	.uleb128 0x45
	.4byte	.LASF108
	.4byte	.LASF108
	.byte	0x3
	.byte	0x57
	.uleb128 0x44
	.4byte	.LASF109
	.4byte	.LASF109
	.byte	0xc
	.2byte	0x233
	.uleb128 0x45
	.4byte	.LASF110
	.4byte	.LASF110
	.byte	0xd
	.byte	0x4f
	.uleb128 0x45
	.4byte	.LASF111
	.4byte	.LASF111
	.byte	0xa
	.byte	0xc7
	.uleb128 0x45
	.4byte	.LASF112
	.4byte	.LASF112
	.byte	0xe
	.byte	0x45
	.uleb128 0x43
	.4byte	.LASF113
	.4byte	.LASF115
	.byte	0xf
	.byte	0
	.4byte	.LASF113
	.uleb128 0x45
	.4byte	.LASF116
	.4byte	.LASF116
	.byte	0x3
	.byte	0x55
	.uleb128 0x44
	.4byte	.LASF117
	.4byte	.LASF117
	.byte	0xc
	.2byte	0x266
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x32
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x33
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x34
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x35
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x36
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x37
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x38
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x39
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3a
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x3b
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x3c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x3d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x3e
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x3f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x40
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x41
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x42
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x43
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x6e
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x44
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x45
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST19:
	.8byte	.LVL47
	.8byte	.LVL49-1
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL49-1
	.8byte	.LFE68
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST20:
	.8byte	.LVL47
	.8byte	.LVL49-1
	.2byte	0x1
	.byte	0x54
	.8byte	.LVL49-1
	.8byte	.LFE68
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST21:
	.8byte	.LVL48
	.8byte	.LVL127
	.2byte	0x1
	.byte	0x67
	.8byte	.LVL128
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x67
	.8byte	0
	.8byte	0
.LLST22:
	.8byte	.LVL50
	.8byte	.LVL51-1
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST23:
	.8byte	.LVL52
	.8byte	.LVL53-1
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL53-1
	.8byte	.LVL66
	.2byte	0x1
	.byte	0x68
	.8byte	0
	.8byte	0
.LLST24:
	.8byte	.LVL54
	.8byte	.LVL127
	.2byte	0x1
	.byte	0x67
	.8byte	.LVL128
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x67
	.8byte	0
	.8byte	0
.LLST25:
	.8byte	.LVL54
	.8byte	.LVL69
	.2byte	0x1
	.byte	0x6d
	.8byte	0
	.8byte	0
.LLST26:
	.8byte	.LVL54
	.8byte	.LVL68
	.2byte	0x1
	.byte	0x69
	.8byte	0
	.8byte	0
.LLST27:
	.8byte	.LVL56
	.8byte	.LVL57
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL57
	.8byte	.LVL94
	.2byte	0x1
	.byte	0x6c
	.8byte	.LVL133
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x6c
	.8byte	0
	.8byte	0
.LLST28:
	.8byte	.LVL85
	.8byte	.LVL94
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL133
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST29:
	.8byte	.LVL86
	.8byte	.LVL94
	.2byte	0x1
	.byte	0x6e
	.8byte	.LVL133
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST30:
	.8byte	.LVL65
	.8byte	.LVL69
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST31:
	.8byte	.LVL58
	.8byte	.LVL61
	.2byte	0x2
	.byte	0x48
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST32:
	.8byte	.LVL58
	.8byte	.LVL61
	.2byte	0x9
	.byte	0x89
	.sleb128 0
	.byte	0x8
	.byte	0x20
	.byte	0x24
	.byte	0x8
	.byte	0x20
	.byte	0x26
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST33:
	.8byte	.LVL59
	.8byte	.LVL61-1
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST34:
	.8byte	.LVL59
	.8byte	.LVL60
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL60
	.8byte	.LVL61-1
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL61-1
	.8byte	.LVL61
	.2byte	0x3
	.byte	0x8f
	.sleb128 24
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST35:
	.8byte	.LVL59
	.8byte	.LVL61
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST37:
	.8byte	.LVL62
	.8byte	.LVL64
	.2byte	0x2
	.byte	0x48
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST38:
	.8byte	.LVL62
	.8byte	.LVL64
	.2byte	0x9
	.byte	0x8d
	.sleb128 0
	.byte	0x8
	.byte	0x20
	.byte	0x24
	.byte	0x8
	.byte	0x20
	.byte	0x26
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST39:
	.8byte	.LVL63
	.8byte	.LVL64-1
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST40:
	.8byte	.LVL63
	.8byte	.LVL64
	.2byte	0x1
	.byte	0x6a
	.8byte	0
	.8byte	0
.LLST41:
	.8byte	.LVL63
	.8byte	.LVL64
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST43:
	.8byte	.LVL86
	.8byte	.LVL93
	.2byte	0x1
	.byte	0x66
	.8byte	.LVL133
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x66
	.8byte	0
	.8byte	0
.LLST44:
	.8byte	.LVL86
	.8byte	.LVL93
	.2byte	0x1
	.byte	0x6e
	.8byte	.LVL133
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST45:
	.8byte	.LVL86
	.8byte	.LVL93
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL133
	.8byte	.LFE68
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST46:
	.8byte	.LVL87
	.8byte	.LVL90
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL90
	.8byte	.LVL94
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST47:
	.8byte	.LVL87
	.8byte	.LVL90
	.2byte	0x1
	.byte	0x66
	.8byte	0
	.8byte	0
.LLST48:
	.8byte	.LVL87
	.8byte	.LVL89
	.2byte	0x2
	.byte	0x40
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST49:
	.8byte	.LVL90
	.8byte	.LVL94
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST50:
	.8byte	.LVL90
	.8byte	.LVL93
	.2byte	0x1
	.byte	0x66
	.8byte	0
	.8byte	0
.LLST51:
	.8byte	.LVL90
	.8byte	.LVL92
	.2byte	0x2
	.byte	0x40
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST52:
	.8byte	.LVL96
	.8byte	.LVL114
	.2byte	0x1
	.byte	0x6e
	.8byte	.LVL128
	.8byte	.LVL133
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST53:
	.8byte	.LVL107
	.8byte	.LVL112
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	.LVL112
	.8byte	.LVL113-1
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL128
	.8byte	.LVL129
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	.LVL129
	.8byte	.LVL130
	.2byte	0x1
	.byte	0x55
	.8byte	0
	.8byte	0
.LLST54:
	.8byte	.LVL98
	.8byte	.LVL99-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL100
	.8byte	.LVL104-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL110
	.8byte	.LVL113-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL128
	.8byte	.LVL130
	.2byte	0x1
	.byte	0x56
	.8byte	0
	.8byte	0
.LLST55:
	.8byte	.LVL97
	.8byte	.LVL105
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL107
	.8byte	.LVL113-1
	.2byte	0x1
	.byte	0x58
	.8byte	.LVL128
	.8byte	.LVL131
	.2byte	0x1
	.byte	0x58
	.8byte	0
	.8byte	0
.LLST56:
	.8byte	.LVL108
	.8byte	.LVL109
	.2byte	0x1
	.byte	0x57
	.8byte	.LVL112
	.8byte	.LVL113-1
	.2byte	0x1
	.byte	0x57
	.8byte	.LVL129
	.8byte	.LVL130
	.2byte	0x2
	.byte	0x31
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST57:
	.8byte	.LVL98
	.8byte	.LVL99-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL102
	.8byte	.LVL104-1
	.2byte	0x1
	.byte	0x56
	.8byte	0
	.8byte	0
.LLST59:
	.8byte	.LVL102
	.8byte	.LVL103
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST60:
	.8byte	.LVL132
	.8byte	.LVL133
	.2byte	0x1
	.byte	0x6c
	.8byte	0
	.8byte	0
.LLST61:
	.8byte	.LVL114
	.8byte	.LVL115
	.2byte	0xa
	.byte	0x3
	.8byte	.LC11
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST62:
	.8byte	.LVL116
	.8byte	.LVL121
	.2byte	0x1
	.byte	0x6e
	.8byte	.LVL121
	.8byte	.LVL122
	.2byte	0x3
	.byte	0x8e
	.sleb128 -1
	.byte	0x9f
	.8byte	.LVL122
	.8byte	.LVL126
	.2byte	0x1
	.byte	0x6e
	.8byte	0
	.8byte	0
.LLST63:
	.8byte	.LVL117
	.8byte	.LVL119-1
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL119
	.8byte	.LVL120
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL120
	.8byte	.LVL123
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST64:
	.8byte	.LVL118
	.8byte	.LVL120
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST1:
	.8byte	.LVL6
	.8byte	.LVL18
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL18
	.8byte	.LVL20
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	.LVL20
	.8byte	.LVL23
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL23
	.8byte	.LVL25
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	.LVL25
	.8byte	.LVL28
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL28
	.8byte	.LFE63
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST2:
	.8byte	.LVL6
	.8byte	.LVL17
	.2byte	0x1
	.byte	0x54
	.8byte	.LVL17
	.8byte	.LVL20
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.8byte	.LVL20
	.8byte	.LVL22
	.2byte	0x1
	.byte	0x54
	.8byte	.LVL22
	.8byte	.LVL25
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.8byte	.LVL25
	.8byte	.LVL27
	.2byte	0x1
	.byte	0x54
	.8byte	.LVL27
	.8byte	.LFE63
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST3:
	.8byte	.LVL6
	.8byte	.LVL19
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL19
	.8byte	.LVL20
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.8byte	.LVL20
	.8byte	.LVL24
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL24
	.8byte	.LVL25-1
	.2byte	0x3
	.byte	0x77
	.sleb128 -4
	.byte	0x9f
	.8byte	.LVL25-1
	.8byte	.LVL25
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.8byte	.LVL25
	.8byte	.LVL29
	.2byte	0x1
	.byte	0x55
	.8byte	.LVL29
	.8byte	.LVL30-1
	.2byte	0x3
	.byte	0x77
	.sleb128 -4
	.byte	0x9f
	.8byte	.LVL30-1
	.8byte	.LFE63
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST4:
	.8byte	.LVL6
	.8byte	.LVL16
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL16
	.8byte	.LVL20-1
	.2byte	0x3
	.byte	0x7b
	.sleb128 -16
	.byte	0x9f
	.8byte	.LVL20-1
	.8byte	.LVL20
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x56
	.byte	0x9f
	.8byte	.LVL20
	.8byte	.LVL21
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL21
	.8byte	.LVL25-1
	.2byte	0x3
	.byte	0x7b
	.sleb128 -16
	.byte	0x9f
	.8byte	.LVL25-1
	.8byte	.LVL25
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x56
	.byte	0x9f
	.8byte	.LVL25
	.8byte	.LVL26
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL26
	.8byte	.LVL30-1
	.2byte	0x3
	.byte	0x7b
	.sleb128 -16
	.byte	0x9f
	.8byte	.LVL30-1
	.8byte	.LFE63
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x56
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST5:
	.8byte	.LVL7
	.8byte	.LVL10
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL11
	.8byte	.LVL12
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL13
	.8byte	.LVL14
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL15
	.8byte	.LVL20-1
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL20
	.8byte	.LVL25-1
	.2byte	0x1
	.byte	0x5a
	.8byte	.LVL25
	.8byte	.LVL30-1
	.2byte	0x1
	.byte	0x5a
	.8byte	0
	.8byte	0
.LLST6:
	.8byte	.LVL8
	.8byte	.LVL9
	.2byte	0x1
	.byte	0x54
	.8byte	0
	.8byte	0
.LLST7:
	.8byte	.LVL8
	.8byte	.LVL9
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST8:
	.8byte	.LVL11
	.8byte	.LVL12
	.2byte	0x1
	.byte	0x55
	.8byte	0
	.8byte	0
.LLST9:
	.8byte	.LVL11
	.8byte	.LVL12
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
.LLST10:
	.8byte	.LVL31
	.8byte	.LVL34
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL34
	.8byte	.LVL36-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL36-1
	.8byte	.LFE54
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST11:
	.8byte	.LVL32
	.8byte	.LVL35
	.2byte	0x6
	.byte	0x71
	.sleb128 0
	.byte	0x6
	.byte	0x23
	.uleb128 0x28
	.byte	0x9f
	.8byte	.LVL35
	.8byte	.LVL36-1
	.2byte	0x1
	.byte	0x57
	.8byte	.LVL36-1
	.8byte	.LFE54
	.2byte	0x3
	.byte	0x91
	.sleb128 40
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST12:
	.8byte	.LVL32
	.8byte	.LVL35
	.2byte	0x6
	.byte	0x71
	.sleb128 0
	.byte	0x6
	.byte	0x23
	.uleb128 0x28
	.byte	0x9f
	.8byte	.LVL35
	.8byte	.LVL36-1
	.2byte	0x1
	.byte	0x57
	.8byte	.LVL36-1
	.8byte	.LVL36
	.2byte	0x3
	.byte	0x91
	.sleb128 40
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST13:
	.8byte	.LVL32
	.8byte	.LVL34
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL34
	.8byte	.LVL36-1
	.2byte	0x1
	.byte	0x56
	.8byte	.LVL36-1
	.8byte	.LVL36
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST14:
	.8byte	.LVL32
	.8byte	.LVL33
	.2byte	0x8
	.byte	0x71
	.sleb128 0
	.byte	0x6
	.byte	0xa
	.2byte	0x2018
	.byte	0x1c
	.byte	0x9f
	.8byte	.LVL33
	.8byte	.LVL36
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST15:
	.8byte	.LVL37
	.8byte	.LVL38
	.2byte	0xa
	.byte	0x3
	.8byte	.LC5
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST16:
	.8byte	.LVL38
	.8byte	.LVL39
	.2byte	0xa
	.byte	0x3
	.8byte	.LC7
	.byte	0x9f
	.8byte	0
	.8byte	0
.LLST0:
	.8byte	.LVL0
	.8byte	.LVL2
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.8byte	.LVL2
	.8byte	.LVL5
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST17:
	.8byte	.LVL41
	.8byte	.LVL42-1
	.2byte	0x1
	.byte	0x53
	.8byte	.LVL42-1
	.8byte	.LVL43
	.2byte	0x1
	.byte	0x6f
	.8byte	.LVL43
	.8byte	.LVL44
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x53
	.byte	0x9f
	.8byte	.LVL44
	.8byte	.LFE56
	.2byte	0x1
	.byte	0x6f
	.8byte	0
	.8byte	0
.LLST18:
	.8byte	.LVL42
	.8byte	.LVL45
	.2byte	0x1
	.byte	0x53
	.8byte	0
	.8byte	0
	.section	.debug_aranges,"",@progbits
	.4byte	0x3c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x8
	.byte	0
	.2byte	0
	.2byte	0
	.8byte	.Ltext0
	.8byte	.Letext0-.Ltext0
	.8byte	.LFB68
	.8byte	.LFE68-.LFB68
	.8byte	0
	.8byte	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.8byte	.LBB32
	.8byte	.LBE32
	.8byte	.LBB35
	.8byte	.LBE35
	.8byte	0
	.8byte	0
	.8byte	.LBB38
	.8byte	.LBE38
	.8byte	.LBB41
	.8byte	.LBE41
	.8byte	0
	.8byte	0
	.8byte	.LBB42
	.8byte	.LBE42
	.8byte	.LBB47
	.8byte	.LBE47
	.8byte	.LBB48
	.8byte	.LBE48
	.8byte	.LBB49
	.8byte	.LBE49
	.8byte	0
	.8byte	0
	.8byte	.LBB100
	.8byte	.LBE100
	.8byte	.LBB174
	.8byte	.LBE174
	.8byte	.LBB175
	.8byte	.LBE175
	.8byte	.LBB176
	.8byte	.LBE176
	.8byte	.LBB207
	.8byte	.LBE207
	.8byte	0
	.8byte	0
	.8byte	.LBB102
	.8byte	.LBE102
	.8byte	.LBB116
	.8byte	.LBE116
	.8byte	.LBB117
	.8byte	.LBE117
	.8byte	.LBB118
	.8byte	.LBE118
	.8byte	0
	.8byte	0
	.8byte	.LBB104
	.8byte	.LBE104
	.8byte	.LBB107
	.8byte	.LBE107
	.8byte	0
	.8byte	0
	.8byte	.LBB111
	.8byte	.LBE111
	.8byte	.LBB119
	.8byte	.LBE119
	.8byte	0
	.8byte	0
	.8byte	.LBB122
	.8byte	.LBE122
	.8byte	.LBB125
	.8byte	.LBE125
	.8byte	0
	.8byte	0
	.8byte	.LBB128
	.8byte	.LBE128
	.8byte	.LBB131
	.8byte	.LBE131
	.8byte	0
	.8byte	0
	.8byte	.LBB134
	.8byte	.LBE134
	.8byte	.LBB137
	.8byte	.LBE137
	.8byte	0
	.8byte	0
	.8byte	.LBB138
	.8byte	.LBE138
	.8byte	.LBB167
	.8byte	.LBE167
	.8byte	.LBB168
	.8byte	.LBE168
	.8byte	.LBB169
	.8byte	.LBE169
	.8byte	0
	.8byte	0
	.8byte	.LBB140
	.8byte	.LBE140
	.8byte	.LBB151
	.8byte	.LBE151
	.8byte	.LBB160
	.8byte	.LBE160
	.8byte	.LBB163
	.8byte	.LBE163
	.8byte	0
	.8byte	0
	.8byte	.LBB142
	.8byte	.LBE142
	.8byte	.LBB146
	.8byte	.LBE146
	.8byte	.LBB147
	.8byte	.LBE147
	.8byte	0
	.8byte	0
	.8byte	.LBB152
	.8byte	.LBE152
	.8byte	.LBB161
	.8byte	.LBE161
	.8byte	.LBB162
	.8byte	.LBE162
	.8byte	0
	.8byte	0
	.8byte	.LBB154
	.8byte	.LBE154
	.8byte	.LBB157
	.8byte	.LBE157
	.8byte	0
	.8byte	0
	.8byte	.LBB177
	.8byte	.LBE177
	.8byte	.LBB206
	.8byte	.LBE206
	.8byte	0
	.8byte	0
	.8byte	.LBB181
	.8byte	.LBE181
	.8byte	.LBB186
	.8byte	.LBE186
	.8byte	.LBB187
	.8byte	.LBE187
	.8byte	.LBB188
	.8byte	.LBE188
	.8byte	0
	.8byte	0
	.8byte	.LBB191
	.8byte	.LBE191
	.8byte	.LBB195
	.8byte	.LBE195
	.8byte	.LBB196
	.8byte	.LBE196
	.8byte	0
	.8byte	0
	.8byte	.LBB198
	.8byte	.LBE198
	.8byte	.LBB204
	.8byte	.LBE204
	.8byte	0
	.8byte	0
	.8byte	.LBB201
	.8byte	.LBE201
	.8byte	.LBB205
	.8byte	.LBE205
	.8byte	0
	.8byte	0
	.8byte	.Ltext0
	.8byte	.Letext0
	.8byte	.LFB68
	.8byte	.LFE68
	.8byte	0
	.8byte	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF79:
	.string	"hello"
.LASF47:
	.string	"_shortbuf"
.LASF122:
	.string	"_IO_lock_t"
.LASF118:
	.string	"GNU C11 7.5.0 -Asystem=linux -Asystem=unix -Asystem=posix -msecure-plt -mcpu=power8 -g -O3 -fstack-protector-strong"
.LASF68:
	.string	"stderr"
.LASF36:
	.string	"_IO_buf_end"
.LASF110:
	.string	"__ctype_b_loc"
.LASF94:
	.string	"xmalloc"
.LASF34:
	.string	"_IO_write_end"
.LASF0:
	.string	"unsigned int"
.LASF75:
	.string	"next"
.LASF28:
	.string	"_flags"
.LASF120:
	.string	"/home/fi6468he-s/labs/lab4"
.LASF93:
	.string	"xcalloc"
.LASF40:
	.string	"_markers"
.LASF126:
	.string	"error"
.LASF115:
	.string	"__builtin___vsprintf_chk"
.LASF82:
	.string	"progname"
.LASF84:
	.string	"other"
.LASF62:
	.string	"_pos"
.LASF67:
	.string	"stdout"
.LASF39:
	.string	"_IO_save_end"
.LASF107:
	.string	"malloc"
.LASF98:
	.string	"__len"
.LASF88:
	.string	"leave_excess"
.LASF125:
	.string	"push"
.LASF10:
	.string	"long long unsigned int"
.LASF70:
	.string	"sys_errlist"
.LASF38:
	.string	"_IO_backup_base"
.LASF49:
	.string	"_offset"
.LASF69:
	.string	"sys_nerr"
.LASF96:
	.string	"__dest"
.LASF14:
	.string	"_ISlower"
.LASF42:
	.string	"_fileno"
.LASF25:
	.string	"__gnuc_va_list"
.LASF27:
	.string	"size_t"
.LASF17:
	.string	"_ISxdigit"
.LASF31:
	.string	"_IO_read_base"
.LASF1:
	.string	"_Bool"
.LASF80:
	.string	"argc"
.LASF66:
	.string	"stdin"
.LASF60:
	.string	"_next"
.LASF71:
	.string	"graph_t"
.LASF109:
	.string	"free"
.LASF74:
	.string	"edge"
.LASF101:
	.string	"__fmt"
.LASF104:
	.string	"vsprintf"
.LASF85:
	.string	"free_graph"
.LASF2:
	.string	"char"
.LASF116:
	.string	"__fprintf_chk"
.LASF55:
	.string	"_mode"
.LASF59:
	.string	"_IO_marker"
.LASF29:
	.string	"_IO_read_ptr"
.LASF117:
	.string	"exit"
.LASF73:
	.string	"node_t"
.LASF78:
	.string	"atomic_int"
.LASF26:
	.string	"va_list"
.LASF32:
	.string	"_IO_write_base"
.LASF63:
	.string	"_IO_2_1_stdin_"
.LASF9:
	.string	"long long int"
.LASF100:
	.string	"printf"
.LASF64:
	.string	"_IO_2_1_stdout_"
.LASF113:
	.string	"__vsprintf_chk"
.LASF37:
	.string	"_IO_save_base"
.LASF86:
	.string	"relabel"
.LASF22:
	.string	"_IScntrl"
.LASF108:
	.string	"__printf_chk"
.LASF105:
	.string	"__ap"
.LASF16:
	.string	"_ISdigit"
.LASF99:
	.string	"memset"
.LASF89:
	.string	"enter_excess"
.LASF18:
	.string	"_ISspace"
.LASF50:
	.string	"__pad1"
.LASF51:
	.string	"__pad2"
.LASF52:
	.string	"__pad3"
.LASF53:
	.string	"__pad4"
.LASF54:
	.string	"__pad5"
.LASF76:
	.string	"edge_t"
.LASF106:
	.string	"_IO_getc"
.LASF46:
	.string	"_vtable_offset"
.LASF72:
	.string	"excess"
.LASF81:
	.string	"argv"
.LASF92:
	.string	"add_edge"
.LASF97:
	.string	"__ch"
.LASF23:
	.string	"_ISpunct"
.LASF30:
	.string	"_IO_read_end"
.LASF19:
	.string	"_ISprint"
.LASF95:
	.string	"next_int"
.LASF5:
	.string	"short int"
.LASF91:
	.string	"connect"
.LASF7:
	.string	"long int"
.LASF111:
	.string	"fclose"
.LASF102:
	.string	"fprintf"
.LASF123:
	.string	"_IO_FILE_plus"
.LASF20:
	.string	"_ISgraph"
.LASF114:
	.string	"__builtin_memset"
.LASF48:
	.string	"_lock"
.LASF8:
	.string	"long unsigned int"
.LASF44:
	.string	"_old_offset"
.LASF58:
	.string	"_IO_FILE"
.LASF15:
	.string	"_ISalpha"
.LASF83:
	.string	"preflow"
.LASF112:
	.string	"__assert_fail"
.LASF4:
	.string	"unsigned char"
.LASF61:
	.string	"_sbuf"
.LASF127:
	.string	"getchar"
.LASF33:
	.string	"_IO_write_ptr"
.LASF103:
	.string	"__stream"
.LASF24:
	.string	"_ISalnum"
.LASF11:
	.string	"__off_t"
.LASF21:
	.string	"_ISblank"
.LASF3:
	.string	"signed char"
.LASF90:
	.string	"new_graph"
.LASF119:
	.string	"sequential.c"
.LASF6:
	.string	"short unsigned int"
.LASF124:
	.string	"main"
.LASF121:
	.string	"__builtin_va_list"
.LASF87:
	.string	"__PRETTY_FUNCTION__"
.LASF41:
	.string	"_chain"
.LASF13:
	.string	"_ISupper"
.LASF57:
	.string	"FILE"
.LASF43:
	.string	"_flags2"
.LASF77:
	.string	"list_t"
.LASF45:
	.string	"_cur_column"
.LASF65:
	.string	"_IO_2_1_stderr_"
.LASF12:
	.string	"__off64_t"
.LASF56:
	.string	"_unused2"
.LASF35:
	.string	"_IO_buf_base"
	.ident	"GCC: (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0"
	.section	.note.GNU-stack,"",@progbits
