import scala.util._
import java.util.Scanner
import java.io._
import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.{Await,ExecutionContext,Future,Promise}
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.io._

import math.abs

case class Flow(f: Int)
case class Debug(debug: Boolean)
case class Control(control:ActorRef)
case class Source(n: Int)

case object Print
case object Start
case object Excess
case object Maxflow
case object Sink
case object Hello

// Self defined stuff
case class PushFinished(ed:Edge, otherq:List[Edge])
case object Run
case class CannotPush(flow:Int)
case class Push(ed:Edge, h:Int, flow:Int, otherq:List[Edge])
case class Finished(e:Int)
case class SourceExcess(e:Int)
case class SinkExcess(e:Int)


class Edge(var u: ActorRef, var v: ActorRef, var c: Int) {
	var	f = 0
}

class Node(val index: Int) extends Actor {
	var	e = 0;				/* excess preflow. 						*/
	var	h = 0;				/* height. 							*/
	var	control:ActorRef = null		/* controller to report to when e is zero. 			*/
	var	source:Boolean	= false		/* true if we are the source.					*/
	var	sink:Boolean	= false		/* true if we are the sink.					*/
	var	edge: List[Edge] = Nil		/* adjacency list with edge objects shared with other nodes.	*/
	var	debug = false			/* to enable printing.						*/

    var     q:List[Edge] = Nil              /* list to resume to in discharge                               */
    var     active:Int = 0                  /* number of currently active push requests                     */


    var incHeight:Boolean = true

    def min(a:Int, b:Int) : Int = { if (a < b) a else b }

	def id: String = "@" + index;

	def other(a:Edge, u:ActorRef) : ActorRef = { if (u == a.u) a.v else a.u }

	def status: Unit = { if (debug) println(id + " e = " + e + ", h = " + h) }

	def enter(func: String): Unit = { if (debug) { println(id + " enters " + func); status } }
	def exit(func: String): Unit = { if (debug) { println(id + " exits " + func); status } }

	def relabel : Unit = {
		enter("relabel")
		h += 1
		exit("relabel")
	}

	def discharge: Unit = {
		var ed:Edge = null
		var p:List[Edge] = Nil

        // Resume or get adjecency list       
        if (q != Nil) {
            p = q
            q = Nil
        } else {
            p = edge
        }

        // Push while excess remains
        while(p != Nil && (e != 0 || source)) {
            ed = p.head
            push(ed, p)
            p = p.tail
            if (e == 0) {
                q = p
            }
	}

        // Send excess if source, else check if done 
        if (debug) { print(id + " completed discharge! e = " + e + "\n") }
        if (source) { 
            control ! SourceExcess(e)
        } else {
            check
        }

	}

    def push(ed:Edge, p:List[Edge]): Unit = {
        if (debug) { print(id + " trying edge with capacity=" + ed.c + ", current flow=" + ed.f + "\n") }
        var flow = 0

        // Calculate possible flow to push
        if (ed.u == self) {
            flow = ed.c - ed.f // Push downstream
            if (!source) { 
                if (e < flow) {
                    flow = e // Source does not worry about excess 
                }
            }
        } else {
            flow = ed.c + ed.f // Push upstream
            if (!source) {
                if (e < flow) {
                    flow = e
                }

            }
            flow = -flow
        }

        if (flow != 0) { // If the edge can handle more flow, request push
            if (debug) { print(id + " e = " + e + ", f = " + ed.f + ", c = " + ed.c + " => pushed flow = " + flow + "\n") }
            e -= abs(flow) // Decrease excess by flow
            if (!source) { assert(e >= 0) }
            active += 1
            other(ed, self) ! Push(ed, h, flow, p)
        }
    }

    def check: Unit = {
        if (debug) { print(id + " e = " + e + ", active = " + active + ", q==Nil=" + (q==Nil) + "\n") }
        if (e == 0) {
            if (debug) { print(id + " finished!\n") }
        } else if (active == 0) { // If we have no active pushes, but excess, we need to discharge
            if (q == Nil) { // q == Nil => we have tried all edges and got CannotPush from all 
                relabel
            }
            discharge
        }
    }

	def receive = {

	case Debug(debug: Boolean)	=> this.debug = debug

	case Print => status

	case edge:Edge => { this.edge = edge :: this.edge /* put this edge first in the adjacency-list. */ }

	case Control(control:ActorRef)	=> this.control = control

	case Sink	=> { sink = true }

	case Source(n:Int)	=> { h = n; source = true }

    case Run => { discharge } // Source starts by discharge

    case Push(ed:Edge, height:Int, flow:Int, otherq:List[Edge]) => {
        if (debug) {print(id + " recieved push request! my h = " + h + ", other h = " + height + "\n") }
        if (h < height) {
            if (debug) { print(id + " old flow = " + ed.f + ", old excess = " + e + "\n") }
            ed.f += flow
            e += abs(flow)
            if (debug) { print(id + " new flow = " + ed.f + ", new excess = " + e + "\n") }
            sender ! PushFinished(ed, otherq) // Tell sender push is complete
            if (source) { // Source & Sink sends updated excess to control
                control ! SourceExcess(e)
            } else if (sink) {
                control ! SinkExcess(e)
            } else {
                if (active == 0) {
                    discharge  // Nodes can discharge as excess is increased
                }
            }
        } else {
            if (debug) { print(id + " reply cannot push!\n") }
            sender ! CannotPush(flow)
        }
    }

    case CannotPush(flow:Int) => {
        active -= 1
        if (debug) { print(id + " received cannot push!\n") }
        e += abs(flow)
        if (!source) {check} // Source doesn't check/relabel
    }

    case PushFinished(ed:Edge, otherq:List[Edge]) => {
        active -= 1
        if (debug) { print(id + " received push finished!\n") }
        if (!source) {
            if (abs(ed.f) < ed.c) {
                q = ed :: q // If we could push, and there is capacity remaining, try edge again first
            }
            check
        } // Source doesn't check/relabel
    }

	case _		=> {
		println("" + index + " received an unknown message" + _) }

		assert(false)
	}

}


class Preflow extends Actor
{
	var	s	= 0;			/* index of source node.					*/
	var	t	= 0;			/* index of sink node.					*/
	var	n	= 0;			/* number of vertices in the graph.				*/
	var	edge:Array[Edge]	= null	/* edges in the graph.						*/
	var	node:Array[ActorRef]	= null	/* vertices in the graph.					*/
	var	ret:ActorRef 		= null	/* Actor to send result to.					*/

        var sinkExcess = -1
        var sourceExcess = -1
	
        def receive = {

	case node:Array[ActorRef]	=> {
		this.node = node
		n = node.size
		s = 0
		t = n-1
		for (u <- node)
			u ! Control(self)
                node(s) ! Source(n)
                node(t) ! Sink
	}

	case edge:Array[Edge] => this.edge = edge

	case SinkExcess(f:Int) => { 
            sinkExcess = f
            print("@C sink = " + sinkExcess + ", source = " + sourceExcess + "\n")
            if (abs(sinkExcess) == abs(sourceExcess)) {
                ret ! abs(f)
            }
        }

        case SourceExcess(f:Int) => {
            sourceExcess = f
            print("@C sink = " + sinkExcess + ", source = " + sourceExcess + "\n")
            if (abs(sinkExcess) == abs(sourceExcess)) {
                ret ! abs(f)
            }
        }

	case Maxflow => {
		ret = sender
		node(s) ! Run // Ask source to begin algorithm
	}

	}
}

object main extends App {
	implicit val t = Timeout(40 seconds);
	val	begin = System.currentTimeMillis()
	val system = ActorSystem("Main")
	val control = system.actorOf(Props[Preflow], name = "control")

	var	n = 0;
	var	m = 0;
	var	edge: Array[Edge] = null
	var	node: Array[ActorRef] = null

	val	s = new Scanner(System.in);

	n = s.nextInt
	m = s.nextInt

	/* next ignore c and p from 6railwayplanning */
	s.nextInt
	s.nextInt

	node = new Array[ActorRef](n)

	for (i <- 0 to n-1)
		node(i) = system.actorOf(Props(new Node(i)), name = "v" + i)

	edge = new Array[Edge](m)

	for (i <- 0 to m-1) {

		val u = s.nextInt
		val v = s.nextInt
		val c = s.nextInt

		edge(i) = new Edge(node(u), node(v), c)

		node(u) ! edge(i)
		node(v) ! edge(i)
		node(v) ! Print
	}

	println(node)
	control ! node
	control ! edge


	val flow = control ? Maxflow
	val f = Await.result(flow, t.duration)

	println("f = " + f)

	system.stop(control);
	system.terminate()

	val	end = System.currentTimeMillis()

	println("t = " + (end - begin) / 1000.0 + " s")
}
