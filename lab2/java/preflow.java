import java.util.Scanner;
import java.util.concurrent.Semaphore;
import java.util.ListIterator;
import java.util.LinkedList;

class Graph {

	int	s;
	int	t;
	int	n;
	int	m;
	Node	excess;		// list of nodes with excess preflow
	Node	node[];
	Edge	edge[];

	class PreflowThread extends Thread {
		public void run() {
			ppreflow();
		}
	}

	Graph(Node node[], Edge edge[])
	{
		this.node	= node;
		this.n		= node.length;
		this.edge	= edge;
		this.m		= edge.length;
	}

	synchronized void enter_excess(int u)
	{
		if (u != s && u != t) {
			node[u].next = excess;
			excess = node[u];
		}
	}

	synchronized int leave_excess() {
		Node v;
		int i = 0; // Here 0 = null
		v = excess;
		if (v != null) {
			i = v.i;
			excess = v.next;
		}
		return i;
	}

	int other(Edge a, Node u)
	{
		if (a.u == u)
			return a.v.i;
		else
			return a.u.i;
	}

	void relabel(int u)
	{
		node[u].h++;
		enter_excess(u);
	}

	void push(int u, int v, int e)
	{
		int f = 0;
		if (u == edge[e].u.i) {
			f = absmin(node[u].e, edge[e].c - edge[e].f);
			edge[e].f += f;
		} else {
			f = absmin(node[u].e, edge[e].c + edge[e].f);
			edge[e].f -= f;
		}
		node[u].e -= f;
		node[v].e += f;

		if (node[u].e > 0) {
			enter_excess(u);
		}
		if (node[v].e == f) {
			enter_excess(v);
		}
	}

	int absmin(int a, int b) {
		if (abs(a) < abs(b)) {
			return a;
		} else {
			return b;
		}
	}

	int abs(int a) {
		if (a < 0) {
			return -a;
		} else {
			return a;
		}
	}

	void ppreflow() {
		ListIterator<Edge>	iter;
		int			    b;
		Edge			a = null;
		Node			v = null;
		int u;

		while (excess != null) {
			u = leave_excess();
			iter = node[u].adj.listIterator();
			while (iter.hasNext()) {
				a = iter.next();
				int ai = a.i;

				if (u == a.u.i) {
					v = a.v;
					b = 1;
				} else {
					v = a.u;
					b = -1;
				}
				if (u < v.i) {
					try {
						node[u].mutex.acquire();
						node[v.i].mutex.acquire();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					try {
						node[v.i].mutex.acquire();
						node[u].mutex.acquire();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				if (node[u].h > v.h && b*edge[ai].f < a.c) {
					break;
				} else {
					try {
						node[v.i].mutex.release();
					} catch (Exception e) {
						e.printStackTrace();
					}
					v = null;
					if (iter.hasNext()) {
						try {
							node[u].mutex.release();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}

			if (v != null) {
				push(u, v.i, a.i);
				try {
					node[v.i].mutex.release();
					node[u].mutex.release();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				relabel(u);
				try {
					node[u].mutex.release();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	int preflow(int s, int t)
	{
		ListIterator<Edge>	iter;
		Edge			a;
		int 			nbr_threads = 8;
		PreflowThread threads[] = new PreflowThread[nbr_threads];

		this.s = s;
		this.t = t;
		node[s].h = n;

		iter = node[s].adj.listIterator();
		while (iter.hasNext()) {
			a = iter.next();
			node[s].e += a.c;
			push(s, other(a, node[s]), a.i);
		}

		for (int i = 0; i < nbr_threads; i++) {
			PreflowThread thread = new PreflowThread();
			threads[i] = thread;
			threads[i].start();
		}
		for (int i = 0; i < nbr_threads; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return node[t].e;
	}
}

class Node {
	int	h;
	int	e;
	int	i;
	Node	next;
	LinkedList<Edge>	adj;
	Semaphore mutex;

	Node(int i)
	{
		this.i = i;
		this.mutex = new Semaphore(1);
		adj = new LinkedList<Edge>();
	}
}

class Edge {
	Node	u;
	Node	v;
	int	f;
	int	c;
	int i;

	Edge(Node u, Node v, int c, int i)
	{
		this.u = u;
		this.v = v;
		this.c = c;
		this.i = i;

	}
}

class Preflow {
	public static void main(String args[])
	{
		double	begin = System.currentTimeMillis();
		Scanner s = new Scanner(System.in);
		int	n;
		int	m;
		int	i;
		int	u;
		int	v;
		int	c;
		int	f;
		Graph	g;

		n = s.nextInt();
		m = s.nextInt();
		s.nextInt();
		s.nextInt();
		Node[] node = new Node[n];
		Edge[] edge = new Edge[m];

		for (i = 0; i < n; i += 1)
			node[i] = new Node(i);

		for (i = 0; i < m; i += 1) {
			u = s.nextInt();
			v = s.nextInt();
			c = s.nextInt();
			edge[i] = new Edge(node[u], node[v], c, i);
			node[u].adj.addLast(edge[i]);
			node[v].adj.addLast(edge[i]);
		}

		g = new Graph(node, edge);
		f = g.preflow(0, n-1);
		double	end = System.currentTimeMillis();
		System.out.println("t = " + (end - begin) / 1000.0 + " s");
		System.out.println("f = " + f);
	}
}
